\section{Rigid connector}\label{sec_rigid_connector}

A rigid connector is a special kinematic constraint which limits the motion of the connected nodes to rigid body translations and rotations, as if they were connected by a rigid body with negligible mass. With such a constraint, the position of a boundary node $p$ at any time can be described as a function its constant position relative to the centroid and the centroid position and orientation relative to the origin as illustrated by Figure \ref{fig_rigid_connector_01} and Equations (\ref{eq_35}) and (\ref{eq_36}).

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics[width=\textwidth]{figures/rigid_connector.pdf}
%        \caption{caption 1}
%        \label{fig_rigid_connector_0}
    \end{subfigure}
    \hspace{0.05\textwidth}
    \begin{subfigure}[b]{0.35\textwidth}
        \includegraphics[width=\textwidth]{figures/rigid_constraint.eps}
%        \caption{caption 2}
%        \label{fig_rigid_connector}
    \end{subfigure}
\caption{Rigid connector kinematic description}
\label{fig_rigid_connector_01}
\end{figure}

\begin{equation}\label{eq_35}
{}_{_0}\vec{r}_p \left(t_0\right) = {}_{_0}\vec{r}_c\left(t_0\right) + \mat{A}_{01}^{T}\left(t_0\right) {}_{_1}\vec{r}
\end{equation}
\begin{equation}\label{eq_36}
{}_{_0}\vec{r}_p\left(t_1\right) = {}_{_0}\vec{r}_c\left(t_1\right) + \mat{A}_{01}^{T}\left(t_1\right) {}_{_1}\vec{r}
\end{equation}

Assuming the centroid reference initially coincides with the inertial frame of reference and its rotation is infinitesimal, node $p$ displacement is given by Equation (\ref{eq_70}).

\begin{equation}\label{eq_37}
\begin{split}
\vec{u}_{p} &= {}_{_0}\vec{r}_p\left(t_1\right) - {}_{_0}\vec{r}_p\left(t_0\right) = {}_{_0}\vec{r}_c\left(t_1\right) - {}_{_0}\vec{r}_c\left(t_0\right)+ \left[\mat{A}_{01}^{T}\left(t_0\right) - \mat{A}_{01}^{T}\left(t_0\right)\right] {}_{_1}\vec{r} \\
 &= \vec{u}_c + \left[\mat{A}_{01}^{T} - \mat{I}_3 \right] {}_{_1}\vec{r} = 
\begin{bmatrix}
u_c \\ v_c \\ w_c
\end{bmatrix} +
\begin{bmatrix}
0 & -\gamma & \beta \\
\gamma & 0 & -\phi \\
-\beta & \phi & 0
\end{bmatrix}
\begin{bmatrix}
x_p - x_c \\
y_p - y_c \\
z_p - z_c
\end{bmatrix}
\end{split}
\end{equation}
\begin{equation}\label{eq_70}
\vec{u}_p = \mat{B}_p \vec{q}_c
\end{equation}

where $\vec{q}_c$ is the degrees of freedom associated with the translation and rotation of the centroid and $\vec{B}_c$ is given by Equation (\ref{eq_38}).

\begin{equation}\label{eq_39}
\vec{q}_c = \begin{bmatrix}
u_{c} & v_{c} & w_{c} & \phi & \beta & \gamma\end{bmatrix}^{T}
\end{equation}
\begin{equation}\label{eq_38}
\vec{B}_p = \begin{bmatrix}1 & 0 & 0 & 0 & - z_{c} + z_{p} & y_{c} - y_{p} \\
0 & 1 & 0 & z_{c} - z_{p} & 0 & - x_{c} + x_{p} \\
0 & 0 & 1 & - y_{c} + y_{p} & x_{c} - x_{p} & 0
\end{bmatrix}
\end{equation}


If the constraint is applied over elements whose nodal degrees of freedom represent not only the node spacial position but also its orientation such like beams and plate elements, Equation (\ref{eq_38}) need to be modified to take the element orientation into account. The angular orientation of a shell node is generally represented by a unitary vector aligned with the shell surface at the node location. The angular degrees of freedom, is often given by the difference $\vec{a}$ between the shell normal with respect to the undeflected orientation or rather by the rotation vector $\vec{\theta}$. For small deflections, the relation between $\vec{a}$ and $\vec{\theta}$ is given by Equation (\ref{eq_64}) \cite{comsol_2019}.

\begin{equation}
{}_{_0}\vec{a} = {}_{_0}\vec{n}\left(t_0\right) - {}_{_0}\vec{n}\left(t_1\right)
\end{equation}
\begin{equation}\label{eq_64}
{}_{_0}\vec{a} = \left(\mat{A}_{01}^T - \vec{I}_3\right) {}_{_1}\vec{n} = \boldsymbol{\theta} \times {}_{_1}\vec{n}
\end{equation}

In shell elements, the rotation around the normal is not allowed and only two components of $\vec{a}$ (or $\vec{\theta}$) are linearly independent \cite{comsol_2019}. If the rotational degrees of freedom are written in terms of $\vec{\theta}$, the constraint Equation (\ref{eq_71}) must be enforced. If those are written in terms of $\vec{a}$, Equation (\ref{eq_65}) gives the constraint for the small displacements. Caution must be taken regarding which component of $\vec{a}$ to eliminate in order to avoid a division by zero.

\begin{equation}\label{eq_71}
\vec{n} \cdot \vec{\theta} = 0
\end{equation}
\begin{equation}\label{eq_65}
\vec{n} \cdot \vec{a} = 0
\end{equation}
%\begin{equation}\label{eq_66}
%a_3 = -\frac{n_1}{n_3}a_1 -\frac{n_2}{n_3}a_2
%\end{equation}

One solution is to represent $\vec{a}$ on a local set of coordinates and keep the remaining components of $\vec{a}$ tangent to the surface of the shell element at the node location. 

\begin{equation}\label{eq_67}
\left(\vec{a} - \vec{\theta}_c \times \vec{n}\right) \cdot \vec{t}_i \qquad i=0,1
\end{equation}

which can be simplified into

\begin{equation}\label{eq_68}
\vec{a} \cdot \vec{t}_0 - \vec{\theta} \cdot \vec{t}_1 = 0
\end{equation}
\begin{equation}\label{eq_69}
\vec{a} \cdot \vec{t}_1 + \vec{\theta} \cdot \vec{t}_0 = 0
\end{equation}

Combining Equations (\ref{eq_37}), (\ref{eq_68}) and (\ref{eq_69}), one can expand $\vec{u}_p$ and $\vec{B}_p$ in Equation (\ref{eq_70}) to include the angular degrees of freedom.

\begin{equation}
\vec{u}_p = \begin{bmatrix}
a_0 & a_1 & u & v & w
\end{bmatrix}^T
\end{equation}
\begin{equation}
\mat{B}_p = \left[\begin{matrix}0 & 0 & 0 & t_{1x} & t_{1y} & t_{1z}\\0 & 0 & 0 & - t_{0x} & - t_{0y} & - t_{0z}\\1 & 0 & 0 & 0 & - z_{c} + z_{p} & y_{c} - y_{p}\\0 & 1 & 0 & z_{c} - z_{p} & 0 & - x_{c} + x_{p}\\0 & 0 & 1 & - y_{c} + y_{p} & x_{c} - x_{p} & 0\end{matrix}\right]
\end{equation}

Equation (\ref{eq_70}) then describes the motion of an arbitrary node $p$ in the boundary as a function of the translation and rotation of the centroid $c$. Combining the motion of all boundary nodes into a single expression, equation (\ref{eq_43}) represents the boundary motion as a function of the centroid position and orientation.

\begin{equation}\label{eq_43}
\vec{u}_b = \mat{B} \vec{q}_c
\end{equation}

The motion of the entire substructure can then be written using the transformation matrix $\mat{N}$ in equations  (\ref{eq_40}) and (\ref{eq_41}).

\begin{equation}\label{eq_40}
\begin{bmatrix}
\vec{u}_b \\ \vec{u}_i
\end{bmatrix}_{(s)} = 
\begin{bmatrix}
\mat{B} & \mat{0} \\
\mat{0} & \mat{I} 
\end{bmatrix}_{(s)}
\begin{bmatrix}
\vec{q}_c \\ \vec{u}_i
\end{bmatrix}_{(s)}
\end{equation}
\begin{equation}\label{eq_41}
\vec{u}^{(s)} = \mat{N}^{(s)} \vec{q}^{(s)}
\end{equation}

Substituting equation (\ref{eq_41}) into equation (\ref{eq_2}) and pre-multiplying by $\mat{N}^T$, one obtains the equation of motion of the tower substructure constrained by the rigid connector, equation (\ref{eq_42}).

\begin{equation}\label{eq_42}
\mat{N}^{T} \mat{M} \mat{N} \vec{\ddot{q}} + \mat{N}^{T} \mat{K} \mat{N} \vec{q} = \mat{N}^{T} \vec{f}
\end{equation}

\begin{comment}
A simple python code is created to compare the rigid connector constraint described above against the rigid connector implemented in COMSOL. The tower structure from the DTU 10MW WT is modelled in COMSOL using shell elements (MITC9: nine nodes quadrilateral elements, quadratic discretisation). The tower structure mass and stiffness matrices are imported into python, where the bottom and a rigid connector are applied. Table \ref{tab_ap_rigid_connector} shows the first 20 natural frequencies of the tower structure with the rigid connector on its top.

\begin{table}[t]
	\centering
	\input{tables/appendix_rigid_connector.tex}
	\caption{caption}
	\label{tab_ap_rigid_connector}
\end{table}
\end{comment}