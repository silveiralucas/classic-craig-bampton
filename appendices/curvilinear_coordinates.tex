\section{Surface curvilinear coordinates}\label{sec_curvilinear_coordinates}

The analysis of scalar, vectorial and tensorial quantities defined over a curved shell element can be greatly simplified and generalised with the use of natural curvilinear coordinates. The shell geometry can be defined in terms of two parameters $\xi$ and $\eta$ through Equation (\ref{eq_a0}) where the parameters assume values between $-1$ and $1$ inside the shell element and are equal to $-1$ or $1$ at the element edges. The parametrisation must be defined through a one-to-one transformation mapping between the Cartesian and natural coordinates and locally and uniquely invertible everywhere inside the shell element.

\begin{figure}[b]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/shell_element.eps}
        \caption{Cartesian coordinates}
        \label{fig_curvilinear_coordinates_a}
    \end{subfigure}
    \hspace{0.10\textwidth}
    \begin{subfigure}[b]{0.35\textwidth}
        \includegraphics[width=\textwidth]{figures/nine_nodes_quadrilateral.eps}
        \caption{Curvilinear coordinates}
        \label{fig_curvilinear_coordinates_b}
    \end{subfigure}
\caption{Some caption}
\label{fig_curvilinear_coordinates}
\end{figure}

\begin{equation}\label{eq_a0}
\vec{r} = x\left(\xi, \eta\right) \vec{e}_x + y\left(\xi, \eta\right) \vec{e}_y + z\left(\xi, \eta\right) \vec{e}_z 
\end{equation}

%\begin{equation}\label{eq_a6}
%\vec{h}_\xi = \frac{\p\vec{r}}{\p\xi}
%\end{equation}
%\begin{equation}\label{eq_a7}
%\vec{h}_\eta = \frac{\p\vec{r}}{\p\eta}
%\end{equation}
%\begin{equation}\label{eq_a8}
%\vec{h}_\zeta = \vec{h}_\xi \times \vec{h}_\eta
%\end{equation}

The tangential basis vectors $\vec{h}_{xi}$ and $\vec{h}_{eta}$ are obtained from the derivative of the position vector with respect to $\xi$ and $\eta$ respectively while the normal vector $\vec{h}_{zeta}$ is obtained from  the cross product of $\vec{h}_{xi}$ and $\vec{h}_{eta}$ \cite{onate_2013}. It is convention to select $\xi$ and $\eta$ using the right hand rule so that the direction of $\zeta$ points outwards.

\begin{equation}\label{eq_a6}
\vec{h}_\xi = \frac{\p\vec{r}}{\p\xi}
,\quad \vec{h}_\eta = \frac{\p\vec{r}}{\p\eta}
,\quad \vec{h}_\zeta = \vec{h}_\xi \times \vec{h}_\eta
\end{equation}

Multiple shell elements with different number of nodes and nodal positions exist. The Lagrange nine nodes rectangular element is illustrated in Figures (\ref{fig_curvilinear_coordinates_a}) and (\ref{fig_curvilinear_coordinates_b}) in Cartesian and Curvilinear coordinates respectively. Serendipity elements are another frequently used class of elements where all the nodes are intentionally placed along the element edges for numerical reasons \cite{liu_2003}. The Cartesian coordinates of a point inside the element is then given by Equations (\ref{eq_a1}) to (\ref{eq_a3})

\begin{equation}\label{eq_a1}
x\left(\xi, \eta\right) = N_i\left(\xi, \eta\right) x_i
\end{equation}
\begin{equation}\label{eq_a2}
y\left(\xi, \eta\right) = N_i\left(\xi, \eta\right) y_i
\end{equation}
\begin{equation}\label{eq_a3}
z\left(\xi, \eta\right) = N_i\left(\xi, \eta\right) z_i
\end{equation}

where $N_i$ are linearly independent shape functions which assume a value one at node $i$ and a value zero at the other nodes. For a nine node rectangular element, one valid definition of $N_i$ is given by Equation (\ref{eq_a4}).

\begin{equation}\label{eq_a4}
N_i\left(\xi, \eta\right) = c_0 + c_1\xi + c_2\eta + c_3\xi\eta + c_4\xi^2 + c_5\eta^2 + c_6\xi^2\eta + c_7\xi\eta^2 + c_8\xi^2\eta^2
\end{equation}

where the constants $c_0$ to $c_8$ are found solving for $N_i\left(\xi_j, \eta_j\right) = \delta_{i, j}$ at the nodes. It is straightforward to show that the nine shape functions are then given by Equation (\ref{eq_a12}).

\begin{equation}\label{eq_a12}
\begin{split}
N_{0}\left(\xi, \eta\right) &= \frac{1}{4} \eta \xi \left(\eta - 1\right) \left(\xi - 1\right)
\\
N_{1}\left(\xi, \eta\right) &= \frac{1}{4} \eta \xi \left(\eta - 1\right) \left(\xi + 1\right)
\\
N_{2}\left(\xi, \eta\right) &= \frac{1}{4} \eta \xi \left(\eta + 1\right) \left(\xi + 1\right)
\\
N_{3}\left(\xi, \eta\right) &= \frac{1}{4} \eta \xi \left(\eta + 1\right) \left(\xi - 1\right)
\\
N_{4}\left(\xi, \eta\right) &= - \frac{1}{2}\eta \left(\eta - 1\right) \left(\xi - 1\right) \left(\xi + 1\right)
\\
N_{5}\left(\xi, \eta\right) &= - \frac{1}{2}\xi \left(\eta - 1\right) \left(\eta + 1\right) \left(\xi + 1\right)
\\
N_{6}\left(\xi, \eta\right) &= - \frac{1}{2}\eta \left(\eta + 1\right) \left(\xi - 1\right) \left(\xi + 1\right)
\\
N_{7}\left(\xi, \eta\right) &= - \frac{1}{2}\xi \left(\eta - 1\right) \left(\eta + 1\right) \left(\xi - 1\right)
\\
N_{8}\left(\xi, \eta\right) &= \eta^{2} \xi^{2} - \eta^{2} - \xi^{2} + 1
\end{split}
\end{equation}

Substituting Equations (\ref{eq_a1}), (\ref{eq_a2}) and (\ref{eq_a3}) into (\ref{eq_a6}), the basis vectors tangent to element surface are given by Equations (\ref{eq_a9}) to (\ref{eq_a11}) and the surface normal can then be calculated with Equation (\ref{eq_a3}).

\begin{equation}\label{eq_a9}
\vec{h}_\xi = \frac{\p N_i}{\p\xi} x_i \;\vec{e}_x + \frac{\p N_i}{\p\xi} y_i \;\vec{e}_y + \frac{\p N_i}{\p\xi} z_i \;\vec{e}_z
\end{equation}
\begin{equation}\label{eq_a10}
\vec{h}_\eta = \frac{\p N_i}{\p\eta} x_i \;\vec{e}_x + \frac{\p N_i}{\p\eta} y_i \;\vec{e}_y + \frac{\p N_i}{\p\eta} z_i \;\vec{e}_z
\end{equation}
\begin{equation}\label{eq_a11}
\vec{h}_{\zeta} = \vec{h}_{\xi} \times \vec{h}_{\eta}
\end{equation}

Unlike Cartesian coordinates, in general curvilinear basis vectors are neither unitary or orthogonal. Although transformations with non-orthogonal, non-unitary basis vectors can be performed (employing Ricci calculus notation) and is encountered in several references, it is usually simpler to define local orthogonal unitary basis vectors through Equations (\ref{eq_a12}) to (\ref{eq_a14}).

\begin{equation}\label{eq_a12}
\vec{e}_0 = \vec{e}_\xi
\end{equation}
\begin{equation}\label{eq_a13}
\vec{e}_1 = \vec{e}_\zeta \times \vec{e}_\xi
\end{equation}
\begin{equation}\label{eq_a14}
\vec{e}_2 = \vec{e}_\zeta
\end{equation}

where,

\begin{equation}\label{eq_a11}
\vec{e}_{\xi} = \frac{\vec{h}_{\xi}}{\left|\vec{h}_{\xi}\right|}, \quad
\vec{e}_{\zeta} = \frac{\vec{h}_{\zeta}}{\left|\vec{h}_{\zeta}\right|}
\end{equation}

It is worth mentioning that normals obtained through this method depend on how accurately the element represent locally the surface geometry. Small inaccuracies on the nodes position may influence significantly the value of the normal vector components. Moreover, small inaccuracies on the vector normals may influence significantly constraint values and consequently the final results. One method to improve the accuracy of the normal values is to extend the element to include more nodes in the interpolation. In order to avoid overfitting, the shape function polynomial order can be maintained and a least squared fitting used. Another simpler approach is to average the normals obtained for nodes shared by different elements.
