\contentsline {chapter}{\numberline {1}Craig Bampton method}{2}%
\contentsline {section}{\numberline {1.1}Static constraint mode shapes}{3}%
\contentsline {section}{\numberline {1.2}Dynamic normal mode shapes}{3}%
\contentsline {section}{\numberline {1.3}Modal reduction}{3}%
\contentsline {section}{\numberline {1.4}Assembly}{4}%
\contentsline {section}{\numberline {1.5}Solution projection}{5}%
\contentsline {section}{\numberline {1.6}Rigid connector}{5}%
\contentsline {section}{\numberline {1.7}Numerical example}{7}%
\contentsline {chapter}{\numberline {A}Appendices}{ii}%
\contentsline {section}{\numberline {A.1}Surface curvilinear coordinates}{iii}%
\contentsline {section}{\numberline {A.2}3D beam element}{v}%
