\chapter{Craig Bampton method}\label{chap_craig_bampton_method}

A dynamic analysis of large scale or finely detailed systems often rely on its subdivision into smaller substructures. The substructures can be processed separately, through model order reduction techniques, in order to obtain approximate models which are computationally simpler while conserving the characteristics of interest of the full model. The reduced substructures are then recoupled to form an approximation of the full system. Even in cases where it is possible to analyse the full system directly, it is often more feasible to reduce its computational complexity before performing a large number of time simulations or dynamic analysis. Model order reduction techniques allows the simulation of refined models at lower computational costs as the ``refinement of finite element models increases at least as fast as the computing capabilities''\cite{rixen_2004}. Another benefit of dividing the model into substructures is that``substructures are often designed by different engineering groups (...) as independently as possible with due consideration being given to the final coupling of substructures''\cite{craig_bampton_1968} and ``building reduced models of subparts facilitates sharing models between design groups''\cite{rixen_2004}. Consider a global structure governed by a linearised system of Equations (\ref{eq_1}).

%\begin{figure}
%    \centering
%    \begin{subfigure}[b]{0.3\textwidth}
%        \includegraphics[width=\textwidth]{figures/system.pdf}
%%        aption{caption 1}
%%        \label{fig_1}
%    \end{subfigure}
%    \hspace{0.05\textwidth}
%    \begin{subfigure}[b]{0.35\textwidth}
%        \includegraphics[width=\textwidth]{figures/system_2.pdf}
%%        \caption{caption 2}
%%        \label{fig_2}
%    \end{subfigure}
%\caption{Discretized substructured domain}
%\label{fig_discretized_substructured_domain}
%\end{figure}


\begin{equation}\label{eq_1}
\mat{M}\vec{\ddot{u}} + \mat{K}\vec{u} = \vec{f}
\end{equation}

The structure may be divided into $n$ non-overlapping substructures and the linearised equations of motion of a substructure $s$ are given by Equation (\ref{eq_2}) where $\vec{f}^{(s)}$ are external forces and $\vec{g}^{(s)}$ are the reaction forces acting on the substructure boundaries due to its connection with adjacent substructures. It is convenient to organise the degrees of freedom in Equation (\ref{eq_2}) grouping together the DOFs relative to nodes in the boundary with other substructures and internal nodes, indicated by the subscripts $b$ an $i$ respectively. It must be clarified that according to this notation the subscript $b$ is applied only to nodes in the boundary between substructures, nodes on the external boundaries of the substructure are also denoted with the $i$ subscript.

\begin{equation}\label{eq_2}
\mat{M}^{(s)}\vec{\ddot{u}}^{(s)} + \mat{K}^{(s)}\vec{u}^{(s)} = \vec{f}^{(s)} + \vec{g}^{(s)}
\end{equation}
\begin{equation}\label{eq_3}
\begin{bmatrix}
\mat{M}^{(s)}_{bb} & \mat{0} \\
\mat{0} & \mat{M}^{(s)}_{ii}
\end{bmatrix}
\begin{bmatrix}
\vec{\ddot{u}}^{(s)}_{b} \\
\vec{\ddot{u}}^{(s)}_{i}
\end{bmatrix} +
\begin{bmatrix}
\mat{K}^{(s)}_{bb} & \mat{K}^{(s)}_{bi} \\
\mat{K}^{(s)}_{ib} & \mat{K}^{(s)}_{ii}
\end{bmatrix}
\begin{bmatrix}
\vec{u}^{(s)}_{b} \\
\vec{u}^{(s)}_{i}
\end{bmatrix}  =
\begin{bmatrix}
\vec{f}^{(s)}_{b} \\
\vec{f}^{(s)}_{i}
\end{bmatrix} +
\begin{bmatrix}
\vec{g}^{(s)}_{b} \\
\vec{0}
\end{bmatrix}
\end{equation}\vspace{0.0mm}

The objective of the model order reduction is to approximate Equation (\ref{eq_2}) by a similar one with fewer degrees of freedom.

\begin{equation}\label{eq_12}
\mat{\bar{M}}^{(s)} \vec{\ddot{q}} + \mat{\bar{K}}^{(s)} \vec{{q}} = \vec{\bar{f}}^{(s)} + \vec{\bar{g}}^{(s)}
\end{equation}

The degrees of freedom $\vec{u}^{(s)}$ of the full substructure are projected on $\vec{q}^{(s)}$ through means of a reduction basis transformation $\mat{A}^{(s)}$. The overline on Equation (\ref{eq_12}) indicates the Galerkin projection of the matrices and vectors on the reduction basis. Several model order reduction techniques for calculating the reduction basis exist. In the classical Craig-Bampton \cite{craig_bampton_1968} model, the reduction basis transformation is given by Equation (\ref{eq_14}).

\begin{equation}\label{eq_13}
\vec{u}^{(s)} = \mat{A}^{(s)} \vec{q}^{(s)}
\end{equation}
\begin{equation}\label{eq_14}
\begin{bmatrix}
\vec{u}_b \\ \vec{u}_i
\end{bmatrix}_{(s)} =
\begin{bmatrix}
\mat{I} & \mat{0} \\
\mat{\Psi}_c & \mat{\bar{\Phi}}_n
\end{bmatrix}_{(s)}
\begin{bmatrix}
\vec{u}_b \\ \vec{\bar{q}}_n
\end{bmatrix}_{(s)}
\end{equation}

where the constraint (Guyan) mode shapes $\mat{\Psi}_c$ account for the effect of the boundary displacement on the internal nodes and the normal mode shapes $\mat{\bar{\Phi}}_n$ account for the internal dynamics of the substructure. A brief description of their calculation and physical meaning is given in sections \ref{constraint_modes} and \ref{normal_modes} respectively.

\section{Static constraint mode shapes}\label{constraint_modes}

The constraint mode shapes relate the displacement of the internal nodes to the static displacement of the nodes in the boundary $b$ between the substructure $s$ and the rest of the structure.

\begin{equation}\label{eq_4}
\vec{u}^{(s)}_{i} = \mat{\Psi}^{(s)}_{c} \vec{u}^{(s)}_{b}
\end{equation}

The relation between internal displacements and the boundary degrees of freedom can be obtained applying successive unitary static displacements to the boundary $b$ nodes degrees of freedom in the absence of other external forces, or from the steady solution of Equation (\ref{eq_3}) with $\vec{f}^{(s)} = \vec{0}$.

\begin{equation}\label{eq_5}
\begin{bmatrix}
\mat{K}^{(s)}_{bb} & \mat{K}^{(s)}_{bi} \\
\mat{K}^{(s)}_{ib} & \mat{K}^{(s)}_{ii}
\end{bmatrix} \begin{bmatrix}
\vec{u}^{(s)}_{b} \\
\vec{u}^{(s)}_{i}
\end{bmatrix}=
\begin{bmatrix}
\vec{g}^{(s)}_{b} \\
\vec{0}
\end{bmatrix}
\end{equation}

The relation between $\vec{u}^{(s)}_{i}$ and $\vec{u}^{(s)}_{b}$ under the assumed conditions is then given by Equation (\ref{eq_7}) and the constraint static mode shapes are given by Equation (\ref{eq_8}).

\begin{comment}
\begin{equation}\label{eq_6}
\mat{K}^{(s)}_{ib} \vec{u}^{(s)}_{b} + \mat{K}^{(s)}_{ii} \vec{u}^{(s)}_{i} = \vec{0}
\end{equation}

\end{comment}

\begin{equation}\label{eq_7}
\vec{u}^{(s)}_{i} = - \left[\mat{K}^{(s)}_{ii}\right]^{-1} \mat{K}^{(s)}_{ib} \vec{u}^{(s)}_{b}
\end{equation}
\begin{equation}\label{eq_8}
\mat{\Psi}^{(s)}_{c} = - \left[\mat{K}^{(s)}_{ii}\right]^{-1} \mat{K}^{(s)}_{ib}
\end{equation}

\section{Dynamic normal mode shapes}\label{normal_modes}

The dynamic normal mode shapes are defined as the normal modes of the substructure while the boundary degrees of freedom are constrained and account for the internal dynamics of the substructure. With $\vec{u}_b = \vec{0}$, Equation (\ref{eq_3}) then becomes:

\begin{equation}\label{eq_9}
\mat{M}_{ii}^{(s)}\vec{\ddot{u}}_{ii} - \mat{K}_{ii}^{(s)}\vec{u}_{ii} = \vec{f}_i^{(s)}
\end{equation}

The normal modes are obtained solving for the eigenvectors of Equation (\ref{eq_9}). The eigenvectors form the respective columns of the matrix $\mat{\Phi}_n$.

\begin{equation}\label{eq_10}
\left(-\omega^2\mat{M}_{ii}^{(s)}\vec{\phi}_i + \mat{K}_{ii}^{(s)}\vec{\phi}_{i} \right) = \vec{0}
\end{equation}
\begin{equation}\label{eq_11}
\vec{\lambda}, \mat{\Phi}_n = eig(\mat{M}_{ii}^{(s)}, \mat{K}_{ii}^{(s)})
\end{equation}

\section{Modal reduction}

In the linearised model, the displacement of the internal nodes is given by a linear combination of the static constraint mode shapes and the internal normal mode shapes, Equation (\ref{eq_15}). The modal reduction comes from the assumption that a small subset $\mat{\bar{\Phi}}_n$ of the dynamic normal modes $\mat{\Phi}_n$ is enough to adequately represent the interior dynamics of the substructure \cite{craig_bampton_1968}.

\begin{equation}\label{eq_15}
\begin{split}
\vec{u}_i & = \mat{\Psi}_c \vec{u}_b + \mat{{\Phi}}_n \vec{q}_n \\
& \approx \mat{\Psi}_c \vec{u}_b + \mat{\bar{\Phi}}_n \vec{\bar{q}}_n
\end{split}
\end{equation}

The reduction basis transformation is then given by Equation (\ref{eq_16}).

\begin{equation}\label{eq_16}
\mat{A}^{(s)} = \begin{bmatrix}
\mat{I} & \mat{0} \\
\mat{\Psi}_c & \mat{\bar{\Phi}}_n
\end{bmatrix}_{(s)}
\end{equation}

\section{Assembly}

\begin{figure}
	\centering
	\includegraphics[width=0.3\textwidth]{figures/ex_ass.pdf} 
	\caption{Displacement and internal forces compatibility}
	\label{fig_compatibility}
\end{figure}

\begin{comment}
To establish substructuring coupling / decoupling in each of the above-mentioned domains, two conditions should be met:

Coordinate compatibility, i.e. the connecting nodes of two substructures should have equal interface displacement.
Force equilibrium, i.e. the interface forces between connecting nodes have equal magnitude and opposing sign.
These are the two essential conditions that keep substructures together, hence allow to construct an assembly of multiple components.
\end{comment}

To couple two substructures, it is necessary to satisfy the displacement and force compatibilities given by Equation (\ref{eq_63}) and illustrated in Figure \ref{fig_compatibility}. Different methods of re-assembling the substructures equations of motion satisfying the displacement and force compatibility conditions exist, each with its advantages and drawbacks. They are generally classified as primal or dual assemblies, although methods significantly different from both exist.

\begin{equation}\label{eq_63}
\left\{\begin{array}{llr}
    \text{displacement compatibility} &: & \vec{u}_0 - \vec{u}_1 = \vec{0}\\
    \text{force compatibility} &: & \vec{g}_0 + \vec{g}_1 = \vec{0}
\end{array}\right.
\end{equation}\vspace{0.0mm}

The primal assembly is maybe the simpler and most straightforward of the two. It involves the identification of the structure-wise generalised degrees of freedom $\vec{q}$ and transformation matrices $\mat{L}^{(s)}$ for the projection of the substructures DOFs $\vec{q}^{(s)}$ on $\vec{q}$ through Equation (\ref{eq_17}). After the transformation matrices are identified, the mass and stiffness matrices are projected on the generalised coordinates and summed to form Equation (\ref{eq_62}).

\begin{equation}\label{eq_62}
\mat{M}\vec{\ddot{q}} + \mat{K}\vec{q} = \vec{f}
\end{equation}

where,

\begin{equation}\label{eq_17}
\vec{q}^{(s)} = \mat{L}^{(s)} \vec{q}
\end{equation}
\begin{equation}\label{eq_18}
\mat{M} = \sum_{s}{{\vec{L}^{(s)}}^T \vec{M}^{(s)} \vec{L}^{(s)}}, \quad
%\end{equation}
%\begin{equation}
\mat{K} = \sum_{s}{{\vec{L}^{(s)}}^T \vec{K}^{(s)} \vec{L}^{(s)}} \quad \text{and} \quad
%\end{equation}
%\begin{equation}
\vec{f} = \sum_{s}{{\vec{L}^{(s)}}^T \vec{f}^{(s)}}
\end{equation}

The stiffness matrix $\mat{K}$ after a primal assembly has the form illustrated on Figure (\ref{fig_primal_assembly_matrices}) where the filled region represents the degrees of freedom appertaining to both substructures. On the simplest scenario where the interface nodes on both substructures coincide and the same system of coordinates is employed, the transformation matrix $\mat{L}^{(s)}$ is composed of ones and zeros. It is straightforward to verify the primal assembly satisfy both the displacement and force compatibility conditions.

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.30\textwidth}
        \includegraphics[width=\textwidth]{figures/primal_assembly.eps}
        \caption{Primal assembly}
        \label{fig_primal_assembly_matrices}
    \end{subfigure}
    \hspace{0.1\textwidth}
    \begin{subfigure}[b]{0.31\textwidth}
        \includegraphics[width=\textwidth]{figures/dual_assembly.eps}
        \caption{Dual assembly}
        \label{fig_dual_assembly_matrices}
    \end{subfigure}
\caption{Structure assembly methods}
\label{fig_structure_assembly_matrices}
\end{figure}

The dual assembly organises the global DOFs keeping the substructures degrees of freedom separated in the assembled matrices, as illustrated on Figure (\ref{fig_dual_assembly_matrices}). The displacement compatibility is enforced explicitly adding a set of equations operating on the displacements. The force compatibility condition is satisfied by the introduction of Lagrange multipliers defined so that $\mat{g} = -\mat{B}^{T} \vec{\boldsymbol{\lambda}}$.

\begin{equation}\label{eq_19}
\begin{cases}
\mat{\tilde{M}}\vec{\ddot{q}} + \mat{\tilde{K}}\vec{q} + \mat{B}^{T} \vec{\boldsymbol{\lambda}} = \vec{f} \\
\mat{B}\vec{u} = \vec{0}
\end{cases}
\end{equation}

where,

\begin{equation}\label{eq_20}
\vec{q} = \begin{bmatrix}
\vec{q}^{(0)} & \vec{q}^{(1)} & \boldsymbol{\lambda}
\end{bmatrix}^{T}
\end{equation}

Another use of the dual assembly is as an intermediary step for obtaining the primal assembly. Once the geometric compatibility is obtained, a transformation of coordinates matrix can be obtained from the null-space of the displacement constraints.

\begin{equation}\label{eq_21}
\vec{u} = \mat{N} \vec{q}
\end{equation}
\begin{equation}\label{eq_22}
\mat{B}\vec{u} = \mat{B} \mat{N} \vec{q} = \vec{0} \quad \forall \vec{q}
\end{equation}

Hence the transformation matrix $\mat{N}$ is the null-space of the geometric constraints $\mat{B}$. This relation holds for other geometric constraints and is a convenient way of automating the process of obtaining transformation matrices. It must be noted that the null matrices are not unique neither sparse.

\begin{equation}\label{eq_23}
{\displaystyle {\begin{cases}\mathbf {N} ={\text{null}}(\mathbf {B} )\\\mathbf {B} ^{T}={\text{null}}(\mathbf {N} ^{T})\end{cases}}}
\end{equation}

\section{Solution projection}

Once the solution is obtained in terms of the generalised coordinates, the nodal displacements $\vec{u}^{(s)}$ can be obtained projecting  the solution $\vec{q}$ using the same transformation matrices used employed to assemble and reduce the substructures, equation (\ref{eq_24})

\begin{equation}\label{eq_24}
\vec{u}^{(s)} = \mat{A}^{(s)} \vec{q}^{(s)} = \mat{A}^{(s)} \mat{L}^{(s)} \vec{q}
\end{equation}

\input{appendices/rigid_connector.tex}

\section{Numerical example}\label{numerical_example}

In section \ref{chap_craig_bampton_method}, the theory and formulation behind classic Craig-Bampton dynamic substructuring and linear model reduction method are presented and all the necessary steps necessary to its implementation are detailed. In this section, part of the DTU 10MW wind turbine structure is used as an example to illustrate the method implementation and its accuracy to deal with linear problems.

The tower of DTU-10MW-WT is composed of 10 conical sections with constant wall thickness. Its outer diameter varies linearly between $8.3$ meters at the  bottom and $5.5$ meters at the top as illustrated in Fig.  \ref{fig_DTU10MW_tower_towertop} and described in Table \ref{tab_tower}. The tower is made of S355 (DIN EN 10025-2) structural steel, with a Young's modulus $E$ of $210.0$ $\text{GPa}$, a Poisson's ratio of $0.3$ and a density $\rho$ of $750.0$ $\text{kg}/\text{m}^3$. However, in the models bellow the tower density is increased to $8500.0$ $\text{kg}/\text{m}^3$ to account for the mass of secondary structures not included in the models.

\begin{figure}
	\centering
	\includegraphics[width=0.08\textwidth]{figures/tower_shell.eps} 
	\caption{Numerical example: DTU 10MW WT tower and nacelle.}
	\label{fig_DTU10MW_tower_towertop}
\end{figure}





\begin{table}
	\centering
	\caption{Tower cross section dimensions and wall thickness distribution}
	\label{tab_tower}
	\input{tables/tower.tex}
\end{table}

In the nacelle, the structure between the tower yaw bearing and the wind turbine shaft is modelled as a cantilever beam with properties are described at Table \ref{tab_tower_top}. The nacelle and spinner mass and inertia are modelled as a concentrated mass $m = 446040.0 \text{ kg}$ and inertia $\mat{I}$, equation (\ref{eq_26}), located a distance $\vec{r}$, equation (\ref{eq_25}), from the tower-top beam.

\begin{equation}\label{eq_25}
\vec{r}=\begin{bmatrix}
0.0 & -2.687 & -0.30061
\end{bmatrix}^T \text{ m}
\end{equation}
\begin{equation}\label{eq_26}
\mat{I} = \text{diag}\begin{bmatrix}
4.1060E+06 & 4.1060E+05 & 4.1060E+06
\end{bmatrix} \text{ kg}\text{m}^2
\end{equation}
\begin{equation}
\mat{J} =\mat{I} +m\left[\left(\vec{r} \cdot \vec{r} \right)\mathbf{I}_{3} - \vec{r} \otimes \vec{r} \right]
\end{equation}

\begin{table}
\centering
\caption{Tower-top properties}
\label{tab_tower_top}
\input{tables/nacelle.tex}
\end{table}

The tower structure is implemented using the commercial software COMSOL using 9 nodes shell elements. Fig. \ref{fig_comsol_mesh} illustrate the towers last section mesh. The unconstrained mass and stiffness are exported to python.

\begin{figure}
	\centering
	\includegraphics[width=0.3\textwidth]{figures/mesh.png}
	\caption{COMSOL mesh}
	\label{fig_comsol_mesh}
\end{figure}


The nacelle is modelled using the tree-dimensional Euler-Bernoulli formulation presented in Appendix~ \ref{sec_3D_beam_element}. A concentrated mass/inertia is added to the other extremity of the tower-top representing the nacelle mass and moments of inertia. A single beam element is used to model the nacelle.

In order to connect the tower and the nacelle substructure, the motion of the nodes on top of the tower must be translated into the six degrees of freedom corresponding to the nacelle lower node. One way of achieving this is to project the tower boundary nodes motion into a set of degrees of freedom which contain the six DOFs of the nacelle lower node. The simplest projection possible contains only these six DOFs. Such a projection corresponds to a rigid connector between the boundary nodes and its centroid.

The tower substructure model is reduced using the classical Craig-Bampton model described in the previous section. The first 24 of the 28064 internal mode shapes are used to represent the interior dynamics of the substructure dynamics. For simplicity the subset was chosen based on the lower natural frequencies. The tower and the nacelle substructures are then mounted using the primal assembly. The natural frequencies of the re-assembled structure are presented in Table \ref{tab_ex_2}.

\begin{table}
	\centering
	\caption{Natural frequencies obtained using model 2}
	\label{tab_ex_2}
	\input{tables/ex_2.tex}
\end{table}

%\begin{comment}
\begin{figure}
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_0.eps}
        \caption{mode 1}
        \label{fig_mode_1}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_1.eps}
        \caption{mode 2}
        \label{fig_mode_2}
    \end{subfigure}
%\caption{caption 12}
%\label{fig_12}
\end{figure}

\begin{figure}\ContinuedFloat
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_2.eps}
        \caption{mode 3}
        \label{fig_mode_3}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_3.eps}
        \caption{mode 4}
        \label{fig_mode_4}
    \end{subfigure}
%\caption{caption 12}
%\label{fig_12}
\end{figure}

\begin{figure}\ContinuedFloat
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_4.eps}
        \caption{mode 5}
        \label{fig_mode_5}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_5.eps}
        \caption{mode 6}
        \label{fig_mode_6}
    \end{subfigure}
%\caption{caption 12}
%\label{fig_12345}
\end{figure}

\begin{figure}\ContinuedFloat
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_6.eps}
        \caption{mode 7}
        \label{fig_mode_7}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_7.eps}
        \caption{mode 8}
        \label{fig_mode_8}
    \end{subfigure}
%\caption{caption 12}
%\label{fig_12345}
\end{figure}

\begin{figure}\ContinuedFloat
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_8.eps}
        \caption{mode 9}
        \label{fig_mode_9}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_9.eps}
        \caption{mode 10}
        \label{fig_mode_10}
    \end{subfigure}
%\caption{caption 12}
%\label{fig_12345}
\end{figure}

\begin{figure}\ContinuedFloat
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_10.eps}
        \caption{mode 11}
        \label{fig_mode_11}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_11.eps}
        \caption{mode 12}
        \label{fig_mode_12}
    \end{subfigure}
%\caption{caption 12}
%\label{fig_12345}
\end{figure}

\begin{figure}\ContinuedFloat
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_12.eps}
        \caption{mode 13}
        \label{fig_mode_13}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_13.eps}
        \caption{mode 14}
        \label{fig_mode_14}
    \end{subfigure}
%\caption{caption 12}
%\label{fig_12345}
\end{figure}

\begin{figure}\ContinuedFloat
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_14.eps}
        \caption{mode 15}
        \label{fig_mode_15}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_15.eps}
        \caption{mode 16}
        \label{fig_mode_16}
    \end{subfigure}
%\caption{caption 12}
%\label{fig_12345}
\end{figure}

\begin{figure}\ContinuedFloat
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_16.eps}
        \caption{mode 17}
        \label{fig_mode_17}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_17.eps}
        \caption{mode 18}
        \label{fig_18}
    \end{subfigure}
%\caption{caption 12}
%\label{fig_12345}
\end{figure}

\begin{figure}\ContinuedFloat
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_18.eps}
        \caption{mode 19}
        \label{fig_19}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/tower_mode_19.eps}
        \caption{mode 20}
        \label{fig_mode_20}
    \end{subfigure}
\caption{Mode shapes}
\label{fig_12345}
\end{figure}
%\end{comment