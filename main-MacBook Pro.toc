\contentsline {chapter}{\numberline {1}Craig Bampton method}{3}%
\contentsline {section}{\numberline {1.1}Static constraint mode shapes}{4}%
\contentsline {section}{\numberline {1.2}Dynamic normal mode shapes}{4}%
\contentsline {section}{\numberline {1.3}Modal reduction}{5}%
\contentsline {section}{\numberline {1.4}Assembly}{5}%
\contentsline {section}{\numberline {1.5}Solution projection}{6}%
\contentsline {section}{\numberline {1.6}Rigid connector}{7}%
\contentsline {section}{\numberline {1.7}Numerical example}{8}%
\contentsline {subsection}{\numberline {1.7.1}Model 1}{10}%
\contentsline {subsection}{\numberline {1.7.2}Model 2}{11}%
\contentsline {chapter}{\numberline {A} }{i}%
\contentsline {section}{\numberline {A.1}3D beam element}{i}%
\contentsline {section}{\numberline {A.2}2D beam element}{iii}%
