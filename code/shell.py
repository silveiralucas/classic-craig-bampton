import numpy as np
import scipy.io as sio
import scipy.linalg as sa
import scipy.sparse as ss
import scipy.sparse.linalg as ssl
import copy
from numba import jit
from mayavi import mlab

from time import time

#%%

# Empty class
class empty_object(object):
    def copy(self):
        return copy.deepcopy(self)

# Loading Matlab data
def load_from_livelink(file_name): # return: nodes, sys
    toto = sio.loadmat(file_name)
    
    sys = empty_object()
    sys.coord = toto['nodes'][0][0][0]
    sys.node = toto['nodes'][0][0][1][0] - 1
    sys.dof = toto['nodes'][0][0][2]
    
    sys.n_nodes = sys.node.size
    sys.n_dofs = sys.dof.size
    
    sys.quad = toto['quad'][0][0][0]
    
    sys.M = (toto['sys'][0][0][0])
    sys.K = (toto['sys'][0][0][1])
    sys.D = (toto['sys'][0][0][2])
    sys.C = (toto['sys'][0][0][3])
    sys.L = (toto['sys'][0][0][4][0])
    sys.N = (toto['sys'][0][0][5])
    sys.Nf = (toto['sys'][0][0][6])
    sys.Mc = (toto['sys'][0][0][7])
    sys.Kc = (toto['sys'][0][0][8])
        
    return sys

@jit
def boolean_matrix(a, b):
    
    # Get matrix sparsity
    n = 0
    for i in range(a.size):
        for j in range(b.size):
            if (a[i]==b[j]):
                n += 1
    
    data = np.ones((n,), dtype=int)
    idx = np.zeros((n,), dtype=int)
    jdx = np.zeros((n,), dtype=int)
    n = 0
    for i in range(a.size):
        for j in range(b.size):
            if (a[i]==b[j]):
                idx[n] = i
                jdx[n] = j
                n += 1
    
    L = ss.csr_matrix((data, (idx, jdx)), shape=(a.size, b.size))
    
    return L

def rigid_string(r, r_c):
    
    x_i, y_i, z_i = r
    x_c, y_c, z_c = r_c
    
    M_e = np.zeros((3, 6), dtype=float)
    M_e[0, 0] = 1
    M_e[0, 4] = -z_c + z_i
    M_e[0, 5] = y_c - y_i
    M_e[1, 1] = 1
    M_e[1, 3] = z_c - z_i
    M_e[1, 5] = -x_c + x_i
    M_e[2, 2] = 1
    M_e[2, 3] = -y_c + y_i
    M_e[2, 4] = x_c - x_i


    B_e = np.zeros((3, 9), dtype=float)
    B_e[0, 0] = 1
    B_e[0, 4] = -z_c + z_i
    B_e[0, 5] = y_c - y_i
    B_e[0, 6] = -1
    B_e[1, 1] = 1
    B_e[1, 3] = z_c - z_i
    B_e[1, 5] = -x_c + x_i
    B_e[1, 7] = -1
    B_e[2, 2] = 1
    B_e[2, 3] = -y_c + y_i
    B_e[2, 4] = x_c - x_i
    B_e[2, 8] = -1
        
    return M_e, B_e

def beam_3D(G, I_x, J, A, rho, I_y, l, E):
    
    K_e = np.zeros((12, 12), dtype=float)
    K_e[0, 0] = 12*E*I_y/l**3
    K_e[0, 4] = -6*E*I_y/l**2
    K_e[0, 6] = -12*E*I_y/l**3
    K_e[0, 10] = -6*E*I_y/l**2
    K_e[1, 1] = 12*E*I_x/l**3
    K_e[1, 3] = 6*E*I_x/l**2
    K_e[1, 7] = -12*E*I_x/l**3
    K_e[1, 9] = 6*E*I_x/l**2
    K_e[2, 2] = A*E/l
    K_e[2, 8] = -A*E/l
    K_e[3, 1] = 6*E*I_x/l**2
    K_e[3, 3] = 4*E*I_x/l
    K_e[3, 7] = -6*E*I_x/l**2
    K_e[3, 9] = 2*E*I_x/l
    K_e[4, 0] = -6*E*I_y/l**2
    K_e[4, 4] = 4*E*I_y/l
    K_e[4, 6] = 6*E*I_y/l**2
    K_e[4, 10] = 2*E*I_y/l
    K_e[5, 5] = G*J/l
    K_e[5, 11] = -G*J/l
    K_e[6, 0] = -12*E*I_y/l**3
    K_e[6, 4] = 6*E*I_y/l**2
    K_e[6, 6] = 12*E*I_y/l**3
    K_e[6, 10] = 6*E*I_y/l**2
    K_e[7, 1] = -12*E*I_x/l**3
    K_e[7, 3] = -6*E*I_x/l**2
    K_e[7, 7] = 12*E*I_x/l**3
    K_e[7, 9] = -6*E*I_x/l**2
    K_e[8, 2] = -A*E/l
    K_e[8, 8] = A*E/l
    K_e[9, 1] = 6*E*I_x/l**2
    K_e[9, 3] = 2*E*I_x/l
    K_e[9, 7] = -6*E*I_x/l**2
    K_e[9, 9] = 4*E*I_x/l
    K_e[10, 0] = -6*E*I_y/l**2
    K_e[10, 4] = 2*E*I_y/l
    K_e[10, 6] = 6*E*I_y/l**2
    K_e[10, 10] = 4*E*I_y/l
    K_e[11, 5] = -G*J/l
    K_e[11, 11] = G*J/l
    
    M_e = np.zeros((12, 12), dtype=float)
    M_e[0, 0] = (13/35)*A*l*rho + (6/5)*I_y*rho/l
    M_e[0, 4] = -11/210*A*l**2*rho - 1/10*I_y*rho
    M_e[0, 6] = (9/70)*A*l*rho - 6/5*I_y*rho/l
    M_e[0, 10] = (13/420)*A*l**2*rho - 1/10*I_y*rho
    M_e[1, 1] = (13/35)*A*l*rho + (6/5)*I_x*rho/l
    M_e[1, 3] = (11/210)*A*l**2*rho + (1/10)*I_x*rho
    M_e[1, 7] = (9/70)*A*l*rho - 6/5*I_x*rho/l
    M_e[1, 9] = -13/420*A*l**2*rho + (1/10)*I_x*rho
    M_e[2, 2] = (1/3)*A*l*rho
    M_e[2, 8] = (1/6)*A*l*rho
    M_e[3, 1] = (11/210)*A*l**2*rho + (1/10)*I_x*rho
    M_e[3, 3] = (1/105)*A*l**3*rho + (2/15)*I_x*l*rho
    M_e[3, 7] = (13/420)*A*l**2*rho - 1/10*I_x*rho
    M_e[3, 9] = -1/140*A*l**3*rho - 1/30*I_x*l*rho
    M_e[4, 0] = -11/210*A*l**2*rho - 1/10*I_y*rho
    M_e[4, 4] = (1/105)*A*l**3*rho + (2/15)*I_y*l*rho
    M_e[4, 6] = -13/420*A*l**2*rho + (1/10)*I_y*rho
    M_e[4, 10] = -1/140*A*l**3*rho - 1/30*I_y*l*rho
    M_e[5, 5] = (1/3)*J*l*rho
    M_e[5, 11] = (1/6)*J*l*rho
    M_e[6, 0] = (9/70)*A*l*rho - 6/5*I_y*rho/l
    M_e[6, 4] = -13/420*A*l**2*rho + (1/10)*I_y*rho
    M_e[6, 6] = (13/35)*A*l*rho + (6/5)*I_y*rho/l
    M_e[6, 10] = (11/210)*A*l**2*rho + (1/10)*I_y*rho
    M_e[7, 1] = (9/70)*A*l*rho - 6/5*I_x*rho/l
    M_e[7, 3] = (13/420)*A*l**2*rho - 1/10*I_x*rho
    M_e[7, 7] = (13/35)*A*l*rho + (6/5)*I_x*rho/l
    M_e[7, 9] = -11/210*A*l**2*rho - 1/10*I_x*rho
    M_e[8, 2] = (1/6)*A*l*rho
    M_e[8, 8] = (1/3)*A*l*rho
    M_e[9, 1] = -13/420*A*l**2*rho + (1/10)*I_x*rho
    M_e[9, 3] = -1/140*A*l**3*rho - 1/30*I_x*l*rho
    M_e[9, 7] = -11/210*A*l**2*rho - 1/10*I_x*rho
    M_e[9, 9] = (1/105)*A*l**3*rho + (2/15)*I_x*l*rho
    M_e[10, 0] = (13/420)*A*l**2*rho - 1/10*I_y*rho
    M_e[10, 4] = -1/140*A*l**3*rho - 1/30*I_y*l*rho
    M_e[10, 6] = (11/210)*A*l**2*rho + (1/10)*I_y*rho
    M_e[10, 10] = (1/105)*A*l**3*rho + (2/15)*I_y*l*rho
    M_e[11, 5] = (1/6)*J*l*rho
    M_e[11, 11] = (1/3)*J*l*rho
    
    return M_e, K_e

def concentrated_inertia_2D(m, I, r=0.):

    # Offset, parallel axis theorem    
    I += m*(r**2)
    
    M_e = np.diag([m, m, I])
    K_e = np.zeros(M_e.shape)
    
    return M_e, K_e

def freq_h(M, K, n_m=20):

    t0 = time()
    vals, vecs = ssl.eigsh(A=M, k=n_m, M=K)
    f_n = 1./(2.*np.pi) * np.sqrt(1./vals)
    idx = f_n.argsort()
    f_n = f_n[idx]
    phi = vecs[:, idx]
    t1 = time()
    print(t1-t0)
    
    for i_m in range(phi.shape[1]):
        phi[:, i_m] = phi[:, i_m] / sa.norm(phi[:, i_m])
    
    return f_n, phi

def pvet(a):
    return a.reshape((a.size, 1))

#%%

file_name = '../../../Craig-Bampton/comsol_example/shell_tower.mat'
sys = load_from_livelink(file_name)

sys.Mc = sys.Nf.T @ sys.M @ sys.N
sys.Kc = sys.Nf.T @ sys.K @ sys.N

# sys.f_n, _ = freq_h(sys.Mc, sys.Kc)

#%% Constraint the bottom, rearranging the matrix order

bot = np.array([i_n for i_n in range(sys.n_nodes) if sys.coord[i_n, 2]<=0.000001])
top = np.array([i_n for i_n in range(sys.n_nodes) if sys.coord[i_n, 2]>=1.999999])

# Constraint DOF at the bottom
qc = np.zeros((sys.n_dofs, ))
qc[sys.dof[bot, :].flatten()] = 1.
qc = sys.Nf.T @ qc
qc_bool = np.zeros(qc.shape, dtype=bool)
qc_bool[qc!=0] = True

# DOFs at the boundary
qb = np.zeros((sys.n_dofs, ))
qb[sys.dof[top, -3:].flatten()] = 1.
qb = sys.Nf.T @ qb
qb_bool = np.zeros(qb.shape, dtype=bool)
qb_bool[qb!=0] = True
qi_bool = ~qb_bool

qb_bool = qb_bool * ~qc_bool
qi_bool = qi_bool * ~qc_bool

n_dofs = sys.Nf.shape[1]
q_idx = np.arange(n_dofs)
qr_idx = np.concatenate((q_idx[qb_bool], q_idx[qi_bool]))

Nr = boolean_matrix(q_idx, qr_idx)
Mr = Nr.T @ sys.Mc @ Nr
Kr = Nr.T @ sys.Kc @ Nr

f_n, phi = freq_h(Mr, Kr)

#%% Constraint the top

# Forget the original dof numbering, embrace the new one

n_b = q_idx[qb_bool].size
n_i = q_idx[qi_bool].size
n_c = 6

cnt = empty_object()
cnt.r = sys.coord[top, :]
cnt.r_c = sys.coord[top, :].mean(axis=0)
cnt.dof = np.arange(n_b).reshape((int(n_b/3), 3))

n_elms = top.shape[0]
n_dofs = cnt.dof.size
A = np.zeros((n_dofs, 6))
for i_e in range(n_elms):
    Ae, _ = rigid_string(cnt.r[i_e], cnt.r_c)
    i_idx = cnt.dof[i_e, :]
    j_idx = np.arange(6)

    A[np.ix_(i_idx, j_idx)] = A[np.ix_(i_idx, j_idx)] + Ae

A = ss.coo_matrix(A)
I = ss.eye(n_i, format="coo")
O_bi = ss.coo_matrix((n_b, n_i))
O_ic = ss.coo_matrix((n_i, n_c))

N = ss.csr_matrix(ss.bmat([[A, O_bi],[O_ic, I]]))
Mp = N.T @ Mr @ N
Kp = N.T @ Kr @ N

fp_n, phi_p = freq_h(Mp, Kp, 40)

# Storing the tower into an object
tw = empty_object()
tw.M = Mp
tw.K = Kp
# tw.N = Nr


#%% Printing mode shapes

i_m = 0
# u = np.zeros((sys.dof.shape[0], ))
# v = np.zeros((sys.dof.shape[0], ))
# w = np.zeros((sys.dof.shape[0], ))
u = sys.Nf @ Nr @ N @ phi_p[:, i_m]*50

ux = u[sys.dof[:, 3]]
vy = u[sys.dof[:, 4]]
wz = u[sys.dof[:, 5]]

x = sys.coord[:, 0] + ux
y = sys.coord[:, 1] + vy
z = sys.coord[:, 2] + wz

s = np.ones(z.shape)
# triangles = sys.tri

fig = mlab.figure()
mlab.points3d(x, y, z, scale_factor=0.01)
# mlab.triangular_mesh(x, y, z, triangles)

#%% Model order reduction

Mbb = Mp[:n_c, :n_c]
Mii = Mp[n_c:, n_c:]
Mib = Mp[n_c:, :n_c]
Mbi = Mp[:n_c, n_c:]

Kbb = Kp[:n_c, :n_c]
Kii = Kp[n_c:, n_c:]
Kib = Kp[n_c:, :n_c]
Kbi = Kp[:n_c, n_c:]

# Constraint mode shapes
phi_c = ssl.spsolve(-Kii, Kib)

# Normal mode shapes
vals, vecs = freq_h(Mii, Kii, 40)
n_m = 12
phi_n = ss.csr_matrix(vecs[:, :n_m])

n_b = phi_c.shape[1]
I = ss.csr_matrix(np.eye(n_b))
O = ss.csr_matrix(np.zeros((n_b, n_m)))
alpha_1 = ss.bmat([[I, O], [phi_c, phi_n]], format='csr')

t_0 = time()
M_1p = alpha_1.T @ Mp @ alpha_1
K_1p = alpha_1.T @ Kp @ alpha_1
t_1 = time()
print(t_1 - t_0)

f1p_n, phi_1p = freq_h(M_1p, K_1p, K_1p.shape[0]-1)

#%% Printing the mode shapes using the reduced modes

i_m = 0
u = sys.Nf @ Nr @ N @ alpha_1 @ phi_1p[:, i_m]*50

ux = u[sys.dof[:, 3]]
vy = u[sys.dof[:, 4]]
wz = u[sys.dof[:, 5]]

x = sys.coord[:, 0] + ux
y = sys.coord[:, 1] + vy
z = sys.coord[:, 2] + wz

fig = mlab.figure()
mlab.points3d(x, y, z, scale_factor=0.01)

# mlab.triangular_mesh(x, y, z, triangles)

#%% Tower top (Tp)

tp = empty_object()
tp.rho = 8500.
tp.E = 210.*10**9
tp.nu = 0.3
tp.G = tp.E/(2.*(1.+tp.nu))

tp.d_out = 0.1
tp.d_in = 0.06
tp.A = (np.pi/4.)*(tp.d_out**2 - tp.d_in**2)
tp.I = (np.pi/64.)*(tp.d_out**4 - tp.d_in**4)

tp.n_elms = 18
tp.n_nodes = tp.n_elms+1
tp.n_dpn = 6

tp.l = 2. / tp.n_elms
tp.J = 2.*tp.I

tp.nodes = np.array([[i_e, i_e+1] for i_e in range(tp.n_elms)])
tp.dofs = np.zeros((tp.n_nodes, tp.n_dpn), dtype=int)
# tp.dofs[0, :] = -1

# DOFs
k = 0
for i in range(tp.dofs.shape[0]):
    for j in range(tp.dofs.shape[1]):
        if (tp.dofs[i, j] != -1):
            tp.dofs[i, j], k = k, k+1

tp.n_dofs = int(np.max(tp.dofs))+1
tp.q = np.arange(tp.n_dofs)
tp.M = np.zeros((tp.n_dofs, tp.n_dofs))
tp.K = np.zeros((tp.n_dofs, tp.n_dofs))

for i_e in range(tp.n_elms):
    n0, n1 = [*tp.nodes[i_e, :]]
    q_e = np.concatenate((tp.dofs[n0, :], tp.dofs[n1, :]))
    L_e = boolean_matrix(q_e, tp.q)

    M_e, K_e = beam_3D(tp.G, tp.I, tp.J, tp.A, tp.rho, tp.I, tp.l, tp.E)
    tp.M += L_e.T @ M_e @ L_e
    tp.K += L_e.T @ K_e @ L_e

tp.M = ss.csr_matrix(tp.M)
tp.K = ss.csr_matrix(tp.K)

# ftp_n, _ = freq_h(tp.M, tp.K, 20)


#%% Joining the tower-top-nacelle with the full rearanged tower

nh = empty_object
nh.q = np.arange(tw.M.shape[0] + tp.M.shape[0] - 6, dtype=int)

nh.q1 = np.arange(tw.M.shape[0], dtype=int)
nh.q2 = np.zeros(tp.M.shape[0], dtype=int)
nh.q2[:6] = nh.q[:6]
nh.q2[6:] = nh.q[-(tp.M.shape[0] - 6):]

nh.L1 = boolean_matrix(nh.q1, nh.q)
nh.L2 = boolean_matrix(nh.q2, nh.q)

nh.M = nh.L1.T @ tw.M @ nh.L1 + nh.L2.T @ tp.M @ nh.L2
nh.K = nh.L1.T @ tw.K @ nh.L1 + nh.L2.T @ tp.K @ nh.L2

freq, _ = freq_h(nh.M, nh.K)

# jo = empty_object()
# jo.q = np.arange(tw.M.shape[0] + tp.M.shape[0] - 6)

# q1 = np.arange(tw.M.shape[0])
# q2 = np.arange(tp.M.shape[0]) + (tw.M.shape[0] - 6)
# q2[:6] = jo.q[:6]
# L1 = boolean_matrix(q1, jo.q)
# L2 = boolean_matrix(q2, jo.q)
# jo.M = L1.T @ tw.M @ L1 + L2.T @ tp.M @ L2
# jo.K = L1.T @ tw.K @ L1 + L2.T @ tp.K @ L2

# toto, _ = freq_h(tw.M, tw.K)
