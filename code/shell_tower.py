import numpy as np
import scipy.io as sio
import scipy.linalg as sa
import scipy.sparse as ss
import scipy.sparse.linalg as ssl
import copy
from numba import jit
from mayavi import mlab
import custom_functions as cf

from time import time

#%%

gen_plots = True

#%%

# Empty class
class empty_object(object):
    def copy(self):
        return copy.deepcopy(self)

# Loading Matlab data
def load_from_livelink(file_name): # return: nodes, sys
    toto = sio.loadmat(file_name)
    
    sys = empty_object()
    sys.coord = toto['nodes'][0][0][0]
    sys.node = toto['nodes'][0][0][1][0] - 1
    sys.dof = toto['nodes'][0][0][2]
    
    sys.n_nodes = sys.node.size
    sys.n_dofs = sys.dof.size
    
    sys.quad = toto['quad'][0][0][0]
    
    sys.M = (toto['sys'][0][0][0])
    sys.K = (toto['sys'][0][0][1])
    sys.D = (toto['sys'][0][0][2])
    sys.C = (toto['sys'][0][0][3])
    sys.L = (toto['sys'][0][0][4][0])
    sys.N = (toto['sys'][0][0][5])
    sys.Nf = (toto['sys'][0][0][6])
    sys.Mc = (toto['sys'][0][0][7])
    sys.Kc = (toto['sys'][0][0][8])
        
    return sys

@jit
def boolean_matrix(a, b):
    
    # Get matrix sparsity
    n = 0
    for i in range(a.size):
        for j in range(b.size):
            if (a[i]==b[j]):
                n += 1
    
    data = np.ones((n,), dtype=int)
    idx = np.zeros((n,), dtype=int)
    jdx = np.zeros((n,), dtype=int)
    n = 0
    for i in range(a.size):
        for j in range(b.size):
            if (a[i]==b[j]):
                idx[n] = i
                jdx[n] = j
                n += 1
    
    L = ss.csr_matrix((data, (idx, jdx)), shape=(a.size, b.size))
    
    return L

def rigid_string(r, r_c):
    
    x_i, y_i, z_i = r
    x_c, y_c, z_c = r_c
    
    M_e = np.zeros((3, 6), dtype=float)
    M_e[0, 0] = 1
    M_e[0, 4] = -z_c + z_i
    M_e[0, 5] = y_c - y_i
    M_e[1, 1] = 1
    M_e[1, 3] = z_c - z_i
    M_e[1, 5] = -x_c + x_i
    M_e[2, 2] = 1
    M_e[2, 3] = -y_c + y_i
    M_e[2, 4] = x_c - x_i


    B_e = np.zeros((3, 9), dtype=float)
    B_e[0, 0] = 1
    B_e[0, 4] = -z_c + z_i
    B_e[0, 5] = y_c - y_i
    B_e[0, 6] = -1
    B_e[1, 1] = 1
    B_e[1, 3] = z_c - z_i
    B_e[1, 5] = -x_c + x_i
    B_e[1, 7] = -1
    B_e[2, 2] = 1
    B_e[2, 3] = -y_c + y_i
    B_e[2, 4] = x_c - x_i
    B_e[2, 8] = -1
        
    return M_e, B_e

def beam_3D(rho, A, l, E, G, I_y, I_z, J):
    
    K_e = np.zeros((12, 12), dtype=float)
    K_e[0, 0] = A*E/l
    K_e[0, 6] = -A*E/l
    K_e[1, 1] = 12*E*I_z/l**3
    K_e[1, 5] = 6*E*I_z/l**2
    K_e[1, 7] = -12*E*I_z/l**3
    K_e[1, 11] = 6*E*I_z/l**2
    K_e[2, 2] = 12*E*I_y/l**3
    K_e[2, 4] = -6*E*I_y/l**2
    K_e[2, 8] = -12*E*I_y/l**3
    K_e[2, 10] = -6*E*I_y/l**2
    K_e[3, 3] = G*J/l
    K_e[3, 9] = -G*J/l
    K_e[4, 2] = -6*E*I_y/l**2
    K_e[4, 4] = 4*E*I_y/l
    K_e[4, 8] = 6*E*I_y/l**2
    K_e[4, 10] = 2*E*I_y/l
    K_e[5, 1] = 6*E*I_z/l**2
    K_e[5, 5] = 4*E*I_z/l
    K_e[5, 7] = -6*E*I_z/l**2
    K_e[5, 11] = 2*E*I_z/l
    K_e[6, 0] = -A*E/l
    K_e[6, 6] = A*E/l
    K_e[7, 1] = -12*E*I_z/l**3
    K_e[7, 5] = -6*E*I_z/l**2
    K_e[7, 7] = 12*E*I_z/l**3
    K_e[7, 11] = -6*E*I_z/l**2
    K_e[8, 2] = -12*E*I_y/l**3
    K_e[8, 4] = 6*E*I_y/l**2
    K_e[8, 8] = 12*E*I_y/l**3
    K_e[8, 10] = 6*E*I_y/l**2
    K_e[9, 3] = -G*J/l
    K_e[9, 9] = G*J/l
    K_e[10, 2] = -6*E*I_y/l**2
    K_e[10, 4] = 2*E*I_y/l
    K_e[10, 8] = 6*E*I_y/l**2
    K_e[10, 10] = 4*E*I_y/l
    K_e[11, 1] = 6*E*I_z/l**2
    K_e[11, 5] = 2*E*I_z/l
    K_e[11, 7] = -6*E*I_z/l**2
    K_e[11, 11] = 4*E*I_z/l
    
    M_e = np.zeros((12, 12), dtype=float)
    M_e[0, 0] = (1/3)*A*l*rho
    M_e[0, 6] = (1/6)*A*l*rho
    M_e[1, 1] = (13/35)*A*l*rho
    M_e[1, 5] = (11/210)*A*l**2*rho
    M_e[1, 7] = (9/70)*A*l*rho
    M_e[1, 11] = -13/420*A*l**2*rho
    M_e[2, 2] = (13/35)*A*l*rho
    M_e[2, 4] = -11/210*A*l**2*rho
    M_e[2, 8] = (9/70)*A*l*rho
    M_e[2, 10] = (13/420)*A*l**2*rho
    M_e[3, 3] = (1/3)*J*l*rho
    M_e[3, 9] = (1/6)*J*l*rho
    M_e[4, 2] = -11/210*A*l**2*rho
    M_e[4, 4] = (1/105)*A*l**3*rho
    M_e[4, 8] = -13/420*A*l**2*rho
    M_e[4, 10] = -1/140*A*l**3*rho
    M_e[5, 1] = (11/210)*A*l**2*rho
    M_e[5, 5] = (1/105)*A*l**3*rho
    M_e[5, 7] = (13/420)*A*l**2*rho
    M_e[5, 11] = -1/140*A*l**3*rho
    M_e[6, 0] = (1/6)*A*l*rho
    M_e[6, 6] = (1/3)*A*l*rho
    M_e[7, 1] = (9/70)*A*l*rho
    M_e[7, 5] = (13/420)*A*l**2*rho
    M_e[7, 7] = (13/35)*A*l*rho
    M_e[7, 11] = -11/210*A*l**2*rho
    M_e[8, 2] = (9/70)*A*l*rho
    M_e[8, 4] = -13/420*A*l**2*rho
    M_e[8, 8] = (13/35)*A*l*rho
    M_e[8, 10] = (11/210)*A*l**2*rho
    M_e[9, 3] = (1/6)*J*l*rho
    M_e[9, 9] = (1/3)*J*l*rho
    M_e[10, 2] = (13/420)*A*l**2*rho
    M_e[10, 4] = -1/140*A*l**3*rho
    M_e[10, 8] = (11/210)*A*l**2*rho
    M_e[10, 10] = (1/105)*A*l**3*rho
    M_e[11, 1] = -13/420*A*l**2*rho
    M_e[11, 5] = -1/140*A*l**3*rho
    M_e[11, 7] = -11/210*A*l**2*rho
    M_e[11, 11] = (1/105)*A*l**3*rho
    
    return M_e, K_e

def concentrated_inertia_2D(m, I, r=0.):

    # Offset, parallel axis theorem    
    I += m*(r**2)
    
    M_e = np.diag([m, m, I])
    K_e = np.zeros(M_e.shape)
    
    return M_e, K_e

def freq_h(M, K, n_m=20):

    t0 = time()
    vals, vecs = ssl.eigsh(A=M, k=n_m, M=K)
    f_n = 1./(2.*np.pi) * np.sqrt(1./vals)
    idx = f_n.argsort()
    f_n = f_n[idx]
    phi = vecs[:, idx]
    t1 = time()
    print(t1-t0)
    
    for i_m in range(phi.shape[1]):
        phi[:, i_m] = phi[:, i_m] / sa.norm(phi[:, i_m])
    
    return f_n, phi

def pvet(a):
    return a.reshape((a.size, 1))

#%%

file_name = '../../../Craig-Bampton/comsol_example/shell_tower.mat'
sys = load_from_livelink(file_name)

sys.Mc = sys.Nf.T @ sys.M @ sys.N
sys.Kc = sys.Nf.T @ sys.K @ sys.N

#%% Constraint the bottom, rearranging the matrix order

bot = np.array([i_n for i_n in range(sys.n_nodes) if sys.coord[i_n, 2]<=0.])
top = np.array([i_n for i_n in range(sys.n_nodes) if sys.coord[i_n, 2]>=115.63])

# Constraint DOF at the bottom
qc = np.zeros((sys.n_dofs, ))
qc[sys.dof[bot, :].flatten()] = 1.
qc = sys.Nf.T @ qc
qc_bool = np.zeros(qc.shape, dtype=bool)
qc_bool[qc!=0] = True

# DOFs at the boundary
qb = np.zeros((sys.n_dofs, ))
qb[sys.dof[top, -3:].flatten()] = 1.
qb = sys.Nf.T @ qb
qb_bool = np.zeros(qb.shape, dtype=bool)
qb_bool[qb!=0] = True
qi_bool = ~qb_bool

qb_bool = qb_bool * ~qc_bool
qi_bool = qi_bool * ~qc_bool

n_dofs = sys.Nf.shape[1]
q_idx = np.arange(n_dofs)
qr_idx = np.concatenate((q_idx[qb_bool], q_idx[qi_bool]))

Nr = boolean_matrix(q_idx, qr_idx)
Mr = Nr.T @ sys.Mc @ Nr
Kr = Nr.T @ sys.Kc @ Nr

f_n, phi = freq_h(Mr, Kr)

#%% Constraint the top

# Forget the original dof numbering, embrace the new one
n_b = q_idx[qb_bool].size
n_i = q_idx[qi_bool].size
n_c = 6

cnt = empty_object()
cnt.r = sys.coord[top, :]
cnt.r_c = sys.coord[top, :].mean(axis=0)
cnt.dof = np.arange(n_b).reshape((int(n_b/3), 3))

n_elms = top.shape[0]
n_dofs = cnt.dof.size
A = np.zeros((n_dofs, 6))
for i_e in range(n_elms):
    Ae, _ = rigid_string(cnt.r[i_e], cnt.r_c)
    i_idx = cnt.dof[i_e, :]
    j_idx = np.arange(6)

    A[np.ix_(i_idx, j_idx)] = A[np.ix_(i_idx, j_idx)] + Ae

A = ss.coo_matrix(A)
I = ss.eye(n_i, format="coo")
O_bi = ss.coo_matrix((n_b, n_i))
O_ic = ss.coo_matrix((n_i, n_c))

N = ss.csr_matrix(ss.bmat([[A, O_bi],[O_ic, I]]))
Mp = N.T @ Mr @ N
Kp = N.T @ Kr @ N

fp_n, phi_p = freq_h(Mp, Kp, 40)

# Storing the tower into an object
tw = empty_object()
tw.Mp = Mp
tw.Kp = Kp

#%% Printing mode shapes

i_m = 6
# u = np.zeros((sys.dof.shape[0], ))
# v = np.zeros((sys.dof.shape[0], ))
# w = np.zeros((sys.dof.shape[0], ))
u = sys.Nf @ Nr @ N @ phi_p[:, i_m]*200

ux = u[sys.dof[:, 3]]
vy = u[sys.dof[:, 4]]
wz = u[sys.dof[:, 5]]

x = sys.coord[:, 0] + ux
y = sys.coord[:, 1] + vy
z = sys.coord[:, 2] + wz

s = np.ones(z.shape)
# triangles = sys.tri

# if (gen_plots):
#     fig = mlab.figure()
#     mlab.points3d(x, y, z, scale_factor=1.)
    # mlab.triangular_mesh(x, y, z, triangles)

#%% Model order reduction

Mbb = Mp[:n_c, :n_c]
Mii = Mp[n_c:, n_c:]
Mib = Mp[n_c:, :n_c]
Mbi = Mp[:n_c, n_c:]

Kbb = Kp[:n_c, :n_c]
Kii = Kp[n_c:, n_c:]
Kib = Kp[n_c:, :n_c]
Kbi = Kp[:n_c, n_c:]

# Constraint mode shapes
phi_c = ssl.spsolve(-Kii, Kib)

# Normal mode shapes
vals, vecs = freq_h(Mii, Kii, 40)
n_m = 24
phi_n = ss.csr_matrix(vecs[:, :n_m])

n_b = phi_c.shape[1]
I = ss.csr_matrix(np.eye(n_b))
O = ss.csr_matrix(np.zeros((n_b, n_m)))
alpha_1 = ss.bmat([[I, O], [phi_c, phi_n]], format='csr')

t_0 = time()
M_1p = alpha_1.T @ Mp @ alpha_1
K_1p = alpha_1.T @ Kp @ alpha_1
t_1 = time()
print(t_1 - t_0)

f1p_n, phi_1p = freq_h(M_1p, K_1p, K_1p.shape[0]-1)

tw.Mr = M_1p
tw.Kr = K_1p

#%% Printing the mode shapes using the reduced modes

i_m = 6
u = sys.Nf @ Nr @ N @ alpha_1 @ phi_1p[:, i_m]*200

ux = u[sys.dof[:, 3]]
vy = u[sys.dof[:, 4]]
wz = u[sys.dof[:, 5]]

x = sys.coord[:, 0] + ux
y = sys.coord[:, 1] + vy
z = sys.coord[:, 2] + wz

if (gen_plots):
    fig = mlab.figure()
    mlab.points3d(x, y, z, scale_factor=1.)    
    # mlab.triangular_mesh(x, y, z, triangles)

#%% Tower top (Tp)

tp = empty_object()
table = np.loadtxt('DTU_10MW_RWT_Towertop_st.dat', skiprows=3, max_rows=2)

tp.n_elms = int(table.shape[0]/2)
tp.n_nodes = tp.n_elms+1
tp.n_dpn = 6

tp.l = table[1, 0] / tp.n_elms
tp.rho = table[0, 1] / table[0, 15]
tp.A = table[0, 15]
tp.E = table[0, 8]
tp.G = table[0, 9]
tp.I = table[0, 10]
tp.J = table[0, 12]



tp.nodes = np.array([[i_e, i_e+1] for i_e in range(tp.n_elms)])
tp.dofs = np.zeros((tp.n_nodes, tp.n_dpn), dtype=int)
# tp.dofs[0, :] = -1

# DOFs
k = 0
for i in range(tp.dofs.shape[0]):
    for j in range(tp.dofs.shape[1]):
        if (tp.dofs[i, j] != -1):
            tp.dofs[i, j], k = k, k+1

tp.n_dofs = int(np.max(tp.dofs))+1
tp.q = np.arange(tp.n_dofs)
tp.M = np.zeros((tp.n_dofs, tp.n_dofs))
tp.K = np.zeros((tp.n_dofs, tp.n_dofs))

for i_e in range(tp.n_elms):
    n0, n1 = [*tp.nodes[i_e, :]]
    q_e = np.concatenate((tp.dofs[n0, :], tp.dofs[n1, :]))
    L_e = boolean_matrix(q_e, tp.q)

    M_e, K_e = beam_3D(tp.rho, tp.A, tp.l, tp.E, tp.G, tp.I, tp.I, tp.J)
    tp.M += L_e.T @ M_e @ L_e
    tp.K += L_e.T @ K_e @ L_e

# toto, _ = freq_h(tp.M, tp.K, n_m=5)


#%% Nacelle (nc)

nc = empty_object()
nc.m = 4.4604E+05
nc.r = np.array([0., -2.6870E+00, -3.0061E-01])

nc.I = np.diag([4.1060E+06, 4.1060E+05, 4.1060E+06])
nc.J = nc.I + nc.m*((nc.r @ nc.r)*np.eye(3) - np.tensordot(nc.r, nc.r, axes=0))
# nc.J = np.zeros(nc.J.shape)

nc.M = np.block([[nc.m*np.eye(3), np.zeros((3, 3))], [np.zeros((3, 3)), nc.J]])
nc.K = np.zeros(nc.M.shape)
nc.q = np.arange(6)


# nc.M = np.zeros(nc.M.shape)

# nc.M, nc.K = concentrated_inertia_2D(nc.m, nc.I, r=np.linalg.norm(nc.offset))

#%% Joining the tower-top and the concentrated inertia in one single object

tpn = empty_object()
tpn.q = tp.q
q_n = tp.q[-6:]
Ln = boolean_matrix(q_n, tpn.q)

tpn.M = ss.csr_matrix(tp.M + Ln.T @ nc.M @ Ln)
tpn.K = ss.csr_matrix(tp.K + Ln.T @ nc.K @ Ln)

#%% Joining the tower-top-nacelle with the full rearanged tower

jf = empty_object()
jf.q = np.arange(tw.Mp.shape[0] + tpn.M.shape[0] - 6)

q1 = np.arange(tw.Mp.shape[0])
q2 = np.concatenate((jf.q[:6], jf.q[-6:]))
L1 = boolean_matrix(q1, jf.q)
L2 = boolean_matrix(q2, jf.q)
jf.M = L1.T @ tw.Mp @ L1 + L2.T @ tpn.M @ L2
jf.K = L1.T @ tw.Kp @ L1 + L2.T @ tpn.K @ L2

ff_n, phi_ff = freq_h(jf.M, jf.K, 40)

#%% Mode shapes

if (gen_plots):
    mode = 0
    for mode in range(20):
        p = phi_ff[:, mode]*300
        u_1 = sys.Nf @ Nr @ N @ L1 @ p
        
        ux = u_1[sys.dof[:, 3]]
        vy = u_1[sys.dof[:, 4]]
        wz = u_1[sys.dof[:, 5]]
        
        x1 = sys.coord[:, 0] + ux
        y1 = sys.coord[:, 1] + vy
        z1 = sys.coord[:, 2] + wz
        
        # fig = mlab.figure()
        # mlab.points3d(x1, y1, z1, scale_factor=1.)
        
        u_2 = L2 @ p
        ux = u_2[tp.dofs[:, 0]]
        vy = u_2[tp.dofs[:, 1]]
        wz = u_2[tp.dofs[:, 2]]
    
        x2 = ux
        y2 = vy
        z2 = np.array([115.63, 118.38]) + wz
        
        x = np.concatenate((x1, x2))
        y = np.concatenate((y1, y2))
        z = np.concatenate((z1, z2))
        
        fig = mlab.figure()
        mlab.points3d(x, y, z, scale_factor=1.)
        mlab.savefig(filename='../figures/tower_mode_%i.eps'%(mode))
        

#%% #%% Joining the tower-top-nacelle with the reduced tower

jr = empty_object()
jr.q = np.arange(tw.Mr.shape[0] + tpn.M.shape[0] - 6)

q1 = np.arange(tw.Mr.shape[0])
q2 = np.concatenate((jr.q[:6], jr.q[-6:]))
L1 = boolean_matrix(q1, jr.q)
L2 = boolean_matrix(q2, jr.q)
jr.M = L1.T @ tw.Mr @ L1 + L2.T @ tpn.M @ L2
jr.K = L1.T @ tw.Kr @ L1 + L2.T @ tpn.K @ L2

fr_n, phi_fr = freq_h(jr.M, jr.K, 20)

#%% Export table

ff_n, phi_ff = freq_h(jf.M, jf.K, 20)

error = np.abs(fr_n-ff_n)/ff_n * 100

comsol = np.loadtxt('model_2_comsol.txt', usecols=0)[:fr_n.size]

num2str = np.vectorize(cf.num2str)

filename = '../tables/ex_2.tex'
M = num2str(np.block([[comsol], [ff_n], [fr_n], [error]]).T, '%0.4e')
V = num2str(np.arange(fr_n.size) + 1, '%i')
H = np.array(['COMSOL [Hz]','Model [Hz]', 'Craig-Bampton [Hz]', 'Error [\%]'])
corner = 'Mode'
cf.latex_table(filename, M, H, V, corner)
