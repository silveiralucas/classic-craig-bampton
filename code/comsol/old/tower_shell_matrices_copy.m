%
% tower_shell_matrices.m
%
% Model exported on Sep 14 2020, 14:21 by COMSOL 5.5.0.359.

import com.comsol.model.*
import com.comsol.model.util.*

model = ModelUtil.create('Model');

model.modelPath('C:\Users\silveiralucas\OneDrive - Syddansk Universitet\Craig-Bampton\comsol_example');

model.component.create('comp1', true);

model.component('comp1').geom.create('geom1', 3);

model.component('comp1').mesh.create('mesh1');

model.component('comp1').physics.create('shell', 'Shell', 'geom1');

model.study.create('std1');
model.study('std1').create('stat', 'Stationary');
model.study('std1').feature('stat').activate('shell', true);

model.component('comp1').geom('geom1').create('cone1', 'Cone');
model.component('comp1').geom('geom1').feature('cone1').set('specifytop', 'radius');
model.component('comp1').geom('geom1').feature('cone1').set('r', '8.3/2');
model.component('comp1').geom('geom1').feature('cone1').set('h', 11.5);
model.component('comp1').geom('geom1').feature('cone1').set('rtop', '8.0215/2');
model.component('comp1').geom('geom1').run('cone1');
model.component('comp1').geom('geom1').feature.duplicate('cone2', 'cone1');
model.component('comp1').geom('geom1').feature('cone2').set('r', '8.0215/2');
model.component('comp1').geom('geom1').feature('cone2').set('rtop', '7.7430/2');
model.component('comp1').geom('geom1').feature('cone2').set('pos', [0 0 11.5]);
model.component('comp1').geom('geom1').run('cone2');
model.component('comp1').geom('geom1').feature.duplicate('cone3', 'cone2');
model.component('comp1').geom('geom1').feature('cone3').set('r', '7.7430/2');
model.component('comp1').geom('geom1').feature('cone3').set('rtop', '7.4646/2');
model.component('comp1').geom('geom1').feature('cone3').set('pos', {'0' '0' '23.0'});
model.component('comp1').geom('geom1').run('cone3');
model.component('comp1').geom('geom1').feature.duplicate('cone4', 'cone3');
model.component('comp1').geom('geom1').feature('cone4').set('r', '7.4646/2');
model.component('comp1').geom('geom1').feature('cone4').set('rtop', '7.1861/2');
model.component('comp1').geom('geom1').feature('cone4').set('pos', [0 0 34.5]);
model.component('comp1').geom('geom1').run('cone4');
model.component('comp1').geom('geom1').feature.duplicate('cone5', 'cone4');
model.component('comp1').geom('geom1').feature('cone5').set('r', '7.1861/2');
model.component('comp1').geom('geom1').feature('cone5').set('rtop', '6.9076/2');
model.component('comp1').geom('geom1').feature('cone5').set('pos', {'0' '0' '46.0'});
model.component('comp1').geom('geom1').run('cone5');
model.component('comp1').geom('geom1').feature.duplicate('cone6', 'cone5');
model.component('comp1').geom('geom1').feature('cone6').set('r', '6.9076/2');
model.component('comp1').geom('geom1').feature('cone6').set('rtop', '6.6291/2');
model.component('comp1').geom('geom1').feature('cone6').set('pos', [0 0 57.5]);
model.component('comp1').geom('geom1').run('cone6');
model.component('comp1').geom('geom1').feature.duplicate('cone7', 'cone6');
model.component('comp1').geom('geom1').feature('cone7').set('r', '6.6291/2');
model.component('comp1').geom('geom1').feature('cone7').set('rtop', '6.3507/2');
model.component('comp1').geom('geom1').feature('cone7').set('pos', {'0' '0' '69.0'});
model.component('comp1').geom('geom1').run('cone7');
model.component('comp1').geom('geom1').feature.duplicate('cone8', 'cone7');
model.component('comp1').geom('geom1').feature('cone8').set('r', '6.3507/2');
model.component('comp1').geom('geom1').feature('cone8').set('rtop', '6.0722/2');
model.component('comp1').geom('geom1').feature('cone8').set('pos', [0 0 80.5]);
model.component('comp1').geom('geom1').run('cone8');
model.component('comp1').geom('geom1').feature.duplicate('cone9', 'cone8');
model.component('comp1').geom('geom1').feature('cone9').set('r', '6.0722/2');
model.component('comp1').geom('geom1').feature('cone9').set('rtop', '5.7937/2');
model.component('comp1').geom('geom1').feature('cone9').set('pos', {'0' '0' '92.0'});
model.component('comp1').geom('geom1').run('cone9');
model.component('comp1').geom('geom1').feature.duplicate('cone10', 'cone9');
model.component('comp1').geom('geom1').feature('cone10').set('r', '5.7937/2');
model.component('comp1').geom('geom1').feature('cone10').set('h', 12.13);
model.component('comp1').geom('geom1').feature('cone10').set('rtop', '5.5/2');
model.component('comp1').geom('geom1').feature('cone10').set('pos', [0 0 103.5]);
model.component('comp1').geom('geom1').run('cone10');
model.component('comp1').geom('geom1').run('fin');

model.component('comp1').material.create('mat1', 'Common');
model.component('comp1').material('mat1').propertyGroup('def').set('youngsmodulus', '210.0E9');
model.component('comp1').material('mat1').propertyGroup('def').set('poissonsratio', '0.3');
model.component('comp1').material('mat1').propertyGroup('def').set('density', '8500');

model.component('comp1').physics('shell').selection.set([1 2 4 5 7 8 10 11 13 14 16 17 19 20 22 23 25 26 28 29 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51]);
model.component('comp1').physics('shell').prop('PhysicsSymbols').set('PhysicsSymbols', true);
model.component('comp1').physics('shell').feature('to1').set('d', '38.0[mm]');
model.component('comp1').physics('shell').feature('to1').set('OffsetDefinition', 'RelativeDistance');
model.component('comp1').physics('shell').feature('to1').set('z_offset_rel', -1);
model.component('comp1').physics('shell').feature.duplicate('to2', 'to1');
model.component('comp1').physics('shell').feature('to2').selection.set([4 5 33 50]);
model.component('comp1').physics('shell').feature('to2').set('d', '36.0[mm]');
model.component('comp1').physics('shell').feature.duplicate('to3', 'to2');
model.component('comp1').physics('shell').feature('to3').selection.set([7 8 34 49]);
model.component('comp1').physics('shell').feature('to3').set('d', '34.0[mm]');
model.component('comp1').physics('shell').feature.duplicate('to4', 'to3');
model.component('comp1').physics('shell').feature('to4').selection.set([10 11 35 48]);
model.component('comp1').physics('shell').feature('to4').set('d', '32.0[mm]');
model.component('comp1').physics('shell').feature.duplicate('to5', 'to4');
model.component('comp1').physics('shell').feature('to5').selection.set([13 14 36 47]);
model.component('comp1').physics('shell').feature('to5').set('d', '30.0[mm]');
model.component('comp1').physics('shell').feature.duplicate('to6', 'to5');
model.component('comp1').physics('shell').feature('to6').selection.set([16 17 37 46]);
model.component('comp1').physics('shell').feature('to6').set('d', '28.0[mm]');
model.component('comp1').physics('shell').feature.duplicate('to7', 'to6');
model.component('comp1').physics('shell').feature('to7').selection.set([19 20 38 45]);
model.component('comp1').physics('shell').feature('to7').set('d', '26.0[mm]');
model.component('comp1').physics('shell').feature.duplicate('to8', 'to7');
model.component('comp1').physics('shell').feature('to8').selection.set([22 23 39 44]);
model.component('comp1').physics('shell').feature('to8').set('d', '24.0[mm]');
model.component('comp1').physics('shell').feature.duplicate('to9', 'to8');
model.component('comp1').physics('shell').feature('to9').selection.set([25 26 40 43]);
model.component('comp1').physics('shell').feature('to9').set('d', '22.0[mm]');
model.component('comp1').physics('shell').feature.duplicate('to10', 'to9');
model.component('comp1').physics('shell').feature('to10').selection.set([28 29 41 42]);
model.component('comp1').physics('shell').feature('to10').set('d', '20.0[mm]');

model.component('comp1').physics('shell').create('fix1', 'Fixed', 1);
model.component('comp1').physics('shell').feature('fix1').selection.set([2 3 34 74]);

model.param.set('F_x', '1400.0E3');
model.param.descr('F_x', '');
model.param.set('F_y', '0');
model.param.descr('F_y', '');
model.param.set('F_z', '0');
model.param.descr('F_z', '');

model.component('comp1').physics('shell').create('el1', 'EdgeLoad', 1);
model.component('comp1').physics('shell').feature('el1').selection.set([31 32 53 55]);
model.component('comp1').physics('shell').feature('el1').set('LoadTypeForce', 'TotalForce');
model.component('comp1').physics('shell').feature('el1').set('FeTot', {'F_x' 'F_y' 'F_z'});

model.component('comp1').mesh('mesh1').create('fq1', 'FreeQuad');
model.component('comp1').mesh('mesh1').feature('fq1').selection.set([1 2 4 5 7 8 10 11 13 14 16 17 19 20 22 23 25 26 28 29 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51]);
model.component('comp1').mesh('mesh1').feature('size').set('hauto', 3);
model.component('comp1').mesh('mesh1').run;

