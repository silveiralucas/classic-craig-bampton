function out = model
%
% shell_tower_with_tower_top_nacelle.m
%
% Model exported on Dec 16 2020, 11:11 by COMSOL 5.5.0.359.

import com.comsol.model.*
import com.comsol.model.util.*

model = ModelUtil.create('Model');

model.modelPath('C:\Users\silveiralucas\OneDrive - Syddansk Universitet\Craig-Bampton\comsol_example');

model.component.create('comp1', true);

model.component('comp1').geom.create('geom1', 3);

model.component('comp1').mesh.create('mesh1');

model.component('comp1').physics.create('beam', 'HermitianBeam', 'geom1');
model.component('comp1').physics.create('shell', 'Shell', 'geom1');

model.study.create('std1');
model.study('std1').create('stat', 'Stationary');
model.study('std1').feature('stat').activate('beam', true);
model.study('std1').feature('stat').activate('shell', true);

model.component('comp1').geom('geom1').create('cone1', 'Cone');
model.component('comp1').geom('geom1').feature('cone1').set('specifytop', 'radius');
model.component('comp1').geom('geom1').feature('cone1').set('r', '8.3/2');
model.component('comp1').geom('geom1').feature('cone1').set('h', 11.5);
model.component('comp1').geom('geom1').feature('cone1').set('rtop', 8.0215);
model.component('comp1').geom('geom1').run('cone1');
model.component('comp1').geom('geom1').feature('cone1').set('rtop', '8.0215/2');
model.component('comp1').geom('geom1').run('cone1');
model.component('comp1').geom('geom1').feature.duplicate('cone2', 'cone1');
model.component('comp1').geom('geom1').feature('cone2').set('r', '8.0215/2');
model.component('comp1').geom('geom1').feature('cone2').set('rtop', '7.7430/2');
model.component('comp1').geom('geom1').feature('cone2').set('pos', [0 0 11.5]);
model.component('comp1').geom('geom1').run('cone2');
model.component('comp1').geom('geom1').feature.duplicate('cone3', 'cone2');
model.component('comp1').geom('geom1').feature('cone3').set('r', '7.7430/2');
model.component('comp1').geom('geom1').feature('cone3').set('rtop', '7.4646/2');
model.component('comp1').geom('geom1').feature('cone3').set('pos', {'0' '0' '23.0'});
model.component('comp1').geom('geom1').run('cone3');
model.component('comp1').geom('geom1').feature.duplicate('cone4', 'cone3');
model.component('comp1').geom('geom1').feature('cone4').set('r', '7.4646/2');
model.component('comp1').geom('geom1').feature('cone4').set('rtop', '7.1861/2');
model.component('comp1').geom('geom1').feature('cone4').set('pos', [0 0 34.5]);
model.component('comp1').geom('geom1').run('cone4');
model.component('comp1').geom('geom1').feature.duplicate('cone5', 'cone4');
model.component('comp1').geom('geom1').feature('cone5').set('r', '7.1861/2');
model.component('comp1').geom('geom1').feature('cone5').set('rtop', '6.9076/2');
model.component('comp1').geom('geom1').feature('cone5').set('pos', {'0' '0' '46.0'});
model.component('comp1').geom('geom1').run('cone5');
model.component('comp1').geom('geom1').feature.duplicate('cone6', 'cone5');
model.component('comp1').geom('geom1').feature('cone6').set('r', '6.9076/2');
model.component('comp1').geom('geom1').feature('cone6').set('rtop', '6.6291/2');
model.component('comp1').geom('geom1').feature('cone6').set('pos', [0 0 57.5]);
model.component('comp1').geom('geom1').run('cone6');
model.component('comp1').geom('geom1').feature.duplicate('cone7', 'cone6');
model.component('comp1').geom('geom1').feature('cone7').set('r', '6.6291/2');
model.component('comp1').geom('geom1').feature('cone7').set('rtop', '6.3507/2');
model.component('comp1').geom('geom1').feature('cone7').set('pos', {'0' '0' '69.0'});
model.component('comp1').geom('geom1').run('cone7');
model.component('comp1').geom('geom1').feature.duplicate('cone8', 'cone7');
model.component('comp1').geom('geom1').feature('cone8').set('r', '6.3507/2');
model.component('comp1').geom('geom1').feature('cone8').set('rtop', '6.0722/2');
model.component('comp1').geom('geom1').feature('cone8').set('pos', [0 0 80.5]);
model.component('comp1').geom('geom1').run('cone8');
model.component('comp1').geom('geom1').run('cone8');
model.component('comp1').geom('geom1').feature.duplicate('cone9', 'cone8');
model.component('comp1').geom('geom1').feature('cone9').set('r', '6.0722/2');
model.component('comp1').geom('geom1').feature('cone9').set('rtop', '5.7937/2');
model.component('comp1').geom('geom1').feature('cone9').set('pos', {'0' '0' '92.0'});
model.component('comp1').geom('geom1').run('cone9');
model.component('comp1').geom('geom1').feature.duplicate('cone10', 'cone9');
model.component('comp1').geom('geom1').feature('cone10').set('r', '5.7937/2');
model.component('comp1').geom('geom1').feature('cone10').set('h', 12.13);
model.component('comp1').geom('geom1').feature('cone10').set('rtop', '5.5/2');
model.component('comp1').geom('geom1').feature('cone10').set('pos', [0 0 103.5]);
model.component('comp1').geom('geom1').run('cone10');
model.component('comp1').geom('geom1').run('fin');
model.component('comp1').geom('geom1').run('cone10');
model.component('comp1').geom('geom1').create('pt1', 'Point');
model.component('comp1').geom('geom1').feature('pt1').setIndex('p', 115.63, 2);
model.component('comp1').geom('geom1').run('pt1');
model.component('comp1').geom('geom1').feature.duplicate('pt2', 'pt1');
model.component('comp1').geom('geom1').feature('pt2').setIndex('p', 118.38, 2);
model.component('comp1').geom('geom1').run('pt2');
model.component('comp1').geom('geom1').run('pt2');
model.component('comp1').geom('geom1').create('ls1', 'LineSegment');
model.component('comp1').geom('geom1').feature('ls1').selection('vertex1').set('pt1', 1);
model.component('comp1').geom('geom1').feature('ls1').selection('vertex2').set('pt2', 1);
model.component('comp1').geom('geom1').run('ls1');
model.component('comp1').geom('geom1').run('fin');

model.component('comp1').physics.move('shell', 0);
model.component('comp1').physics('shell').selection.set([1 2 4 5 7 8 10 11 13 14 16 17 19 20 22 23 25 26 28 29 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51]);
model.component('comp1').physics('shell').prop('PhysicsSymbols').set('PhysicsSymbols', true);
model.component('comp1').physics('shell').feature('to1').set('d', '38.0[mm]');
model.component('comp1').physics('shell').feature('to1').set('OffsetDefinition', 'PhysicalDistance');
model.component('comp1').physics('shell').feature('to1').set('z_offset', '-(38.0/2)[mm]');
model.component('comp1').physics('shell').feature.duplicate('to2', 'to1');
model.component('comp1').physics('shell').feature('to2').selection.set([4 5 33 50]);
model.component('comp1').physics('shell').feature('to2').set('d', '36.0[mm]');
model.component('comp1').physics('shell').feature('to2').set('z_offset', '-(36.0/2)[mm]');
model.component('comp1').physics('shell').feature.duplicate('to3', 'to2');
model.component('comp1').physics('shell').feature('to3').selection.set([7 8 34 49]);
model.component('comp1').physics('shell').feature('to3').set('d', '34.0[mm]');
model.component('comp1').physics('shell').feature('to3').set('z_offset', '-(34.0/2)[mm]');
model.component('comp1').physics('shell').feature.duplicate('to4', 'to3');
model.component('comp1').physics('shell').feature('to4').selection.set([10 11 35 48]);
model.component('comp1').physics('shell').feature('to4').set('d', '32.0[mm]');
model.component('comp1').physics('shell').feature('to4').set('z_offset', '-(32.0/2)[mm]');
model.component('comp1').physics('shell').feature.duplicate('to5', 'to4');
model.component('comp1').physics('shell').feature('to5').selection.set([13 14 36 47]);
model.component('comp1').physics('shell').feature('to5').set('d', '30.0[mm]');
model.component('comp1').physics('shell').feature('to5').set('z_offset', '-(30.0/2)[mm]');
model.component('comp1').physics('shell').feature.duplicate('to6', 'to5');
model.component('comp1').physics('shell').feature('to6').selection.set([16 17 37 46]);
model.component('comp1').physics('shell').feature('to6').set('d', '28.0[mm]');
model.component('comp1').physics('shell').feature('to6').set('z_offset', '-(28.0/2)[mm]');
model.component('comp1').physics('shell').feature.duplicate('to7', 'to6');
model.component('comp1').physics('shell').feature('to7').selection.set([19 20 38 45]);
model.component('comp1').physics('shell').feature('to7').set('d', '26.0[mm]');
model.component('comp1').physics('shell').feature('to7').set('z_offset', '-(26.0/2)[mm]');
model.component('comp1').physics('shell').feature.duplicate('to8', 'to7');
model.component('comp1').physics('shell').feature('to8').selection.set([22 23 39 44]);
model.component('comp1').physics('shell').feature('to8').set('d', '24.0[mm]');
model.component('comp1').physics('shell').feature('to8').set('z_offset', '-(24.0/2)[mm]');
model.component('comp1').physics('shell').feature.duplicate('to9', 'to8');
model.component('comp1').physics('shell').feature('to9').selection.set([25 26 40 43]);
model.component('comp1').physics('shell').feature('to9').set('d', '22.0[mm]');
model.component('comp1').physics('shell').feature('to9').set('z_offset', '-(22.0/2)[mm]');
model.component('comp1').physics('shell').feature.duplicate('to10', 'to9');
model.component('comp1').physics('shell').feature('to10').selection.set([28 29 41 42]);
model.component('comp1').physics('shell').feature('to10').set('d', '20.0[mm]');
model.component('comp1').physics('shell').feature('to10').set('z_offset', '-(20.0/2)[mm]');
model.component('comp1').physics('shell').create('fix1', 'Fixed', 1);
model.component('comp1').physics('shell').feature('fix1').selection.set([2 3 34 74]);
model.component('comp1').physics('beam').selection.set([54]);
model.component('comp1').physics('beam').prop('PhysicsSymbols').set('PhysicsSymbols', true);
model.component('comp1').physics('beam').feature('csd1').set('area', '0.298[m^2]');
model.component('comp1').physics('beam').feature('csd1').set('Izz', '0.552[m^4]');
model.component('comp1').physics('beam').feature('csd1').set('Iyy', '0.552[m^4]');
model.component('comp1').physics('beam').feature('csd1').set('J_beam', '1.1[m^4]');
model.component('comp1').physics('beam').create('pl1', 'PointLoad', 0);
model.component('comp1').physics('beam').feature('pl1').selection.set([24]);
model.component('comp1').physics('beam').feature('pl1').set('Fp', [100 0 0]);

model.component('comp1').multiphysics.create('shbc1', 'ShellBeamConnection', -1);
model.component('comp1').multiphysics('shbc1').set('selectionControl', true);
model.component('comp1').multiphysics('shbc1').selection('edgPointShellSelection').set([31 32 53 55 56]);
model.component('comp1').multiphysics('shbc1').selection('edgPointBeamSelection').set([23]);

model.component('comp1').physics('beam').create('adm1', 'AddedMass1', 1);
model.component('comp1').physics('beam').feature('adm1').selection.set([54]);
model.component('comp1').physics('beam').feature.remove('adm1');
model.component('comp1').physics('beam').create('pm1', 'PointMass', 0);
model.component('comp1').physics('beam').feature('pm1').selection.set([24]);
model.component('comp1').physics('beam').feature('pm1').set('pointmass', '4.4604E+05');
model.component('comp1').physics('beam').feature.move('pm1', 4);

model.component('comp1').material.create('mat1', 'Common');
model.component('comp1').material('mat1').propertyGroup('def').set('youngsmodulus', '210E9');
model.component('comp1').material('mat1').propertyGroup('def').set('poissonsratio', '0.3');
model.component('comp1').material('mat1').propertyGroup('def').set('density', '8500');
model.component('comp1').material.duplicate('mat2', 'mat1');
model.component('comp1').material('mat2').selection.geom('geom1', 1);
model.component('comp1').material('mat2').selection.set([54]);

model.component('comp1').mesh('mesh1').create('fq1', 'FreeQuad');
model.component('comp1').mesh('mesh1').feature('fq1').selection.all;
model.component('comp1').mesh('mesh1').feature('fq1').selection.set([1 2 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51]);
model.component('comp1').mesh('mesh1').create('edg1', 'Edge');
model.component('comp1').mesh('mesh1').feature('edg1').selection.set([54]);
model.component('comp1').mesh('mesh1').feature('size').set('hauto', 3);
model.component('comp1').mesh('mesh1').run;
model.component('comp1').mesh('mesh1').feature('size').set('hauto', 2);
model.component('comp1').mesh('mesh1').run;

model.sol.create('sol1');
model.sol('sol1').study('std1');

model.study('std1').feature('stat').set('notlistsolnum', 1);
model.study('std1').feature('stat').set('notsolnum', '1');
model.study('std1').feature('stat').set('listsolnum', 1);
model.study('std1').feature('stat').set('solnum', '1');

model.sol('sol1').create('st1', 'StudyStep');
model.sol('sol1').feature('st1').set('study', 'std1');
model.sol('sol1').feature('st1').set('studystep', 'stat');
model.sol('sol1').create('v1', 'Variables');
model.sol('sol1').feature('v1').set('control', 'stat');
model.sol('sol1').create('s1', 'Stationary');
model.sol('sol1').feature('s1').feature('aDef').set('cachepattern', true);
model.sol('sol1').feature('s1').create('seDef', 'Segregated');
model.sol('sol1').feature('s1').create('fc1', 'FullyCoupled');
model.sol('sol1').feature('s1').feature('fc1').set('termonres', 'auto');
model.sol('sol1').feature('s1').feature('fc1').set('reserrfact', 1000);
model.sol('sol1').feature('s1').feature('fc1').set('linsolver', 'dDef');
model.sol('sol1').feature('s1').feature('fc1').set('termonres', 'auto');
model.sol('sol1').feature('s1').feature('fc1').set('reserrfact', 1000);
model.sol('sol1').feature('s1').feature.remove('fcDef');
model.sol('sol1').feature('s1').feature.remove('seDef');
model.sol('sol1').attach('std1');

model.result.dataset.create('shl1', 'Shell');
model.result.dataset('shl1').set('data', 'dset1');
model.result.dataset('shl1').setIndex('orientationexpr', 'shell.nlX', 0);
model.result.dataset('shl1').setIndex('orientationexpr', 'shell.nlY', 1);
model.result.dataset('shl1').setIndex('orientationexpr', 'shell.nlZ', 2);
model.result.dataset('shl1').set('distanceexpr', 'shell.z_pos');
model.result.create('pg1', 'PlotGroup3D');
model.result('pg1').set('data', 'dset1');
model.result('pg1').label('Stress (shell)');
model.result('pg1').set('showlegends', true);
model.result('pg1').set('data', 'shl1');
model.result('pg1').create('surf1', 'Surface');
model.result('pg1').feature('surf1').set('expr', 'gpeval(4,shell.mises)');
model.result('pg1').feature('surf1').set('descr', 'von Mises stress');
model.result('pg1').feature('surf1').set('colortable', 'RainbowLight');
model.result('pg1').feature('surf1').create('def', 'Deform');
model.result('pg1').feature('surf1').feature('def').set('expr', {'u2' 'v2' 'w2'});
model.result('pg1').feature('surf1').feature('def').set('descr', 'Displacement field');
model.result.create('pg2', 'PlotGroup3D');
model.result('pg2').set('data', 'dset1');
model.result('pg2').label('Shell Geometry (shell)');
model.result('pg2').set('titletype', 'manual');
model.result('pg2').set('title', 'Shell Geometry');
model.result('pg2').set('showlegends', false);
model.result('pg2').set('edgecolor', 'cyan');
model.result('pg2').create('surf1', 'Surface');
model.result('pg2').feature('surf1').set('expr', 'shell.z');
model.result('pg2').feature('surf1').set('data', 'shl1');
model.result('pg2').feature('surf1').label('Top and Bottom');
model.result('pg2').feature('surf1').set('colortable', 'RainbowLight');
model.result.create('pg3', 'PlotGroup3D');
model.result('pg3').set('data', 'dset1');
model.result('pg3').label('Thickness and Orientation (shell)');
model.result('pg3').set('titletype', 'manual');
model.result('pg3').set('title', 'Thickness and Orientation');
model.result('pg3').set('showlegendsunit', true);
model.result('pg3').create('con1', 'Contour');
model.result('pg3').feature('con1').set('expr', 'shell.d');
model.result('pg3').feature('con1').label('Thickness');
model.result('pg3').feature('con1').set('colortable', 'HeatCameraLight');
model.result('pg3').feature('con1').set('colortablerev', true);
model.result('pg3').feature('con1').set('contourtype', 'filled');
model.result('pg3').feature('con1').set('includeoutside', true);
model.result('pg3').feature('con1').set('number', 11);
model.result('pg3').feature('con1').set('smooth', 'none');
model.result('pg3').create('syss', 'CoordSysSurface');
model.result('pg3').feature('syss').set('sys', 'shellsys');
model.result('pg3').feature('syss').label('Shell Local System');
model.result.create('pg4', 'PlotGroup3D');
model.result('pg4').set('data', 'dset1');
model.result('pg4').create('line1', 'Line');
model.result('pg4').feature('line1').set('expr', 'beam.mises');
model.result('pg4').label('Stress (beam)');
model.result('pg4').feature('line1').set('colortable', 'RainbowLight');
model.result('pg4').feature('line1').set('linetype', 'tube');
model.result('pg4').feature('line1').set('radiusexpr', 'beam.re');
model.result('pg4').feature('line1').set('tuberadiusscaleactive', true);
model.result('pg4').feature('line1').set('tuberadiusscale', 1);
model.result('pg4').feature('line1').create('def', 'Deform');
model.result('pg4').feature('line1').feature('def').set('expr', {'u' 'v' 'w'});
model.result('pg4').feature('line1').feature('def').set('descr', 'Displacement field');

model.nodeGroup.create('dset1beamsfgrp', 'Results');
model.nodeGroup('dset1beamsfgrp').label('Section Forces (beam)');
model.nodeGroup('dset1beamsfgrp').set('type', 'plotgroup');
model.nodeGroup('dset1beamsfgrp').placeAfter('plotgroup', 'pg4');

model.result.create('pg5', 'PlotGroup3D');
model.result('pg5').set('data', 'dset1');
model.result('pg5').create('line1', 'Line');
model.result('pg5').feature('line1').set('expr', 'beam.Myl');
model.result('pg5').label('Moment Y (beam)');
model.result('pg5').feature('line1').set('colortable', 'Wave');
model.result('pg5').feature('line1').set('colortablesym', true);
model.result('pg5').feature('line1').set('linetype', 'tube');
model.result('pg5').feature('line1').set('radiusexpr', 'beam.re');
model.result('pg5').feature('line1').set('tuberadiusscaleactive', true);
model.result('pg5').feature('line1').set('tuberadiusscale', 1);
model.result('pg5').feature('line1').create('def', 'Deform');
model.result('pg5').feature('line1').feature('def').set('expr', {'u' 'v' 'w'});
model.result('pg5').feature('line1').feature('def').set('descr', 'Displacement field');

model.nodeGroup('dset1beamsfgrp').add('plotgroup', 'pg5');

model.result.create('pg6', 'PlotGroup3D');
model.result('pg6').set('data', 'dset1');
model.result('pg6').create('line1', 'Line');
model.result('pg6').feature('line1').set('expr', 'beam.Mzl');
model.result('pg6').label('Moment Z (beam)');
model.result('pg6').feature('line1').set('colortable', 'Wave');
model.result('pg6').feature('line1').set('colortablesym', true);
model.result('pg6').feature('line1').set('linetype', 'tube');
model.result('pg6').feature('line1').set('radiusexpr', 'beam.re');
model.result('pg6').feature('line1').set('tuberadiusscaleactive', true);
model.result('pg6').feature('line1').set('tuberadiusscale', 1);
model.result('pg6').feature('line1').create('def', 'Deform');
model.result('pg6').feature('line1').feature('def').set('expr', {'u' 'v' 'w'});
model.result('pg6').feature('line1').feature('def').set('descr', 'Displacement field');

model.nodeGroup('dset1beamsfgrp').add('plotgroup', 'pg6');

model.result.create('pg7', 'PlotGroup3D');
model.result('pg7').set('data', 'dset1');
model.result('pg7').create('line1', 'Line');
model.result('pg7').feature('line1').set('expr', 'beam.Tyl');
model.result('pg7').label('Shear Force Y (beam)');
model.result('pg7').feature('line1').set('colortable', 'Wave');
model.result('pg7').feature('line1').set('colortablesym', true);
model.result('pg7').feature('line1').set('linetype', 'tube');
model.result('pg7').feature('line1').set('radiusexpr', 'beam.re');
model.result('pg7').feature('line1').set('tuberadiusscaleactive', true);
model.result('pg7').feature('line1').set('tuberadiusscale', 1);
model.result('pg7').feature('line1').create('def', 'Deform');
model.result('pg7').feature('line1').feature('def').set('expr', {'u' 'v' 'w'});
model.result('pg7').feature('line1').feature('def').set('descr', 'Displacement field');

model.nodeGroup('dset1beamsfgrp').add('plotgroup', 'pg7');

model.result.create('pg8', 'PlotGroup3D');
model.result('pg8').set('data', 'dset1');
model.result('pg8').create('line1', 'Line');
model.result('pg8').feature('line1').set('expr', 'beam.Tzl');
model.result('pg8').label('Shear Force Z (beam)');
model.result('pg8').feature('line1').set('colortable', 'Wave');
model.result('pg8').feature('line1').set('colortablesym', true);
model.result('pg8').feature('line1').set('linetype', 'tube');
model.result('pg8').feature('line1').set('radiusexpr', 'beam.re');
model.result('pg8').feature('line1').set('tuberadiusscaleactive', true);
model.result('pg8').feature('line1').set('tuberadiusscale', 1);
model.result('pg8').feature('line1').create('def', 'Deform');
model.result('pg8').feature('line1').feature('def').set('expr', {'u' 'v' 'w'});
model.result('pg8').feature('line1').feature('def').set('descr', 'Displacement field');

model.nodeGroup('dset1beamsfgrp').add('plotgroup', 'pg8');

model.result.create('pg9', 'PlotGroup3D');
model.result('pg9').set('data', 'dset1');
model.result('pg9').create('line1', 'Line');
model.result('pg9').feature('line1').set('expr', 'beam.Nxl');
model.result('pg9').label('Axial Force (beam)');
model.result('pg9').feature('line1').set('colortable', 'Wave');
model.result('pg9').feature('line1').set('colortablesym', true);
model.result('pg9').feature('line1').set('linetype', 'tube');
model.result('pg9').feature('line1').set('radiusexpr', 'beam.re');
model.result('pg9').feature('line1').set('tuberadiusscaleactive', true);
model.result('pg9').feature('line1').set('tuberadiusscale', 1);
model.result('pg9').feature('line1').create('def', 'Deform');
model.result('pg9').feature('line1').feature('def').set('expr', {'u' 'v' 'w'});
model.result('pg9').feature('line1').feature('def').set('descr', 'Displacement field');

model.nodeGroup('dset1beamsfgrp').add('plotgroup', 'pg9');

model.result.create('pg10', 'PlotGroup3D');
model.result('pg10').set('data', 'dset1');
model.result('pg10').create('line1', 'Line');
model.result('pg10').feature('line1').set('expr', 'beam.Mxl');
model.result('pg10').label('Torsion Moment (beam)');
model.result('pg10').feature('line1').set('colortable', 'Wave');
model.result('pg10').feature('line1').set('colortablesym', true);
model.result('pg10').feature('line1').set('linetype', 'tube');
model.result('pg10').feature('line1').set('radiusexpr', 'beam.re');
model.result('pg10').feature('line1').set('tuberadiusscaleactive', true);
model.result('pg10').feature('line1').set('tuberadiusscale', 1);
model.result('pg10').feature('line1').create('def', 'Deform');
model.result('pg10').feature('line1').feature('def').set('expr', {'u' 'v' 'w'});
model.result('pg10').feature('line1').feature('def').set('descr', 'Displacement field');

model.nodeGroup('dset1beamsfgrp').add('plotgroup', 'pg10');

model.result.create('pg11', 'PlotGroup3D');
model.result('pg11').set('data', 'dset1');
model.result('pg11').create('line1', 'Line');
model.result('pg11').feature('line1').set('expr', 'beam.re');
model.result('pg11').label('Beam Orientation (beam)');
model.result('pg11').feature('line1').set('linetype', 'tube');
model.result('pg11').feature('line1').set('radiusexpr', 'beam.re');
model.result('pg11').feature('line1').set('colortable', 'GrayPrint');
model.result('pg11').feature('line1').set('tuberadiusscaleactive', true);
model.result('pg11').feature('line1').set('tuberadiusscale', 1);
model.result('pg11').feature('line1').label('Size');
model.result('pg11').set('titletype', 'manual');
model.result('pg11').set('title', 'Beam approximate radius and principal axes');
model.result('pg11').create('arws1', 'ArrowLine');
model.result('pg11').feature('arws1').label('Local Y Direction (Green)');
model.result('pg11').feature('arws1').set('expr', {'beam.beamsys.e_y1*beam.rgy' 'beam.beamsys.e_y2*beam.rgy' 'beam.beamsys.e_y3*beam.rgy'});
model.result('pg11').feature('arws1').set('color', 'green');
model.result('pg11').feature('arws1').set('placement', 'gausspoints');
model.result('pg11').feature('arws1').set('scale', 8);
model.result('pg11').create('arws2', 'ArrowLine');
model.result('pg11').feature('arws2').label('Local Z Direction (Blue)');
model.result('pg11').feature('arws2').set('expr', {'beam.beamsys.e_z1*beam.rgz' 'beam.beamsys.e_z2*beam.rgz' 'beam.beamsys.e_z3*beam.rgz'});
model.result('pg11').feature('arws2').set('color', 'blue');
model.result('pg11').feature('arws2').set('placement', 'gausspoints');
model.result('pg11').feature('arws2').set('scale', 8);

model.nodeGroup.create('dset1beamlgrp', 'Results');
model.nodeGroup('dset1beamlgrp').label('Applied Loads (beam)');
model.nodeGroup('dset1beamlgrp').set('type', 'plotgroup');
model.nodeGroup('dset1beamlgrp').placeAfter('plotgroup', 'pg11');

model.result.create('pg12', 'PlotGroup3D');
model.result('pg12').set('data', 'dset1');
model.result('pg12').label('Point Loads (beam)');

model.nodeGroup('dset1beamlgrp').add('plotgroup', 'pg12');

model.result('pg12').set('showlegends', true);
model.result('pg12').set('titletype', 'custom');
model.result('pg12').set('typeintitle', false);
model.result('pg12').set('descriptionintitle', false);
model.result('pg12').set('unitintitle', false);
model.result('pg12').set('frametype', 'spatial');
model.result('pg12').set('showlegendsunit', true);
model.result('pg12').create('arpt1', 'ArrowPoint');
model.result('pg12').feature('arpt1').set('expr', {'beam.pm1.F_PX' 'beam.pm1.F_PY' 'beam.pm1.F_PZ'});
model.result('pg12').feature('arpt1').set('arrowbase', 'tail');
model.result('pg12').feature('arpt1').label('Point Mass 1');
model.result('pg12').feature('arpt1').set('inheritplot', 'none');
model.result('pg12').feature('arpt1').create('col', 'Color');
model.result('pg12').feature('arpt1').feature('col').set('expr', 'comp1.beam.pm1.F_P_Mag');
model.result('pg12').feature('arpt1').feature('col').set('colortable', 'Spectrum');
model.result('pg12').feature('arpt1').feature('col').set('coloring', 'gradient');
model.result('pg12').feature('arpt1').feature('col').set('topcolor', 'red');
model.result('pg12').feature('arpt1').set('color', 'blue');
model.result('pg12').feature('arpt1').create('def', 'Deform');
model.result('pg12').feature('arpt1').feature('def').set('expr', {'u' 'v' 'w'});
model.result('pg12').feature('arpt1').feature('def').set('descr', 'Displacement field');
model.result('pg12').feature('arpt1').feature('def').set('scaleactive', true);
model.result('pg12').feature('arpt1').feature('def').set('scale', 0);

model.nodeGroup('dset1beamlgrp').placeAfter('plotgroup', 'pg11');

model.result('pg12').create('arpt2', 'ArrowPoint');
model.result('pg12').feature('arpt2').set('expr', {'beam.pl1.F_Px' 'beam.pl1.F_Py' 'beam.pl1.F_Pz'});
model.result('pg12').feature('arpt2').set('arrowbase', 'tail');
model.result('pg12').feature('arpt2').label('Point Load 1');
model.result('pg12').feature('arpt2').set('inheritplot', 'arpt1');
model.result('pg12').feature('arpt2').create('col', 'Color');
model.result('pg12').feature('arpt2').feature('col').set('expr', 'comp1.beam.pl1.F_P_Mag');
model.result('pg12').feature('arpt2').feature('col').set('colortable', 'Spectrum');
model.result('pg12').feature('arpt2').feature('col').set('coloring', 'gradient');
model.result('pg12').feature('arpt2').feature('col').set('topcolor', 'red');
model.result('pg12').feature('arpt2').set('color', 'blue');
model.result('pg12').feature('arpt2').create('def', 'Deform');
model.result('pg12').feature('arpt2').feature('def').set('expr', {'u' 'v' 'w'});
model.result('pg12').feature('arpt2').feature('def').set('descr', 'Displacement field');
model.result('pg12').feature('arpt2').feature('def').set('scaleactive', true);
model.result('pg12').feature('arpt2').feature('def').set('scale', 0);

model.sol('sol1').runAll;

model.result('pg1').run;

model.study.create('std2');
model.study('std2').create('eig', 'Eigenfrequency');
model.study('std2').feature('eig').set('solnum', 'auto');
model.study('std2').feature('eig').set('notsolnum', 'auto');
model.study('std2').feature('eig').set('ngen', '5');
model.study('std2').feature('eig').activate('shell', true);
model.study('std2').feature('eig').activate('beam', true);
model.study('std2').feature('eig').activate('shbc1', true);

model.component('comp1').common.create('mpf1', 'ParticipationFactors');

model.study('std2').feature('eig').set('neigsactive', true);
model.study('std2').feature('eig').set('neigs', 20);

model.sol.create('sol2');
model.sol('sol2').study('std2');

model.study('std2').feature('eig').set('notlistsolnum', 1);
model.study('std2').feature('eig').set('notsolnum', 'auto');
model.study('std2').feature('eig').set('listsolnum', 1);
model.study('std2').feature('eig').set('solnum', 'auto');

model.sol('sol2').create('st1', 'StudyStep');
model.sol('sol2').feature('st1').set('study', 'std2');
model.sol('sol2').feature('st1').set('studystep', 'eig');
model.sol('sol2').create('v1', 'Variables');
model.sol('sol2').feature('v1').set('control', 'eig');
model.sol('sol2').create('e1', 'Eigenvalue');
model.sol('sol2').feature('e1').set('eigvfunscale', 'maximum');
model.sol('sol2').feature('e1').set('eigvfunscaleparam', '1.1899999999999999E-4');
model.sol('sol2').feature('e1').set('control', 'eig');
model.sol('sol2').feature('e1').feature('aDef').set('cachepattern', true);
model.sol('sol2').attach('std2');

model.result.dataset.create('shl2', 'Shell');
model.result.dataset('shl2').set('data', 'dset2');
model.result.dataset('shl2').setIndex('topconst', '1', 3, 1);
model.result.dataset('shl2').setIndex('bottomconst', '-1', 3, 1);
model.result.dataset('shl2').setIndex('orientationexpr', 'shell.nlX', 0);
model.result.dataset('shl2').setIndex('orientationexpr', 'shell.nlY', 1);
model.result.dataset('shl2').setIndex('orientationexpr', 'shell.nlZ', 2);
model.result.dataset('shl2').set('distanceexpr', 'shell.z_pos');
model.result.create('pg13', 'PlotGroup3D');
model.result('pg13').set('data', 'dset2');
model.result('pg13').set('showlegends', false);
model.result('pg13').create('surf1', 'Surface');
model.result('pg13').feature('surf1').set('expr', 'shell.disp');
model.result('pg13').label('Mode Shape (shell)');
model.result('pg13').feature('surf1').set('colortable', 'AuroraBorealis');
model.result('pg13').feature('surf1').create('def', 'Deform');
model.result('pg13').feature('surf1').feature('def').set('expr', {'u2' 'v2' 'w2'});
model.result('pg13').feature('surf1').feature('def').set('descr', 'Displacement field');
model.result.create('pg14', 'PlotGroup3D');
model.result('pg14').set('data', 'dset2');
model.result('pg14').label('Shell Geometry (shell) 1');
model.result('pg14').set('titletype', 'manual');
model.result('pg14').set('title', 'Shell Geometry');
model.result('pg14').set('showlegends', false);
model.result('pg14').set('edgecolor', 'cyan');
model.result('pg14').create('surf1', 'Surface');
model.result('pg14').feature('surf1').set('expr', 'shell.z');
model.result('pg14').feature('surf1').set('data', 'shl2');
model.result('pg14').feature('surf1').label('Top and Bottom');
model.result('pg14').feature('surf1').set('colortable', 'RainbowLight');
model.result.create('pg15', 'PlotGroup3D');
model.result('pg15').set('data', 'dset2');
model.result('pg15').label('Thickness and Orientation (shell) 1');
model.result('pg15').set('titletype', 'manual');
model.result('pg15').set('title', 'Thickness and Orientation');
model.result('pg15').set('showlegendsunit', true);
model.result('pg15').create('con1', 'Contour');
model.result('pg15').feature('con1').set('expr', 'shell.d');
model.result('pg15').feature('con1').label('Thickness');
model.result('pg15').feature('con1').set('colortable', 'HeatCameraLight');
model.result('pg15').feature('con1').set('colortablerev', true);
model.result('pg15').feature('con1').set('contourtype', 'filled');
model.result('pg15').feature('con1').set('includeoutside', true);
model.result('pg15').feature('con1').set('number', 11);
model.result('pg15').feature('con1').set('smooth', 'none');
model.result('pg15').create('syss', 'CoordSysSurface');
model.result('pg15').feature('syss').set('sys', 'shellsys');
model.result('pg15').feature('syss').label('Shell Local System');
model.result.evaluationGroup.create('std2EvgFrq', 'EvaluationGroup');
model.result.evaluationGroup('std2EvgFrq').set('data', 'dset2');
model.result.evaluationGroup('std2EvgFrq').label('Eigenfrequencies (Study 2)');
model.result.evaluationGroup('std2EvgFrq').create('gev1', 'EvalGlobal');
model.result.evaluationGroup('std2EvgFrq').feature('gev1').setIndex('expr', 'freq*2*pi', 0);
model.result.evaluationGroup('std2EvgFrq').feature('gev1').setIndex('unit', 'rad/s', 0);
model.result.evaluationGroup('std2EvgFrq').feature('gev1').setIndex('descr', 'Angular frequency', 0);
model.result.evaluationGroup('std2EvgFrq').feature('gev1').setIndex('expr', 'imag(freq)/abs(freq)', 1);
model.result.evaluationGroup('std2EvgFrq').feature('gev1').setIndex('unit', '1', 1);
model.result.evaluationGroup('std2EvgFrq').feature('gev1').setIndex('descr', 'Damping ratio', 1);
model.result.evaluationGroup('std2EvgFrq').feature('gev1').setIndex('expr', 'abs(freq)/imag(freq)/2', 2);
model.result.evaluationGroup('std2EvgFrq').feature('gev1').setIndex('unit', '1', 2);
model.result.evaluationGroup('std2EvgFrq').feature('gev1').setIndex('descr', 'Quality factor', 2);
model.result.create('pg16', 'PlotGroup3D');
model.result('pg16').set('data', 'dset2');
model.result('pg16').set('showlegends', false);
model.result('pg16').create('line1', 'Line');
model.result('pg16').feature('line1').set('expr', 'beam.disp');
model.result('pg16').label('Mode Shape (beam)');
model.result('pg16').feature('line1').set('colortable', 'AuroraBorealis');
model.result('pg16').feature('line1').set('linetype', 'tube');
model.result('pg16').feature('line1').set('radiusexpr', 'beam.re');
model.result('pg16').feature('line1').set('tuberadiusscaleactive', true);
model.result('pg16').feature('line1').set('tuberadiusscale', 1);
model.result('pg16').feature('line1').create('def', 'Deform');
model.result('pg16').feature('line1').feature('def').set('expr', {'u' 'v' 'w'});
model.result('pg16').feature('line1').feature('def').set('descr', 'Displacement field');
model.result.create('pg17', 'PlotGroup3D');
model.result('pg17').set('data', 'dset2');
model.result('pg17').create('line1', 'Line');
model.result('pg17').feature('line1').set('expr', 'beam.re');
model.result('pg17').label('Beam Orientation (beam) 1');
model.result('pg17').feature('line1').set('linetype', 'tube');
model.result('pg17').feature('line1').set('radiusexpr', 'beam.re');
model.result('pg17').feature('line1').set('colortable', 'GrayPrint');
model.result('pg17').feature('line1').set('tuberadiusscaleactive', true);
model.result('pg17').feature('line1').set('tuberadiusscale', 1);
model.result('pg17').feature('line1').label('Size');
model.result('pg17').set('titletype', 'manual');
model.result('pg17').set('title', 'Beam approximate radius and principal axes');
model.result('pg17').create('arws1', 'ArrowLine');
model.result('pg17').feature('arws1').label('Local Y Direction (Green)');
model.result('pg17').feature('arws1').set('expr', {'beam.beamsys.e_y1*beam.rgy' 'beam.beamsys.e_y2*beam.rgy' 'beam.beamsys.e_y3*beam.rgy'});
model.result('pg17').feature('arws1').set('color', 'green');
model.result('pg17').feature('arws1').set('placement', 'gausspoints');
model.result('pg17').feature('arws1').set('scale', 8);
model.result('pg17').create('arws2', 'ArrowLine');
model.result('pg17').feature('arws2').label('Local Z Direction (Blue)');
model.result('pg17').feature('arws2').set('expr', {'beam.beamsys.e_z1*beam.rgz' 'beam.beamsys.e_z2*beam.rgz' 'beam.beamsys.e_z3*beam.rgz'});
model.result('pg17').feature('arws2').set('color', 'blue');
model.result('pg17').feature('arws2').set('placement', 'gausspoints');
model.result('pg17').feature('arws2').set('scale', 8);
model.result.evaluationGroup.create('std2mpf1', 'EvaluationGroup');
model.result.evaluationGroup('std2mpf1').set('data', 'dset2');
model.result.evaluationGroup('std2mpf1').label('Participation Factors (Study 2)');
model.result.evaluationGroup('std2mpf1').create('gev1', 'EvalGlobal');
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('expr', 'mpf1.pfLnormX', 0);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('unit', '1', 0);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('descr', 'Participation factor, normalized, X-translation', 0);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('expr', 'mpf1.pfLnormY', 1);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('unit', '1', 1);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('descr', 'Participation factor, normalized, Y-translation', 1);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('expr', 'mpf1.pfLnormZ', 2);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('unit', '1', 2);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('descr', 'Participation factor, normalized, Z-translation', 2);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('expr', 'mpf1.pfRnormX', 3);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('unit', '1', 3);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('descr', 'Participation factor, normalized, X-rotation', 3);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('expr', 'mpf1.pfRnormY', 4);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('unit', '1', 4);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('descr', 'Participation factor, normalized, Y-rotation', 4);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('expr', 'mpf1.pfRnormZ', 5);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('unit', '1', 5);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('descr', 'Participation factor, normalized, Z-rotation', 5);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('expr', 'mpf1.mEffLX', 6);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('unit', 'kg', 6);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('descr', 'Effective modal mass, X-translation', 6);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('expr', 'mpf1.mEffLY', 7);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('unit', 'kg', 7);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('descr', 'Effective modal mass, Y-translation', 7);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('expr', 'mpf1.mEffLZ', 8);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('unit', 'kg', 8);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('descr', 'Effective modal mass, Z-translation', 8);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('expr', 'mpf1.mEffRX', 9);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('unit', 'kg*m^2', 9);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('descr', 'Effective modal mass, X-rotation', 9);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('expr', 'mpf1.mEffRY', 10);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('unit', 'kg*m^2', 10);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('descr', 'Effective modal mass, Y-rotation', 10);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('expr', 'mpf1.mEffRZ', 11);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('unit', 'kg*m^2', 11);
model.result.evaluationGroup('std2mpf1').feature('gev1').setIndex('descr', 'Effective modal mass, Z-rotation', 11);

model.sol('sol2').runAll;

model.result('pg13').run;
model.result.evaluationGroup('std2EvgFrq').run;
model.result.evaluationGroup('std2mpf1').run;
model.result('pg16').run;
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 2, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 3, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 4, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 5, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 6, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 7, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 8, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 9, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 10, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 11, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 12, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 13, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 14, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 15, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 16, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 17, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 18, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 19, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 20, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 19, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 18, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 17, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 16, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 15, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 14, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 13, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 12, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 11, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 10, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 9, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 8, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 7, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 6, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 5, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 4, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 3, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 2, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 1, 0);
model.result('pg13').run;

model.component('comp1').physics('shell').selection.set([1 2 4 5 7 8 10 11 13 14 16 17 19 20 22 23 25 26 28 29 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51]);
model.component('comp1').physics('shell').feature('fix1').selection.set([2 3 34 75]);

model.sol('sol2').runAll;

model.result('pg13').run;
model.result.evaluationGroup('std2EvgFrq').run;

model.component('comp1').mesh('mesh1').feature('fq1').selection.set([]);
model.component('comp1').mesh('mesh1').feature.remove('fq1');
model.component('comp1').mesh('mesh1').clearMesh;
model.component('comp1').mesh('mesh1').create('map1', 'Map');
model.component('comp1').mesh('mesh1').feature('map1').selection.set([1 2 4 5 7 8 10 11 13 14 16 17 19 20 22 23 25 26 28 29 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51]);
model.component('comp1').mesh('mesh1').feature('size').set('hauto', 1);
model.component('comp1').mesh('mesh1').run;
model.component('comp1').mesh('mesh1').feature('size').set('custom', true);
model.component('comp1').mesh('mesh1').feature('size').set('hmax', '2.37/2');
model.component('comp1').mesh('mesh1').feature('size').set('hmin', '0.0237/2');
model.component('comp1').mesh('mesh1').run;

model.sol('sol2').runAll;

model.result('pg13').run;
model.result.evaluationGroup('std2EvgFrq').run;

model.component('comp1').mesh('mesh1').feature('size').set('hmax', '2.37/4');
model.component('comp1').mesh('mesh1').feature('size').set('hmin', '0.0237/4');
model.component('comp1').mesh('mesh1').run;

model.sol('sol2').runAll;

model.result('pg13').run;
model.result.evaluationGroup('std2EvgFrq').run;

model.label('shell_tower_with_tower_top_nacelle.mph');

model.component('comp1').physics('beam').feature('pm1').set('mmi3D', [7366701.98937148 0 0 0 450907.01661148 -360283.9347828 0 -360283.9347828 7326394.97276]);

model.sol('sol1').runAll;

model.result('pg1').run;

model.sol('sol2').runAll;

model.result('pg13').run;

model.component('comp1').physics('beam').feature('pm1').set('pointmass', 0);
model.component('comp1').physics('beam').feature('pm1').set('mmi3D', {'0.' '0' '0' '0' '0.' '0' '0' '0' '0.'});

model.sol('sol2').runAll;

model.result('pg13').run;
model.result.evaluationGroup('std2EvgFrq').run;

model.component('comp1').physics('beam').feature('pm1').set('pointmass', '4.4604E+05');
model.component('comp1').physics('beam').feature('pm1').set('mmi3D', [7366701.98937148 0 0 0 450907.01661148 -360283.9347828 0 -360283.9347828 7326394.97276]);

model.sol('sol2').runAll;

model.result('pg13').run;
model.result.evaluationGroup('std2EvgFrq').run;
model.result.evaluationGroup('std2EvgFrq').save('C:\Users\silveiralucas\OneDrive - Syddansk Universitet\Reports\Craig-Bampton\code\model_3_comsol.txt');

model.label('shell_tower_with_tower_top_nacelle.mph');

model.component('comp1').physics('beam').feature('pl1').active(false);

model.result('pg1').run;
model.result('pg16').run;

model.sol('sol2').runAll;

model.result('pg13').run;
model.result('pg16').run;
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 2, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 3, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 4, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 5, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 6, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 7, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 8, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 9, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 8, 0);
model.result('pg13').run;

model.component('comp1').physics('beam').feature('pm1').set('mmi3D', [7366701.98937148 0 0 0 7366701.98937148 0 0 0 7366701.98937148]);

model.sol('sol2').runAll;

model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 9, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 10, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 11, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 12, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 13, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 14, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 15, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 16, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 17, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 18, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 19, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 20, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 19, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 18, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 17, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 16, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 15, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 14, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 13, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 12, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 11, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 10, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 9, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 8, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 7, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 6, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 5, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 4, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 3, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 2, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 1, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 2, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 3, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 4, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 5, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 1, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 2, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 3, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 4, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 5, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 6, 0);
model.result('pg13').run;

model.component('comp1').multiphysics('shbc1').selection('edgPointShellSelection').remove([55]);

model.component('comp1').physics('beam').feature('pm1').set('mmi3D', [7366701.98937148 0 0 0 450907.01661148 -360283.9347828 0 -360283.9347828 7326394.97276]);

model.sol('sol2').runAll;

model.result('pg13').run;
model.result.evaluationGroup('std2EvgFrq').run;
model.result.evaluationGroup('std2EvgFrq').save('C:\Users\silveiralucas\OneDrive - Syddansk Universitet\Reports\Craig-Bampton\code\comsol\tower_nacelle.txt');

model.component('comp1').physics('beam').feature('pm1').set('mmi3D', [7366701.98937148 0 0 0 7366701.98937148 0 0 0 7366701.98937148]);

model.sol('sol2').runAll;

model.result('pg13').run;
model.result.evaluationGroup('std2EvgFrq').run;

model.component('comp1').physics('beam').feature('pm1').set('mmi3D', {'0.' '0' '0' '0' '0.' '0' '0' '0' '0.'});

model.sol('sol2').runAll;

model.result('pg13').run;
model.result.evaluationGroup('std2EvgFrq').run;
model.result.evaluationGroup('std2EvgFrq').save('C:\Users\silveiralucas\OneDrive - Syddansk Universitet\Reports\Craig-Bampton\code\comsol\tower_nacelle.txt');
model.result.evaluationGroup('std2EvgFrq').save('C:\Users\silveiralucas\OneDrive - Syddansk Universitet\Reports\Craig-Bampton\code\comsol\tower_nacelle.txt');

model.component('comp1').physics('beam').feature('pm1').active(false);

model.component('comp1').material('mat2').propertyGroup('def').set('density', {'0.0'});

model.sol('sol2').runAll;

model.result('pg13').run;
model.result.evaluationGroup('std2EvgFrq').run;
model.result.evaluationGroup('std2EvgFrq').save('C:\Users\silveiralucas\OneDrive - Syddansk Universitet\Reports\Craig-Bampton\code\comsol\tower_connector.txt');

model.component('comp1').physics('shell').feature('to2').set('OffsetDefinition', 'RelativeDistance');
model.component('comp1').physics('shell').feature('to2').set('z_offset_rel', -1);
model.component('comp1').physics('shell').feature('to1').set('OffsetDefinition', 'RelativeDistance');
model.component('comp1').physics('shell').feature('to1').set('z_offset_rel', -1);
model.component('comp1').physics('shell').feature('to3').set('z_offset', -1);
model.component('comp1').physics('shell').feature('to3').set('OffsetDefinition', 'RelativeDistance');
model.component('comp1').physics('shell').feature('to3').set('z_offset_rel', -1);
model.component('comp1').physics('shell').feature('to4').set('OffsetDefinition', 'RelativeDistance');
model.component('comp1').physics('shell').feature('to4').set('z_offset_rel', -1);
model.component('comp1').physics('shell').feature('to5').set('OffsetDefinition', 'RelativeDistance');
model.component('comp1').physics('shell').feature('to5').set('z_offset_rel', -1);
model.component('comp1').physics('shell').feature('to6').set('OffsetDefinition', 'RelativeDistance');
model.component('comp1').physics('shell').feature('to6').set('z_offset_rel', -1);
model.component('comp1').physics('shell').feature('to7').set('OffsetDefinition', 'RelativeDistance');
model.component('comp1').physics('shell').feature('to7').set('z_offset_rel', -1);
model.component('comp1').physics('shell').feature('to8').set('OffsetDefinition', 'RelativeDistance');
model.component('comp1').physics('shell').feature('to8').set('z_offset_rel', -1);
model.component('comp1').physics('shell').feature('to9').set('OffsetDefinition', 'RelativeDistance');
model.component('comp1').physics('shell').feature('to9').set('z_offset_rel', -1);
model.component('comp1').physics('shell').feature('to10').set('OffsetDefinition', 'RelativeDistance');
model.component('comp1').physics('shell').feature('to10').set('z_offset_rel', -1);

model.sol('sol2').runAll;

model.result('pg13').run;
model.result.evaluationGroup('std2EvgFrq').run;
model.result.evaluationGroup('std2EvgFrq').save('C:\Users\silveiralucas\OneDrive - Syddansk Universitet\Reports\Craig-Bampton\code\comsol\tower_connector.txt');

model.label('shell_tower_with_tower_top_nacelle.mph');

model.sol('sol2').runAll;

model.result('pg13').run;
model.result.evaluationGroup('std2EvgFrq').run;

model.component('comp1').physics('beam').feature('pm1').active(true);

model.sol('sol2').runAll;

model.result('pg13').run;

model.component('comp1').physics('beam').feature('pm1').set('mmi3D', [7366701.98937148 0 0 0 450907.01661148 -360283.9347828 0 -360283.9347828 7326394.97276]);

model.sol('sol2').runAll;

model.result('pg13').run;
model.result.evaluationGroup('std2EvgFrq').run;
model.result.evaluationGroup('std2EvgFrq').save('C:\Users\silveiralucas\OneDrive - Syddansk Universitet\Reports\Craig-Bampton\code\comsol\tower_nacelle.txt');
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 5, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 4, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 3, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 2, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 1, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 2, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 3, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 4, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 5, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 6, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 5, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 4, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 3, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 2, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 1, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 2, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 3, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 2, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 1, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 2, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 3, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 4, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 5, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 6, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 7, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 8, 0);
model.result('pg13').run;
model.result('pg13').setIndex('looplevel', 9, 0);
model.result('pg13').run;

model.component('comp1').physics('shell').create('srig1', 'RigidConnectorShell', 1);
model.component('comp1').physics('shell').feature('srig1').selection.set([31 32 53 56]);

model.sol('sol2').runAll;

model.result('pg13').run;
model.result.evaluationGroup('std2EvgFrq').run;

model.component('comp1').material('mat2').propertyGroup('def').set('density', {'8500'});

model.sol('sol2').runAll;

model.result('pg13').run;
model.result.evaluationGroup('std2EvgFrq').run;
model.result.evaluationGroup('std2EvgFrq').save('C:\Users\silveiralucas\OneDrive - Syddansk Universitet\Reports\Craig-Bampton\code\comsol\tower_nacelle.txt');

model.component('comp1').physics('shell').feature('srig1').active(false);
model.component('comp1').physics('beam').feature('pm1').set('pointmass', '446040.0');
model.component('comp1').physics('beam').feature('pm1').set('mmi3D', [7366701.98937148 0 0 0 450907.01661148 -360283.9347828 0 -360283.9347828 7326394.9727]);

model.label('shell_tower_with_tower_top_nacelle.mph');

out = model;
