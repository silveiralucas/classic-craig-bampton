% Getting the COMSOL problem matrices
str = mphmatrix(model,'sol2','out',{'M','K','L','D','E','Kc','Lc','Dc','Ec', 'N','MLB','MUB','Null','Nullf'},'initmethod','init');

% Getting the COMSOLproblem mesh informations
info = mphxmeshinfo(model);

% % Mesh nodal coordinates
% x = info.nodes.coords(1, 1:end)';
% y = info.nodes.coords(2, 1:end)';
% z = info.nodes.coords(3, 1:end)';

%%

n_nodes = size(info.nodes.coords, 2);
n_dim = size(info.nodes.coords, 1);
n_dofspnode = size(info.nodes.dofs, 1);
n_dofs = info.ndofs;

clear nodes;
nodes.coords = (info.nodes.coords).';
nodes.number = (1:n_nodes);
nodes.dofs = (info.nodes.dofs).';

clear quad;
quad.nodes = (info.elements.quad.nodes).';

clear sys;
sys.M = str.E;
sys.K = str.K;
sys.D = str.D;
sys.C = str.M;
sys.L = str.L.';
sys.Null = str.Null;
sys.Nullf = str.Nullf;
sys.Mc = str.Ec;
sys.Kc = str.Kc;

save('shell_tower.mat', 'sys', 'nodes', 'quad');

%%
