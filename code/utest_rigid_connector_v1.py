import numpy as np
import scipy.linalg as sa
import fem
import copy

from mayavi import mlab

#%% Local functions

# Empty class
class empty_object(object):
    def copy(self):
        return copy.deepcopy(self)

# Rotation matrix
def rotation_matrix(angle,axis): # return A
    '''
    Generates a transformation matrix.
    
    Parameters
    ----------
    angle : sympy.Symbol or sympy.Function
            [rad] angle between the two reference frames along the 'axis'
    axis : str
           axis of roation. 'x', 'y' and 'z'.
    
    Returns
    -------
    A : sympy.Matrix [:, :]
        transformation matrix from reference frame 1 to reference frame 2.
    '''
    if (axis=='x'):
        A = np.array([[1., 0., 0.],
                    [0., np.cos(angle), np.sin(angle)],
                    [0., -np.sin(angle), np.cos(angle)]], dtype=float)
    elif (axis=='y'):
        A = np.array([[np.cos(angle), 0., -np.sin(angle)],
                    [0., 1., 0.],
                    [np.sin(angle), 0., np.cos(angle)]], dtype=float)
    elif (axis=='z'):
        A = np.array([[np.cos(angle), np.sin(angle), 0.],
                    [-np.sin(angle), np.cos(angle), 0.],
                    [0., 0., 1.]])
    else:
        print('error in rotate_matrix function')
    
    return A

#%% Random test

tri_t0 = empty_object()

# Random triangle nodes
tri_t0.p_0 = np.random.rand(3)
tri_t0.p_1 = np.random.rand(3)
tri_t0.p_2 = np.random.rand(3)

# Random rotation centre
p_c = np.random.rand(3)

# Random rotation angles (<5 deg)
phi = np.random.rand(1)[0]*5. * (np.pi/180.)
beta = np.random.rand(1)[0]*5. * (np.pi/180.)
gamma = np.random.rand(1)[0]*5. * (np.pi/180.)

#--

# Local unit vectors (at t_0)
tri_t0.v_0 = (tri_t0.p_1 - tri_t0.p_0)
tri_t0.v_1 = (tri_t0.p_2 - tri_t0.p_0)
tri_t0.v_2 = np.cross(tri_t0.v_0, tri_t0.v_1)
tri_t0.e_0 = tri_t0.v_0/sa.norm(tri_t0.v_0)
tri_t0.e_2 = tri_t0.v_2/sa.norm(tri_t0.v_2)
tri_t0.e_1 = np.cross(tri_t0.e_2, tri_t0.e_0) / sa.norm(np.cross(tri_t0.e_2, tri_t0.e_0))

# Rotation matrix
A_01 = rotation_matrix(gamma, 'z') @ rotation_matrix(beta, 'y') @ rotation_matrix(phi, 'x')
# A_01 = np.array([[1., gamma, -beta], 
#                  [-gamma, 1., phi], 
#                  [beta, -phi, 1.]])


# Rotate the triangle
# r_p(t_1) = r_c(t_1) + A_01.T @ r_cp
tri_t1 = empty_object()
tri_t1.p_0 = p_c + A_01.T @ (tri_t0.p_0 - p_c)
tri_t1.p_1 = p_c + A_01.T @ (tri_t0.p_1 - p_c)
tri_t1.p_2 = p_c + A_01.T @ (tri_t0.p_2 - p_c)

# Local unit vectors (at t_1)
tri_t1.v_0 = (tri_t1.p_1 - tri_t1.p_0)
tri_t1.v_1 = (tri_t1.p_2 - tri_t1.p_0)
tri_t1.v_2 = np.cross(tri_t1.v_0, tri_t1.v_1)
tri_t1.e_0 = tri_t1.v_0/sa.norm(tri_t1.v_0)
tri_t1.e_2 = tri_t1.v_2/sa.norm(tri_t1.v_2)
tri_t1.e_1 = np.cross(tri_t1.e_2, tri_t1.e_0) / sa.norm(np.cross(tri_t1.e_2, tri_t1.e_0))

# Linear displacement
u = tri_t1.p_0 - tri_t0.p_0

# Angular displacement
a = tri_t1.e_2 - tri_t0.e_2

# Unit test
#----------
Ae = fem.rigid_connector(tri_t0.p_0, p_c, tri_t0.e_0, tri_t0.e_1)
u_c = np.array([0., 0., 0., phi, beta, gamma], dtype=float)
u_p = Ae @ u_c

# Transformation of coordinates (local -> global)
L_ij = np.block([[tri_t0.e_0], 
                  [tri_t0.e_1], 
                  [tri_t0.e_2]])
a_p = L_ij.T @ np.array([u_p[0], u_p[1], 0.])

# (A_01.T - np.eye(3)) @ tri_t0.e_2

# Verification parameter
res_u = (u - u_p[-3:])/sa.norm(u)
res_a = (a - a_p)/sa.norm(a)
res = sa.norm(np.concatenate([res_u, res_a]))
print('error = %0.3f '%res)

# plot
# ----

r_e = np.block([[tri_t0.p_0],
                [tri_t0.p_1], 
                [tri_t0.p_2], 
                [tri_t1.p_0], 
                [tri_t1.p_1], 
                [tri_t1.p_2], 
                [p_c]])

x, y, z = r_e[:, 0], r_e[:, 1], r_e[:, 2]
triangles = np.arange(6).reshape((2, 3))

fig = mlab.figure()
mlab.triangular_mesh(x, y, z, triangles[0:1, :], color=(0., 0., 0.5))
mlab.triangular_mesh(x, y, z, triangles[1:2, :], color=(0., 0.5, 0.))
mlab.points3d(x, y, z, scale_factor=0.025)
mlab.points3d(x[0], y[0], z[0], scale_factor=0.025, color=(0.5, 0., 0.))
mlab.points3d(x[3], y[3], z[3], scale_factor=0.025, color=(0.5, 0., 0.))
for i in range(6):
    mlab.plot3d(x[[i, 6]], y[[i, 6]], z[[i, 6]], tube_radius=.0025)

p0 = tri_t0.p_0 + u_p[-3:]
mlab.points3d(p0[-3:][0], p0[-3:][1], p0[-3:][2], scale_factor=0.025, color=(0., 0.5, 0.))
mlab.quiver3d(x[0], y[0], z[0], tri_t0.e_2[0], tri_t0.e_2[1], tri_t0.e_2[2], scale_factor=0.1)
mlab.quiver3d(p0[-3:][0], p0[-3:][1], p0[-3:][2], (tri_t0.e_2+a_p)[0], (tri_t0.e_2+a_p)[1], (tri_t0.e_2+a_p)[2], scale_factor=0.1)
