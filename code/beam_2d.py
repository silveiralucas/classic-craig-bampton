import numpy as np
import scipy.linalg as sa
import copy

import fem

#%% Functions

def boolean_matrix(a, b): # return L
    '''
    Boolean transformation matrix between coordinates a and b.
    '''
    L = np.zeros((a.size, b.size), dtype=int)
    for i in range(a.size):
        for j in range(b.size):
            if (a[i]==b[j]):
                L[i, j] = 1
    return L


def beam_2d_circular(x, d_out, d_int, rho, E):
    
    l = x[1] - x[0]
    D_0, D_1 = d_out
    d_0, d_1 = d_int
    
    K_e = np.zeros((6, 6), dtype=float)
    K_e[0, 0] = (1/12)*np.pi*D_0**2*E/l + (1/12)*np.pi*D_0*D_1*E/l + (1/12)*np.pi*D_1**2*E/l - 1/12*np.pi*E*d_0**2/l - 1/12*np.pi*E*d_0*d_1/l - 1/12*np.pi*E*d_1**2/l
    K_e[0, 3] = -1/12*np.pi*D_0**2*E/l - 1/12*np.pi*D_0*D_1*E/l - 1/12*np.pi*D_1**2*E/l + (1/12)*np.pi*E*d_0**2/l + (1/12)*np.pi*E*d_0*d_1/l + (1/12)*np.pi*E*d_1**2/l
    K_e[1, 1] = (33/560)*np.pi*D_0**4*E/l**3 + (3/112)*np.pi*D_0**3*D_1*E/l**3 + (9/560)*np.pi*D_0**2*D_1**2*E/l**3 + (3/112)*np.pi*D_0*D_1**3*E/l**3 + (33/560)*np.pi*D_1**4*E/l**3 - 33/560*np.pi*E*d_0**4/l**3 - 3/112*np.pi*E*d_0**3*d_1/l**3 - 9/560*np.pi*E*d_0**2*d_1**2/l**3 - 3/112*np.pi*E*d_0*d_1**3/l**3 - 33/560*np.pi*E*d_1**4/l**3
    K_e[1, 2] = (47/1120)*np.pi*D_0**4*E/l**2 + (11/560)*np.pi*D_0**3*D_1*E/l**2 + (9/1120)*np.pi*D_0**2*D_1**2*E/l**2 + (1/140)*np.pi*D_0*D_1**3*E/l**2 + (19/1120)*np.pi*D_1**4*E/l**2 - 47/1120*np.pi*E*d_0**4/l**2 - 11/560*np.pi*E*d_0**3*d_1/l**2 - 9/1120*np.pi*E*d_0**2*d_1**2/l**2 - 1/140*np.pi*E*d_0*d_1**3/l**2 - 19/1120*np.pi*E*d_1**4/l**2
    K_e[1, 4] = -33/560*np.pi*D_0**4*E/l**3 - 3/112*np.pi*D_0**3*D_1*E/l**3 - 9/560*np.pi*D_0**2*D_1**2*E/l**3 - 3/112*np.pi*D_0*D_1**3*E/l**3 - 33/560*np.pi*D_1**4*E/l**3 + (33/560)*np.pi*E*d_0**4/l**3 + (3/112)*np.pi*E*d_0**3*d_1/l**3 + (9/560)*np.pi*E*d_0**2*d_1**2/l**3 + (3/112)*np.pi*E*d_0*d_1**3/l**3 + (33/560)*np.pi*E*d_1**4/l**3
    K_e[1, 5] = (19/1120)*np.pi*D_0**4*E/l**2 + (1/140)*np.pi*D_0**3*D_1*E/l**2 + (9/1120)*np.pi*D_0**2*D_1**2*E/l**2 + (11/560)*np.pi*D_0*D_1**3*E/l**2 + (47/1120)*np.pi*D_1**4*E/l**2 - 19/1120*np.pi*E*d_0**4/l**2 - 1/140*np.pi*E*d_0**3*d_1/l**2 - 9/1120*np.pi*E*d_0**2*d_1**2/l**2 - 11/560*np.pi*E*d_0*d_1**3/l**2 - 47/1120*np.pi*E*d_1**4/l**2
    K_e[2, 1] = (47/1120)*np.pi*D_0**4*E/l**2 + (11/560)*np.pi*D_0**3*D_1*E/l**2 + (9/1120)*np.pi*D_0**2*D_1**2*E/l**2 + (1/140)*np.pi*D_0*D_1**3*E/l**2 + (19/1120)*np.pi*D_1**4*E/l**2 - 47/1120*np.pi*E*d_0**4/l**2 - 11/560*np.pi*E*d_0**3*d_1/l**2 - 9/1120*np.pi*E*d_0**2*d_1**2/l**2 - 1/140*np.pi*E*d_0*d_1**3/l**2 - 19/1120*np.pi*E*d_1**4/l**2
    K_e[2, 2] = (17/560)*np.pi*D_0**4*E/l + (9/560)*np.pi*D_0**3*D_1*E/l + (1/140)*np.pi*D_0**2*D_1**2*E/l + (1/280)*np.pi*D_0*D_1**3*E/l + (3/560)*np.pi*D_1**4*E/l - 17/560*np.pi*E*d_0**4/l - 9/560*np.pi*E*d_0**3*d_1/l - 1/140*np.pi*E*d_0**2*d_1**2/l - 1/280*np.pi*E*d_0*d_1**3/l - 3/560*np.pi*E*d_1**4/l
    K_e[2, 4] = -47/1120*np.pi*D_0**4*E/l**2 - 11/560*np.pi*D_0**3*D_1*E/l**2 - 9/1120*np.pi*D_0**2*D_1**2*E/l**2 - 1/140*np.pi*D_0*D_1**3*E/l**2 - 19/1120*np.pi*D_1**4*E/l**2 + (47/1120)*np.pi*E*d_0**4/l**2 + (11/560)*np.pi*E*d_0**3*d_1/l**2 + (9/1120)*np.pi*E*d_0**2*d_1**2/l**2 + (1/140)*np.pi*E*d_0*d_1**3/l**2 + (19/1120)*np.pi*E*d_1**4/l**2
    K_e[2, 5] = (13/1120)*np.pi*D_0**4*E/l + (1/280)*np.pi*D_0**3*D_1*E/l + (1/1120)*np.pi*D_0**2*D_1**2*E/l + (1/280)*np.pi*D_0*D_1**3*E/l + (13/1120)*np.pi*D_1**4*E/l - 13/1120*np.pi*E*d_0**4/l - 1/280*np.pi*E*d_0**3*d_1/l - 1/1120*np.pi*E*d_0**2*d_1**2/l - 1/280*np.pi*E*d_0*d_1**3/l - 13/1120*np.pi*E*d_1**4/l
    K_e[3, 0] = -1/12*np.pi*D_0**2*E/l - 1/12*np.pi*D_0*D_1*E/l - 1/12*np.pi*D_1**2*E/l + (1/12)*np.pi*E*d_0**2/l + (1/12)*np.pi*E*d_0*d_1/l + (1/12)*np.pi*E*d_1**2/l
    K_e[3, 3] = (1/12)*np.pi*D_0**2*E/l + (1/12)*np.pi*D_0*D_1*E/l + (1/12)*np.pi*D_1**2*E/l - 1/12*np.pi*E*d_0**2/l - 1/12*np.pi*E*d_0*d_1/l - 1/12*np.pi*E*d_1**2/l
    K_e[4, 1] = -33/560*np.pi*D_0**4*E/l**3 - 3/112*np.pi*D_0**3*D_1*E/l**3 - 9/560*np.pi*D_0**2*D_1**2*E/l**3 - 3/112*np.pi*D_0*D_1**3*E/l**3 - 33/560*np.pi*D_1**4*E/l**3 + (33/560)*np.pi*E*d_0**4/l**3 + (3/112)*np.pi*E*d_0**3*d_1/l**3 + (9/560)*np.pi*E*d_0**2*d_1**2/l**3 + (3/112)*np.pi*E*d_0*d_1**3/l**3 + (33/560)*np.pi*E*d_1**4/l**3
    K_e[4, 2] = -47/1120*np.pi*D_0**4*E/l**2 - 11/560*np.pi*D_0**3*D_1*E/l**2 - 9/1120*np.pi*D_0**2*D_1**2*E/l**2 - 1/140*np.pi*D_0*D_1**3*E/l**2 - 19/1120*np.pi*D_1**4*E/l**2 + (47/1120)*np.pi*E*d_0**4/l**2 + (11/560)*np.pi*E*d_0**3*d_1/l**2 + (9/1120)*np.pi*E*d_0**2*d_1**2/l**2 + (1/140)*np.pi*E*d_0*d_1**3/l**2 + (19/1120)*np.pi*E*d_1**4/l**2
    K_e[4, 4] = (33/560)*np.pi*D_0**4*E/l**3 + (3/112)*np.pi*D_0**3*D_1*E/l**3 + (9/560)*np.pi*D_0**2*D_1**2*E/l**3 + (3/112)*np.pi*D_0*D_1**3*E/l**3 + (33/560)*np.pi*D_1**4*E/l**3 - 33/560*np.pi*E*d_0**4/l**3 - 3/112*np.pi*E*d_0**3*d_1/l**3 - 9/560*np.pi*E*d_0**2*d_1**2/l**3 - 3/112*np.pi*E*d_0*d_1**3/l**3 - 33/560*np.pi*E*d_1**4/l**3
    K_e[4, 5] = -19/1120*np.pi*D_0**4*E/l**2 - 1/140*np.pi*D_0**3*D_1*E/l**2 - 9/1120*np.pi*D_0**2*D_1**2*E/l**2 - 11/560*np.pi*D_0*D_1**3*E/l**2 - 47/1120*np.pi*D_1**4*E/l**2 + (19/1120)*np.pi*E*d_0**4/l**2 + (1/140)*np.pi*E*d_0**3*d_1/l**2 + (9/1120)*np.pi*E*d_0**2*d_1**2/l**2 + (11/560)*np.pi*E*d_0*d_1**3/l**2 + (47/1120)*np.pi*E*d_1**4/l**2
    K_e[5, 1] = (19/1120)*np.pi*D_0**4*E/l**2 + (1/140)*np.pi*D_0**3*D_1*E/l**2 + (9/1120)*np.pi*D_0**2*D_1**2*E/l**2 + (11/560)*np.pi*D_0*D_1**3*E/l**2 + (47/1120)*np.pi*D_1**4*E/l**2 - 19/1120*np.pi*E*d_0**4/l**2 - 1/140*np.pi*E*d_0**3*d_1/l**2 - 9/1120*np.pi*E*d_0**2*d_1**2/l**2 - 11/560*np.pi*E*d_0*d_1**3/l**2 - 47/1120*np.pi*E*d_1**4/l**2
    K_e[5, 2] = (13/1120)*np.pi*D_0**4*E/l + (1/280)*np.pi*D_0**3*D_1*E/l + (1/1120)*np.pi*D_0**2*D_1**2*E/l + (1/280)*np.pi*D_0*D_1**3*E/l + (13/1120)*np.pi*D_1**4*E/l - 13/1120*np.pi*E*d_0**4/l - 1/280*np.pi*E*d_0**3*d_1/l - 1/1120*np.pi*E*d_0**2*d_1**2/l - 1/280*np.pi*E*d_0*d_1**3/l - 13/1120*np.pi*E*d_1**4/l
    K_e[5, 4] = -19/1120*np.pi*D_0**4*E/l**2 - 1/140*np.pi*D_0**3*D_1*E/l**2 - 9/1120*np.pi*D_0**2*D_1**2*E/l**2 - 11/560*np.pi*D_0*D_1**3*E/l**2 - 47/1120*np.pi*D_1**4*E/l**2 + (19/1120)*np.pi*E*d_0**4/l**2 + (1/140)*np.pi*E*d_0**3*d_1/l**2 + (9/1120)*np.pi*E*d_0**2*d_1**2/l**2 + (11/560)*np.pi*E*d_0*d_1**3/l**2 + (47/1120)*np.pi*E*d_1**4/l**2
    K_e[5, 5] = (3/560)*np.pi*D_0**4*E/l + (1/280)*np.pi*D_0**3*D_1*E/l + (1/140)*np.pi*D_0**2*D_1**2*E/l + (9/560)*np.pi*D_0*D_1**3*E/l + (17/560)*np.pi*D_1**4*E/l - 3/560*np.pi*E*d_0**4/l - 1/280*np.pi*E*d_0**3*d_1/l - 1/140*np.pi*E*d_0**2*d_1**2/l - 9/560*np.pi*E*d_0*d_1**3/l - 17/560*np.pi*E*d_1**4/l
    
    M_e = np.zeros((6, 6), dtype=float)
    M_e[0, 0] = (1/20)*np.pi*D_0**2*l*rho + (1/40)*np.pi*D_0*D_1*l*rho + (1/120)*np.pi*D_1**2*l*rho - 1/20*np.pi*d_0**2*l*rho - 1/40*np.pi*d_0*d_1*l*rho - 1/120*np.pi*d_1**2*l*rho
    M_e[0, 3] = (1/80)*np.pi*D_0**2*l*rho + (1/60)*np.pi*D_0*D_1*l*rho + (1/80)*np.pi*D_1**2*l*rho - 1/80*np.pi*d_0**2*l*rho - 1/60*np.pi*d_0*d_1*l*rho - 1/80*np.pi*d_1**2*l*rho
    M_e[1, 1] = (29/504)*np.pi*D_0**2*l*rho + (1/36)*np.pi*D_0*D_1*l*rho + (19/2520)*np.pi*D_1**2*l*rho - 29/504*np.pi*d_0**2*l*rho - 1/36*np.pi*d_0*d_1*l*rho - 19/2520*np.pi*d_1**2*l*rho
    M_e[1, 2] = (13/2016)*np.pi*D_0**2*l**2*rho + (5/1008)*np.pi*D_0*D_1*l**2*rho + (17/10080)*np.pi*D_1**2*l**2*rho - 13/2016*np.pi*d_0**2*l**2*rho - 5/1008*np.pi*d_0*d_1*l**2*rho - 17/10080*np.pi*d_1**2*l**2*rho
    M_e[1, 4] = (23/2520)*np.pi*D_0**2*l*rho + (1/72)*np.pi*D_0*D_1*l*rho + (23/2520)*np.pi*D_1**2*l*rho - 23/2520*np.pi*d_0**2*l*rho - 1/72*np.pi*d_0*d_1*l*rho - 23/2520*np.pi*d_1**2*l*rho
    M_e[1, 5] = -5/2016*np.pi*D_0**2*l**2*rho - 17/5040*np.pi*D_0*D_1*l**2*rho - 19/10080*np.pi*D_1**2*l**2*rho + (5/2016)*np.pi*d_0**2*l**2*rho + (17/5040)*np.pi*d_0*d_1*l**2*rho + (19/10080)*np.pi*d_1**2*l**2*rho
    M_e[2, 1] = (13/2016)*np.pi*D_0**2*l**2*rho + (5/1008)*np.pi*D_0*D_1*l**2*rho + (17/10080)*np.pi*D_1**2*l**2*rho - 13/2016*np.pi*d_0**2*l**2*rho - 5/1008*np.pi*d_0*d_1*l**2*rho - 17/10080*np.pi*d_1**2*l**2*rho
    M_e[2, 2] = (1/1008)*np.pi*D_0**2*l**3*rho + (1/1008)*np.pi*D_0*D_1*l**3*rho + (1/2520)*np.pi*D_1**2*l**3*rho - 1/1008*np.pi*d_0**2*l**3*rho - 1/1008*np.pi*d_0*d_1*l**3*rho - 1/2520*np.pi*d_1**2*l**3*rho
    M_e[2, 4] = (19/10080)*np.pi*D_0**2*l**2*rho + (17/5040)*np.pi*D_0*D_1*l**2*rho + (5/2016)*np.pi*D_1**2*l**2*rho - 19/10080*np.pi*d_0**2*l**2*rho - 17/5040*np.pi*d_0*d_1*l**2*rho - 5/2016*np.pi*d_1**2*l**2*rho
    M_e[2, 5] = -1/2016*np.pi*D_0**2*l**3*rho - 1/1260*np.pi*D_0*D_1*l**3*rho - 1/2016*np.pi*D_1**2*l**3*rho + (1/2016)*np.pi*d_0**2*l**3*rho + (1/1260)*np.pi*d_0*d_1*l**3*rho + (1/2016)*np.pi*d_1**2*l**3*rho
    M_e[3, 0] = (1/80)*np.pi*D_0**2*l*rho + (1/60)*np.pi*D_0*D_1*l*rho + (1/80)*np.pi*D_1**2*l*rho - 1/80*np.pi*d_0**2*l*rho - 1/60*np.pi*d_0*d_1*l*rho - 1/80*np.pi*d_1**2*l*rho
    M_e[3, 3] = (1/120)*np.pi*D_0**2*l*rho + (1/40)*np.pi*D_0*D_1*l*rho + (1/20)*np.pi*D_1**2*l*rho - 1/120*np.pi*d_0**2*l*rho - 1/40*np.pi*d_0*d_1*l*rho - 1/20*np.pi*d_1**2*l*rho
    M_e[4, 1] = (23/2520)*np.pi*D_0**2*l*rho + (1/72)*np.pi*D_0*D_1*l*rho + (23/2520)*np.pi*D_1**2*l*rho - 23/2520*np.pi*d_0**2*l*rho - 1/72*np.pi*d_0*d_1*l*rho - 23/2520*np.pi*d_1**2*l*rho
    M_e[4, 2] = (19/10080)*np.pi*D_0**2*l**2*rho + (17/5040)*np.pi*D_0*D_1*l**2*rho + (5/2016)*np.pi*D_1**2*l**2*rho - 19/10080*np.pi*d_0**2*l**2*rho - 17/5040*np.pi*d_0*d_1*l**2*rho - 5/2016*np.pi*d_1**2*l**2*rho
    M_e[4, 4] = (19/2520)*np.pi*D_0**2*l*rho + (1/36)*np.pi*D_0*D_1*l*rho + (29/504)*np.pi*D_1**2*l*rho - 19/2520*np.pi*d_0**2*l*rho - 1/36*np.pi*d_0*d_1*l*rho - 29/504*np.pi*d_1**2*l*rho
    M_e[4, 5] = -17/10080*np.pi*D_0**2*l**2*rho - 5/1008*np.pi*D_0*D_1*l**2*rho - 13/2016*np.pi*D_1**2*l**2*rho + (17/10080)*np.pi*d_0**2*l**2*rho + (5/1008)*np.pi*d_0*d_1*l**2*rho + (13/2016)*np.pi*d_1**2*l**2*rho
    M_e[5, 1] = -5/2016*np.pi*D_0**2*l**2*rho - 17/5040*np.pi*D_0*D_1*l**2*rho - 19/10080*np.pi*D_1**2*l**2*rho + (5/2016)*np.pi*d_0**2*l**2*rho + (17/5040)*np.pi*d_0*d_1*l**2*rho + (19/10080)*np.pi*d_1**2*l**2*rho
    M_e[5, 2] = -1/2016*np.pi*D_0**2*l**3*rho - 1/1260*np.pi*D_0*D_1*l**3*rho - 1/2016*np.pi*D_1**2*l**3*rho + (1/2016)*np.pi*d_0**2*l**3*rho + (1/1260)*np.pi*d_0*d_1*l**3*rho + (1/2016)*np.pi*d_1**2*l**3*rho
    M_e[5, 4] = -17/10080*np.pi*D_0**2*l**2*rho - 5/1008*np.pi*D_0*D_1*l**2*rho - 13/2016*np.pi*D_1**2*l**2*rho + (17/10080)*np.pi*d_0**2*l**2*rho + (5/1008)*np.pi*d_0*d_1*l**2*rho + (13/2016)*np.pi*d_1**2*l**2*rho
    M_e[5, 5] = (1/2520)*np.pi*D_0**2*l**3*rho + (1/1008)*np.pi*D_0*D_1*l**3*rho + (1/1008)*np.pi*D_1**2*l**3*rho - 1/2520*np.pi*d_0**2*l**3*rho - 1/1008*np.pi*d_0*d_1*l**3*rho - 1/1008*np.pi*d_1**2*l**3*rho
    
    return M_e, K_e

def concentrated_inertia_2D(m, I, r=0.):

    # Offset, parallel axis theorem    
    I += m*(r**2)
    
    M_e = np.diag([m, m, I])
    K_e = np.zeros(M_e.shape)
    
    return M_e, K_e

def freq_h(M, K):
    vals, vecs = sa.eigh(M, K)
    
    f_n = 1/(2*np.pi)*np.sqrt(1./vals)
    idx = np.argsort(f_n)
    f_n = f_n[idx]
    vecs = vecs[:, idx]
    
    return f_n, vecs

#%% Classes

class EmptyObject(object):
    def copy(self):
        return copy.deepcopy(self)

class CircularBeam2D(object):
    
    def __init__(self, x, d_out, d_int, rho, E):

        self.n_npe = 2
        self.n_dpn = 3

        self.x = x
        
        self.n_nodes = x.size
        self.n_elms = self.n_nodes - 1
        self.n_dofs = self.n_nodes * self.n_dpn

        self.d_out = d_out if isinstance(d_out, np.ndarray) else d_out*np.ones((self.n_elms, self.n_npe))
        self.d_int = d_int if isinstance(d_int, np.ndarray) else d_int*np.ones((self.n_elms, self.n_npe))
        self.rho = rho if isinstance(rho, np.ndarray) else rho*np.ones((self.n_elms, self.n_npe))
        self.E = rho if isinstance(E, np.ndarray) else E*np.ones((self.n_elms, self.n_npe))
        
        self.nodes = np.array([[i_e, i_e+1] for i_e in range(self.n_elms)])
        self.dofs = np.arange(self.n_dofs).reshape((self.n_nodes, self.n_dpn))
        self.cts = np.zeros(self.dofs.shape, dtype=bool)
        
        self.q_idx = np.arange(self.n_dofs)
        self.M = np.zeros((self.n_dofs, self.n_dofs))
        self.K = np.zeros((self.n_dofs, self.n_dofs))

        for i_e in range(self.n_elms):
            n0, n1 = self.nodes[i_e, :]
            qe_idx = np.concatenate((self.dofs[n0, :], self.dofs[n1, :]))
            Le = boolean_matrix(qe_idx, self.q_idx)
            
            rho = self.rho[i_e, :].mean()
            E = self.E[i_e, :].mean()
            
            Me, Ke = beam_2d_circular(self.x[n0:n1+1], self.d_out[i_e, :], self.d_int[i_e, :], rho, E)
            self.M += Le.T @ Me @ Le
            self.K += Le.T @ Ke @ Le

    def fixed_constraint(self, cts=None):
        
        if (cts is not None):        
            self.cts = cts

        q_bool = ~self.cts.flatten()
        
        self.M = self.M[np.ix_(q_bool, q_bool)]
        self.K = self.K[np.ix_(q_bool, q_bool)]
        
        k = 0
        for i in range(self.n_nodes):
            for j in range(self.n_dpn):
                if (self.cts[i, j]):
                    self.dofs[i, j] = -1
                else:
                    self.dofs[i, j] = k
                    k += 1
        self.n_dofs = k
        self.q_idx = np.arange(self.n_dofs)


class Beam2D(object):
    
    def __init__(self, x, A, I, rho, E):

        self.n_npe = 2
        self.n_dpn = 3

        self.x = x
        
        self.n_nodes = x.size
        self.n_elms = self.n_nodes - 1
        self.n_dofs = self.n_nodes * self.n_dpn

        self.A = A if isinstance(A, np.ndarray) else A*np.ones((self.n_elms, self.n_npe))
        self.I = I if isinstance(I, np.ndarray) else I*np.ones((self.n_elms, self.n_npe))
        self.rho = rho if isinstance(rho, np.ndarray) else rho*np.ones((self.n_elms, self.n_npe))
        self.E = rho if isinstance(E, np.ndarray) else E*np.ones((self.n_elms, self.n_npe))
        
        self.nodes = np.array([[i_e, i_e+1] for i_e in range(self.n_elms)])
        self.dofs = np.arange(self.n_dofs).reshape((self.n_nodes, self.n_dpn))
        self.cts = np.zeros(self.dofs.shape, dtype=bool)
        
        self.q_idx = np.arange(self.n_dofs)
        self.M = np.zeros((self.n_dofs, self.n_dofs))
        self.K = np.zeros((self.n_dofs, self.n_dofs))

        for i_e in range(self.n_elms):
            n0, n1 = self.nodes[i_e, :]
            qe_idx = np.concatenate((self.dofs[n0, :], self.dofs[n1, :]))
            Le = boolean_matrix(qe_idx, self.q_idx)
            
            rho = self.rho[i_e, :].mean()
            E = self.E[i_e, :].mean()
            A = self.A[i_e, :].mean()
            I = self.I[i_e, :].mean()
            
            Me, Ke = fem.beam_2D_const(self.x[n0:n1+1], A, I, rho, E)
            self.M += Le.T @ Me @ Le
            self.K += Le.T @ Ke @ Le

    def fixed_constraint(self, cts=None):
        
        if (cts is not None):        
            self.cts = cts

        q_bool = ~self.cts.flatten()
        
        self.M = self.M[np.ix_(q_bool, q_bool)]
        self.K = self.K[np.ix_(q_bool, q_bool)]
        
        k = 0
        for i in range(self.n_nodes):
            for j in range(self.n_dpn):
                if (self.cts[i, j]):
                    self.dofs[i, j] = -1
                else:
                    self.dofs[i, j] = k
                    k += 1
        self.n_dofs = k
        self.q_idx = np.arange(self.n_dofs)

class ConcentratedInertia(object):
    
    def __init__(self, m, I, r=0.):
        I += m*(r**2)
    
        self.M = np.diag([m, m, I])
        self.K = np.zeros(self.M.shape)

