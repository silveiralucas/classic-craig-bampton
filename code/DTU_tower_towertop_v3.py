import numpy as np
import scipy.io as sio
import scipy.linalg as sa
import scipy.sparse as ss
import scipy.sparse.linalg as ssl
import scipy.optimize as opt
import copy
from numba import jit
from mayavi import mlab
import custom_functions as cf

from time import time
import matplotlib.pyplot as plt

import fem

#%%

plot_figures = True
export_figures = True
export_tables = True

#%%

# Empty class
class empty_object(object):
    def copy(self):
        return copy.deepcopy(self)

# Loading Matlab data
def load_from_livelink(file_name): # return: nodes, sys
    toto = sio.loadmat(file_name)
    
    sys = empty_object()
    sys.coord = toto['nodes'][0][0][0]
    sys.node = toto['nodes'][0][0][1][0] - 1
    sys.dof = toto['nodes'][0][0][2]
    
    sys.n_nodes = sys.node.size
    sys.n_dofs = sys.dof.size
    
    sys.quad = toto['quad'][0][0][0]
    
    sys.n_elms = sys.quad.shape[0]
    
    sys.M = (toto['sys'][0][0][0])
    sys.K = (toto['sys'][0][0][1])
    sys.D = (toto['sys'][0][0][2])
    sys.C = (toto['sys'][0][0][3])
    sys.L = (toto['sys'][0][0][4][0])
    sys.N = (toto['sys'][0][0][5])
    sys.Nf = (toto['sys'][0][0][6])
    sys.Mc = (toto['sys'][0][0][7])
    sys.Kc = (toto['sys'][0][0][8])
    
    return sys

@jit
def boolean_matrix(a, b):
    
    # Get matrix sparsity
    n = 0
    for i in range(a.size):
        for j in range(b.size):
            if (a[i]==b[j]):
                n += 1
    
    data = np.ones((n,), dtype=int)
    idx = np.zeros((n,), dtype=int)
    jdx = np.zeros((n,), dtype=int)
    n = 0
    for i in range(a.size):
        for j in range(b.size):
            if (a[i]==b[j]):
                idx[n] = i
                jdx[n] = j
                n += 1
    
    L = ss.csr_matrix((data, (idx, jdx)), shape=(a.size, b.size))
    
    return L

def freq_h(M, K, n_m=20):

    t0 = time()
    vals, vecs = ssl.eigsh(A=M, k=n_m, M=K, maxiter=M.shape[0]*100, tol=1.e-50)
    f_n = 1./(2.*np.pi) * np.sqrt(1./vals)
    idx = f_n.argsort()
    f_n = f_n[idx]
    phi = vecs[:, idx]
    t1 = time()
    print(t1-t0)
    
    for i_m in range(phi.shape[1]):
        phi[:, i_m] = phi[:, i_m] / sa.norm(phi[:, i_m])
    
    return f_n, phi

def pvet(a):
    return a.reshape((a.size, 1))

def rotation_matrix(angle,axis):
    if (axis=='x'):
        A = np.array([[1, 0, 0],
                    [0, np.cos(angle), np.sin(angle)],
                    [0, -np.sin(angle), np.cos(angle)]], dtype=float)
    elif (axis=='y'):
        A = np.array([[np.cos(angle), 0, -np.sin(angle)],
                    [0, 1, 0],
                    [np.sin(angle), 0, np.cos(angle)]], dtype=float)
    elif (axis=='z'):
        A = np.array([[np.cos(angle), np.sin(angle), 0],
                    [-np.sin(angle), np.cos(angle), 0],
                    [0, 0, 1]], dtype=float)
    else:
        print('error in rotate_matrix function')
    
    return A


#%%

file_name = './comsol/shell_tower.mat'
sys = load_from_livelink(file_name)

# Reordered version of sys.quad
sys.elm = np.zeros(sys.quad.shape, dtype=int)
for i_e in range(sys.n_elms):
    
    r_e = sys.coord[sys.quad[i_e, :], :]
    x_i = r_e[:, 0]
    y_i = r_e[:, 1]
    z_i = r_e[:, 2]
    
    r_i = np.sqrt(x_i**2+y_i**2)
    theta_i = np.arctan2(y_i, x_i) * (180./np.pi)
    for i in range(theta_i.size):
        if theta_i[i] < 0:
            theta_i[i] = theta_i[i] + 360.
    
    theta_min = theta_i.min()
    theta_max = theta_i.max()
    theta_in = (2./(theta_max-theta_min)*(theta_i-theta_min)-1).round().astype(int)
    
    z_max = r_e[:, 2].max()
    z_min = r_e[:, 2].min()
    
    z = (2./(z_max-z_min)*(r_e[:, 2]-z_min)-1).round().astype(int)
    z1 = np.array([-1, -1, -1, 0, 0, 0, 1, 1, 1], dtype=int)
    z2 = np.array([-1, 0, 1, -1, 0, 1, -1, 0, 1], dtype=int)
    z3 = np.array([1, 1, 1, 0, 0, 0, -1, -1, -1], dtype=int)
    z4 = np.array([1, 0, -1, 1, 0, -1, 1, 0, -1], dtype=int)
    
    theta_3 = np.array([1, 0, -1, 1, 0, -1, 1, 0, -1])
    theta_4 = np.array([-1, -1, -1, 0, 0, 0, 1, 1, 1])
    
    if (z == z1).all():
        node_order = np.array([0, 2, 8, 6, 1, 5, 7, 3, 4])
    elif(z == z2).all():
        node_order = np.array([6, 0, 2, 8, 3, 1, 5, 7, 4])
    elif(z == z3).all():
        node_order = np.array([8, 6, 0, 2, 7, 3, 1, 5, 4])
    elif(z == z4).all():
        node_order = np.array([2, 8, 6, 0, 5, 7, 3, 1, 4])
    else:
        print("I'm sorry, Dave, I'm affraid I can't do that")
        
    sys.elm[i_e, :] = sys.quad[i_e, node_order]

del node_order

#%% Curvilinear coordinates

normal = np.zeros((sys.n_nodes, 4, 3), dtype=float)
n_count = np.zeros((sys.n_nodes, ), dtype=int)
h_xi = np.zeros((3, ), dtype=float)
h_eta = np.zeros((3, ), dtype=float)
h_zeta = np.zeros((3, ), dtype=float)
e_xi = np.zeros((sys.n_nodes, 4, 3), dtype=float)
e_eta = np.zeros((sys.n_nodes, 4, 3), dtype=float)
e_zeta = np.zeros((sys.n_nodes, 4, 3), dtype=float)

for i_e in range(sys.n_elms):
    
    # xi and eta order tratidional order
    xi_order = np.array([-1, 1, 1, -1, 0, 1, 0, -1, 0])
    eta_order = np.array([-1, -1, 1, 1, -1, 0, 1, 0, 0])
    
    # Nodes coordonates
    r_e = sys.coord[sys.elm[i_e, :], :]
    x_i = r_e[:, 0]
    y_i = r_e[:, 1]
    z_i = r_e[:, 2]
    
    # Nodes normals
    for i_node in range(9):
        
        node = sys.elm[i_e, i_node]
        
        xi = xi_order[i_node]
        eta = eta_order[i_node]
        
        # Shape functions and derivatives
        N, N_xi, N_eta = fem.quad9(xi, eta)

        # Curvilinear coordinates        
        h_xi[0] = N_xi.T @ x_i
        h_xi[1] = N_xi.T @ y_i
        h_xi[2] = N_xi.T @ z_i
        e_xi[node, n_count[node], :] = h_xi / sa.norm(h_xi)

        h_eta[0] = N_eta.T @ x_i
        h_eta[1] = N_eta.T @ y_i
        h_eta[2] = N_eta.T @ z_i
        e_eta[node, n_count[node], :] = h_eta / sa.norm(h_eta)

        h_zeta = np.cross(h_xi, h_eta)
        e_zeta[node, n_count[node], :] = h_zeta / sa.norm(h_zeta)
        n_count[node] += 1
        
sys.e_0 = np.zeros((sys.n_nodes, 3), dtype=float)
sys.e_1 = np.zeros((sys.n_nodes, 3), dtype=float)
sys.e_2 = np.zeros((sys.n_nodes, 3), dtype=float)
for i_n in range(sys.n_nodes):
    sys.e_2[i_n, 0] = e_zeta[i_n, :, 0].sum()/n_count[i_n]
    sys.e_2[i_n, 1] = e_zeta[i_n, :, 1].sum()/n_count[i_n]
    sys.e_2[i_n, 2] = e_zeta[i_n, :, 2].sum()/n_count[i_n]
    
    sys.e_0[i_n, 0] = e_xi[i_n, :, 0].sum()/n_count[i_n]
    sys.e_0[i_n, 1] = e_xi[i_n, :, 1].sum()/n_count[i_n]
    sys.e_0[i_n, 2] = e_xi[i_n, :, 2].sum()/n_count[i_n]
    
    sys.e_1[i_n, :] = np.cross(sys.e_2[i_n, :], sys.e_0[i_n, :])
    sys.e_1[i_n, :] = sys.e_1[i_n, :]/sa.norm(sys.e_1[i_n, :])
    
    sys.e_0[i_n, :] = np.cross(sys.e_1[i_n, :], sys.e_2[i_n, :])
    sys.e_0[i_n, :] = sys.e_0[i_n, :]/sa.norm(sys.e_0[i_n, :])

sys.L =  np.zeros((sys.n_nodes, 3, 3))
for i_n in range(sys.n_nodes):
    sys.L[i_n, 0, :] = sys.e_0[i_n, :]
    sys.L[i_n, 1, :] = sys.e_1[i_n, :]
    sys.L[i_n, 2, :] = sys.e_2[i_n, :]

#%% Plot geometry and unit vectors

# Divide the retangles into two triangles
t1 = np.zeros((sys.n_elms, 3), dtype=int)
t2 = np.zeros((sys.n_elms, 3), dtype=int)
for i_e in range(sys.n_elms):
    nodes = sys.elm[i_e, :]
    t1[i_e, :] = nodes[[0, 1, 2]]
    t2[i_e, :] = nodes[[2, 3, 0]]
sys.triangles = np.concatenate([t1, t2])
del t1, t2

sys.x = sys.coord[:, 0]
sys.y = sys.coord[:, 1]
sys.z = sys.coord[:, 2]

i_e = 9
x_i = sys.coord[sys.quad[i_e, :], 0]
y_i = sys.coord[sys.quad[i_e, :], 1]
z_i = sys.coord[sys.quad[i_e, :], 2]
if (True):
    fig = mlab.figure()
    mlab.triangular_mesh(sys.x, sys.y, sys.z, sys.triangles[0:1, :])
    mlab.points3d(x_i, y_i, z_i, scale_factor=0.1)
    i_n = 3
    mlab.points3d(x_i[i_n], y_i[i_n], z_i[i_n], scale_factor=0.1, color=(0.5, 0., 0.))
    
    mlab.quiver3d(sys.x, sys.y, sys.z, sys.e_0[:, 0], sys.e_0[:, 1], sys.e_0[:, 2], scale_factor=3)
    mlab.quiver3d(sys.x, sys.y, sys.z, sys.e_1[:, 0], sys.e_1[:, 1], sys.e_1[:, 2], scale_factor=3)

#%% Constraint n·a = 0

# Change "a" coordinate system

twf = sys.copy()
twf.q_idx = np.arange(sys.n_dofs)

twf.N0 = np.zeros((twf.n_nodes*6, twf.n_nodes*6), dtype=float)
for i_n in range(sys.n_nodes):
        
    Ne = np.zeros((6, 6), dtype=float)
    Ne[:3, :3] = sys.L[i_n, :, :].T
    
    Ne[3:, 3:] = np.eye(3, dtype=float)
    
    i_idx = sys.dof[i_n, :]
    j_idx = sys.dof[i_n, :]
    twf.N0[np.ix_(i_idx, j_idx)] = twf.N0[np.ix_(i_idx, j_idx)] + Ne
twf.N0 = ss.csc_matrix(twf.N0)

# Constraint a·n = 0

dofn = np.arange(twf.n_nodes*5).reshape((twf.n_nodes, 5))

twf.N1 = np.zeros((twf.n_nodes*6, twf.n_nodes*5), dtype=float)
for i_n in range(sys.n_nodes):
    
    # twf.n[i_n, :] = sys.S[i_n, :, :] @ sys.n[i_n, :]
    # n_0, n_1, n_2 = twf.e_2[i_n, :]
    n_0, n_1, n_2 = [0., 0., 1.]
    
    Ne = np.zeros((6, 5), dtype=float)
    Ne[0, 0] = 1.
    Ne[1, 1] = 1.
    Ne[3, 2] = 1.
    Ne[4, 3] = 1.
    Ne[5, 4] = 1.
    Ne[2, 0] = -(n_0/n_2) # 0 since n_0 = 0
    Ne[2, 1] = -(n_1/n_2) # 0 since n_1 = 0

    i_idx = sys.dof[i_n, :] # old coordinates
    j_idx = dofn[i_n, :]    # new coordinates
    twf.N1[np.ix_(i_idx, j_idx)] = twf.N1[np.ix_(i_idx, j_idx)] + Ne
twf.N1 = ss.csc_matrix(twf.N1)

twf.N = twf.N0 @ twf.N1    

twf.Mc = twf.N.T @ twf.M @ twf.N
twf.Kc = twf.N.T @ twf.K @ twf.N


#%% Constraint the bottom, rearranging the matrix order

bot = np.array([i_n for i_n in range(twf.n_nodes) if twf.coord[i_n, 2]<=0.])
top = np.array([i_n for i_n in range(twf.n_nodes) if twf.coord[i_n, 2]>=115.63])

n_dofs = twf.Mc.shape[0]

# DOFs at the bottom
qc_bool = np.zeros((n_dofs, ), dtype=bool)
qc_bool[dofn[bot, :]] = True

# DOFs at the top
qb_bool = np.zeros((n_dofs, ), dtype=bool)
qb_bool[dofn[top, :]] = True

# Internal DOFs (i.e. not the boundary)
qi_bool = ~qb_bool

qb_bool = qb_bool * ~qc_bool
qi_bool = qi_bool * ~qc_bool

q_idx = np.arange(n_dofs)
qr_idx = np.concatenate([q_idx[qb_bool], q_idx[qi_bool]])

twr = empty_object()

twr.Nr = boolean_matrix(q_idx, qr_idx)
twr.Mc = twr.Nr.T @ twf.Mc @ twr.Nr
twr.Kc = twr.Nr.T @ twf.Kc @ twr.Nr

twr.f_n, twr.phi = freq_h(twr.Mc, twr.Kc)

sys.f_n, _ = freq_h(twr.Nr.T @ sys.Mc @ twr.Nr, twr.Nr.T @ sys.Kc @ twr.Nr)

# toto = np.zeros((int(sys.f_n.size/2), 2))
# for i in range(0,int(sys.f_n.size/2)):
#     toto[i, 0] = sys.f_n[2*i+1]-sys.f_n[2*i]
#     toto[i, 1] = twr.f_n[2*i+1]-twr.f_n[2*i]

#%%

i_n = 110

Nd = sys.N[np.ix_(sys.dof[i_n, :], dofn[i_n, :])].todense()
Ne = twf.N[np.ix_(sys.dof[i_n, :], dofn[i_n, :])].todense()

n_0, n_1, n_2 = sys.e_2[i_n, :]
n = sys.e_2[i_n, :]

#%%
'''

#%% Display mode shapes

i_m = 9
u = twf.N @ twr.Nr @ twr.phi[:, i_m]*200

ux = u[sys.dof[:, 3]]
vy = u[sys.dof[:, 4]]
wz = u[sys.dof[:, 5]]

x = sys.coord[:, 0] + ux
y = sys.coord[:, 1] + vy
z = sys.coord[:, 2] + wz

if (plot_figures):
    fig = mlab.figure()
    mlab.triangular_mesh(x, y, z, sys.triangles)
    # mlab.points3d(x, y, z, scale_factor=1.)    
    # mlab.triangular_mesh(x, y, z, triangles)

'''

#%% Constraint the top

# Forget the original dof numbering, embrace the new one
n_b = q_idx[qb_bool].size
n_i = q_idx[qi_bool].size
n_c = 6

cnt = empty_object()
cnt.r = sys.coord[top, :]
cnt.e_0 = sys.e_0[top, :]
cnt.e_1 = sys.e_1[top, :]

cnt.r_c = sys.coord[top, :].mean(axis=0)
cnt.dof = np.arange(n_b).reshape((int(n_b/5), 5))

n_elms = top.shape[0]
n_dofs = cnt.dof.size
A = np.zeros((n_dofs, 6))
for i_e in range(n_elms):
    Ae = fem.rigid_connector(cnt.r[i_e], cnt.r_c, cnt.e_0[i_e], cnt.e_1[i_e])
    i_idx = cnt.dof[i_e, :]
    j_idx = np.arange(6)

    A[np.ix_(i_idx, j_idx)] = A[np.ix_(i_idx, j_idx)] + Ae

A = ss.coo_matrix(A)
I = ss.eye(n_i, format="coo")
O_bi = ss.coo_matrix((n_b, n_i))
O_ic = ss.coo_matrix((n_i, n_c))

twn = empty_object()

twn.N = ss.csr_matrix(ss.bmat([[A, O_bi],[O_ic, I]]))
twn.Mc = twn.N.T @ twr.Mc @ twn.N
twn.Kc = twn.N.T @ twr.Kc @ twn.N

twn.f_n, twn.phi = freq_h(twn.Mc, twn.Kc, n_m=40)

#%% Display mode shapes


i_m = 0
u = twf.N @ twr.Nr @ twn.N @ twn.phi[:, i_m]*400

ax = u[sys.dof[:, 0]]
ay = u[sys.dof[:, 1]]
az = u[sys.dof[:, 2]]
ux = u[sys.dof[:, 3]]
vy = u[sys.dof[:, 4]]
wz = u[sys.dof[:, 5]]

x = sys.coord[:, 0] + ux
y = sys.coord[:, 1] + vy
z = sys.coord[:, 2] + wz

if (plot_figures):
    fig = mlab.figure()
    mlab.triangular_mesh(x, y, z, sys.triangles)

#%% Model order reduction

tws = empty_object()

Mbb = twn.Mc[:n_c, :n_c]
Mii = twn.Mc[n_c:, n_c:]
Mib = twn.Mc[n_c:, :n_c]
Mbi = twn.Mc[:n_c, n_c:]

Kbb = twn.Kc[:n_c, :n_c]
Kii = twn.Kc[n_c:, n_c:]
Kib = twn.Kc[n_c:, :n_c]
Kbi = twn.Kc[:n_c, n_c:]

# Constraint mode shapes
Psi_c = ssl.spsolve(-Kii, Kib)

# Normal mode shapes
vals, vecs = freq_h(Mii, Kii, 40)
n_m = 24
Phi_n = ss.csr_matrix(vecs[:, :n_m])

n_b = Psi_c.shape[1]
I = ss.csc_matrix(np.eye(n_b))
O = ss.csc_matrix(np.zeros((n_b, n_m)))
tws.As = ss.bmat([[I, O], [Psi_c, Phi_n]], format='csc')

t_0 = time()
tws.Mc = tws.As.T @ twn.Mc @ tws.As
tws.Kc = tws.As.T @ twn.Kc @ tws.As
t_1 = time()
print(t_1 - t_0)

tws.f_n, tws.phi = freq_h(tws.Mc, tws.Kc)

#%% Tower top (Tp)

tmp = empty_object()
table = np.loadtxt('DTU_10MW_RWT_Towertop_st.dat', skiprows=3, max_rows=2)

tmp.n_elms = int(table.shape[0]/2)
tmp.n_nodes = tmp.n_elms+1
tmp.n_dpn = 6

tmp.L = table[1, 0]
tmp.x = np.linspace(0., tmp.L, tmp.n_nodes)
tmp.rho = table[0, 1] / table[0, 15]
tmp.A = table[0, 15]
tmp.E = table[0, 8]
tmp.G = table[0, 9]
tmp.I = table[0, 10]
tmp.J = table[0, 12]

tp = fem.Beam3D(tmp.x, tmp.A, tmp.I, tmp.I, tmp.J, tmp.rho, tmp.E, tmp.G)

del tmp

#%% Nacelle (nc)

nc = empty_object()
nc.m = 4.4604E+05 # kg
nc.r = np.array([0., -2.6870E+00, -3.0061E-01]) # m

nc.I = np.diag([4.1060E+06, 4.1060E+05, 4.1060E+06])
nc.J = nc.I + nc.m*((nc.r @ nc.r)*np.eye(3) - np.tensordot(nc.r, nc.r, axes=0))

nc.M = np.block([[nc.m*np.eye(3), np.zeros((3, 3))], [np.zeros((3, 3)), nc.J]])
nc.K = np.zeros(nc.M.shape)
nc.q = np.arange(6)

#%% Joining the tower-top and the concentrated inertia in one single object

tpn = empty_object()
tpn.q = tp.q_idx
q_n = tp.q_idx[-6:]
Ln = boolean_matrix(q_n, tpn.q)

tpn.M = ss.csr_matrix(tp.M + Ln.T @ nc.M @ Ln)
tpn.K = ss.csr_matrix(tp.K + Ln.T @ nc.K @ Ln)

#%% Joining the tower-top-nacelle with the full rearanged tower

jf = empty_object()
jf.q = np.arange(twn.Mc.shape[0] + tpn.M.shape[0] - 6)

q1 = np.arange(twn.Mc.shape[0])
q2 = np.concatenate((jf.q[:6], jf.q[-6:]))
jf.L1 = boolean_matrix(q1, jf.q)
jf.L2 = boolean_matrix(q2, jf.q)
jf.M = jf.L1.T @ twn.Mc @ jf.L1 + jf.L2.T @ tpn.M @ jf.L2
jf.K = jf.L1.T @ twn.Kc @ jf.L1 + jf.L2.T @ tpn.K @ jf.L2

jf.f_n, jf.phi = freq_h(jf.M, jf.K, 20)

#%% Joining the tower-top-nacelle with the reduced tower

jr = empty_object()
jr.q = np.arange(tws.Mc.shape[0] + tpn.M.shape[0] - 6)

q1 = np.arange(tws.Mc.shape[0])
q2 = np.concatenate((jr.q[:6], jr.q[-6:]))
jr.L1 = boolean_matrix(q1, jr.q)
jr.L2 = boolean_matrix(q2, jr.q)
jr.M = jr.L1.T @ tws.Mc @ jr.L1 + jr.L2.T @ tpn.M @ jr.L2
jr.K = jr.L1.T @ tws.Kc @ jr.L1 + jr.L2.T @ tpn.K @ jr.L2

jr.f_n, jr.phi = freq_h(jr.M, jr.K, 20)

#%% Exporting result matrix

num2str = np.vectorize(cf.num2str)

fn_comsol = np.loadtxt('./comsol/tower_nacelle.txt', skiprows=5, usecols=0)[:20]

error_f = np.abs((jf.f_n - fn_comsol)/fn_comsol) *100.
error_r = np.abs((jr.f_n - fn_comsol)/fn_comsol) *100.

table = cf.LatexTable()

table.filename = '../tables/ex_2.tex'
table.M = num2str(np.block([[fn_comsol], [jf.f_n], [error_f], [jr.f_n], [error_r]]).T, '%0.6e')
table.V = num2str(np.arange(jr.f_n.size) + 1, '%i')
table.H = np.array(['COMSOL [Hz]', 'Model [Hz]', 'Error [\%]', 'Reduced [Hz]', 'Error [\%]'])
table.corner = 'Mode'
if export_tables:
    table.print()

#%% Display mode shapes (full)

i_m = 0
u0 = twf.N @ twr.Nr @ twn.N @ jf.L1 @ jf.phi[:, i_m]*400

ax = u0[sys.dof[:, 0]]
ay = u0[sys.dof[:, 1]]
az = u0[sys.dof[:, 2]]
ux = u0[sys.dof[:, 3]]
vy = u0[sys.dof[:, 4]]
wz = u0[sys.dof[:, 5]]

x0 = sys.coord[:, 0] + ux
y0 = sys.coord[:, 1] + vy
z0 = sys.coord[:, 2] + wz

u1 = jf.L2 @ jf.phi[:, i_m]*400
ux = u1[tp.dofs[:, 0]]
vy = u1[tp.dofs[:, 1]]
wz = u1[tp.dofs[:, 2]]

x1 = ux
y1 = vy
z1 = np.array([115.63, 118.38]) + wz

if (plot_figures):
    fig = mlab.figure()
    mlab.triangular_mesh(x0, y0, z0, sys.triangles)
    mlab.points3d(x1, y1, z1, scale_factor=1.)
    mlab.plot3d(x1, y1, z1, tube_radius=.25)
    if (export_figures):
        mlab.savefig(filename='../figures/tower_mode_%i.eps'%(i_m))

#%% Display mode shapes (reduced)

scale_factor = 40

for i_m in range(20):

    u0 = twf.N @ twr.Nr @ twn.N @ tws.As @ jr.L1 @ jr.phi[:, i_m]*scale_factor
    
    ax = u0[sys.dof[:, 0]]
    ay = u0[sys.dof[:, 1]]
    az = u0[sys.dof[:, 2]]
    ux = u0[sys.dof[:, 3]]
    vy = u0[sys.dof[:, 4]]
    wz = u0[sys.dof[:, 5]]
    
    x0 = sys.coord[:, 0] + ux
    y0 = sys.coord[:, 1] + vy
    z0 = sys.coord[:, 2] + wz
    
    u1 = jr.L2 @ jr.phi[:, i_m]*scale_factor
    ux = u1[tp.dofs[:, 0]]
    vy = u1[tp.dofs[:, 1]]
    wz = u1[tp.dofs[:, 2]]
    
    x1 = ux
    y1 = vy
    z1 = np.array([115.63, 118.38]) + wz
    
    if (plot_figures):
        fig = mlab.figure()
        mlab.triangular_mesh(x0, y0, z0, sys.triangles, color=(0.1, 0.1, 0.75))
        mlab.points3d(x1, y1, z1, scale_factor=0.1)
        mlab.points3d(x0, y0, z0, scale_factor=0.1)
        mlab.plot3d(x1, y1, z1, tube_radius=.25)
        if (export_figures):
            mlab.savefig(filename='../figures/tower_mode_%i.eps'%(i_m))

