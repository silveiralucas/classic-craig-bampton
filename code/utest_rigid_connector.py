import numpy as np
import scipy.linalg as sa
import fem

#%% Functions

def rotation_matrix(angle,axis): # return A
    '''
    Generates a transformation matrix.
    
    Parameters
    ----------
    angle : sympy.Symbol or sympy.Function
            [rad] angle between the two reference frames along the 'axis'
    axis : str
           axis of roation. 'x', 'y' and 'z'.
    
    Returns
    -------
    A : sympy.Matrix [:, :]
        transformation matrix from reference frame 1 to reference frame 2.
    '''
    if (axis=='x'):
        A = np.array([[1., 0., 0.],
                    [0., np.cos(angle), np.sin(angle)],
                    [0., -np.sin(angle), np.cos(angle)]], dtype=float)
    elif (axis=='y'):
        A = np.array([[np.cos(angle), 0., -np.sin(angle)],
                    [0., 1., 0.],
                    [np.sin(angle), 0., np.cos(angle)]], dtype=float)
    elif (axis=='z'):
        A = np.array([[np.cos(angle), np.sin(angle), 0.],
                    [-np.sin(angle), np.cos(angle), 0.],
                    [0., 0., 1.]])
    else:
        print('error in rotate_matrix function')
    
    return A

#%% Prescribed test

# r_p0 = np.array([1., 0., 10.])
# r_c = np.array([0., 0., 10.])

# n = np.array([1., 0., 0.])
# t_0 = np.array([0., 1., 0.])
# t_1 = np.array([0., 0., 1.])

# Ae = fem.rigid_connector(r_p0, r_c, t_0, t_1)

# theta_c = np.array([0., -5., 0])*(np.pi/180.)
# u_c = np.array([0., 0., 0.])
# u_c = np.concatenate([u_c, theta_c])

# u_p = Ae@u_c

# r_p = np.array([0., 0., 1.]) + u_p[:3]

#%% Random test

# Random plane
p_0 = np.random.rand(3)
p_1 = np.random.rand(3)
p_2 = np.random.rand(3)
v_0 = (p_1 - p_0)/sa.norm(p_1 - p_0)
v_1 = (p_2 - p_0)/sa.norm(p_2 - p_0)

# Normal and tangent vectors
n = np.cross(v_0, v_1)/sa.norm(np.cross(v_0, v_1))
t_0 = v_0
t_1 = np.cross(n, t_0)

# Transformation matrix (exyz -> e012)
L = np.block([[t_0], [t_1], [n]])

# Position vectors
r_p0 = np.concatenate([p_0, n])
r_c = np.concatenate([np.random.rand(3), np.zeros(3)])


# Random rotation
phi = (np.random.rand(1)[0]*5) *(np.pi/180.)
beta = (np.random.rand(1)[0]*5) *(np.pi/180.)
gamma = (np.random.rand(1)[0]*5) *(np.pi/180.)
#
A_01 = rotation_matrix(gamma, 'z') @ rotation_matrix(beta, 'y') @ rotation_matrix(phi, 'x')

# Position vector after rotation
r_p = np.zeros((6, ))
r_p[:3] = r_c[:3] + A_01.T @ (r_p0[:3]-r_c[:3])

# Normal vector after rotation
a = (A_01.T - np.eye(3)) @ n
r_p[3:] = n + a

# Checking the routine
Ae = fem.rigid_connector(r_p0[:3], r_c[:3], t_0, t_1)
u_c = np.array([0., 0., 0., phi, beta, gamma])
q_p = Ae @ u_c

s_p = np.zeros((6, ))
s_p[:3] = q_p[-3:]

a012 = np.concatenate([q_p[0:2], [0]])
axyz = L.T @ a012
nxyz = n + axyz

