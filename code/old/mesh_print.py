import numpy as np
import scipy.io as sio
import scipy.linalg as sa
import scipy.sparse as ss
import scipy.sparse.linalg as ssl
import copy
from numba import jit
from mayavi import mlab
import custom_functions as cf

from time import time

from scipy.special import comb
import itertools

#%%

gen_plots = True

#%%

# Empty class
class empty_object(object):
    def copy(self):
        return copy.deepcopy(self)

# Loading Matlab data
def load_from_livelink(file_name): # return: nodes, sys
    toto = sio.loadmat(file_name)
    
    sys = empty_object()
    sys.coord = toto['nodes'][0][0][0]
    sys.node = toto['nodes'][0][0][1][0] - 1
    sys.dof = toto['nodes'][0][0][2]
    
    sys.n_nodes = sys.node.size
    sys.n_dofs = sys.dof.size
    
    sys.quad = toto['quad'][0][0][0]
    
    sys.M = (toto['sys'][0][0][0])
    sys.K = (toto['sys'][0][0][1])
    sys.D = (toto['sys'][0][0][2])
    sys.C = (toto['sys'][0][0][3])
    sys.L = (toto['sys'][0][0][4][0])
    sys.N = (toto['sys'][0][0][5])
    sys.Nf = (toto['sys'][0][0][6])
    sys.Mc = (toto['sys'][0][0][7])
    sys.Kc = (toto['sys'][0][0][8])
        
    return sys

@jit
def boolean_matrix(a, b):
    
    # Get matrix sparsity
    n = 0
    for i in range(a.size):
        for j in range(b.size):
            if (a[i]==b[j]):
                n += 1
    
    data = np.ones((n,), dtype=int)
    idx = np.zeros((n,), dtype=int)
    jdx = np.zeros((n,), dtype=int)
    n = 0
    for i in range(a.size):
        for j in range(b.size):
            if (a[i]==b[j]):
                idx[n] = i
                jdx[n] = j
                n += 1
    
    L = ss.csr_matrix((data, (idx, jdx)), shape=(a.size, b.size))
    
    return L

def pvet(a):
    return a.reshape((a.size, 1))

#%%

file_name = '../../../Craig-Bampton/comsol_example/shell_tower.mat'
sys = load_from_livelink(file_name)

sys.Mc = sys.Nf.T @ sys.M @ sys.N
sys.Kc = sys.Nf.T @ sys.K @ sys.N

#%%


x = sys.coord[sys.quad[0, :], :][:, 0]
y = sys.coord[sys.quad[0, :], :][:, 1]
z = sys.coord[sys.quad[0, :], :][:, 2]
# fig = mlab.figure()
# mlab.points3d(x, y, z, scale_factor=0.1)

xc = x.mean()
yc = y.mean()
zc = z.mean()

# mlab.points3d(xc, yc, zc, scale_factor=0.1)


#%%

# Finding the central node

nodes = sys.coord[sys.quad[0, :]]
r_c = np.array([xc, yc, zc])

idx = sa.norm(nodes - r_c, axis=1).argsort()
r_c = nodes[idx[0], :]
# nodes = nodes[idx, :][1:]
# vecs = nodes - r_c

