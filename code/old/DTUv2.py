import numpy as np
import scipy.linalg as sa
import matplotlib.pyplot as plt
import copy

#%% Functions

class empty_object(object):
    def copy(self):
        return copy.deepcopy(self)

def boolean_matrix(a, b): # return L
    L = np.zeros((a.size, b.size), dtype=int)
    for i in range(a.size):
        for j in range(b.size):
            if (a[i]==b[j]):
                L[i, j] = 1
    return L

def beam_2D_const(rho, E, l, A, I): # returns M_e, K_e
    
    K_e = np.zeros((6, 6), dtype=float)
    K_e[0, 0] = A*E/l
    K_e[0, 3] = -A*E/l
    K_e[1, 1] = 12*E*I/l**3
    K_e[1, 2] = 6*E*I/l**2
    K_e[1, 4] = -12*E*I/l**3
    K_e[1, 5] = 6*E*I/l**2
    K_e[2, 1] = 6*E*I/l**2
    K_e[2, 2] = 4*E*I/l
    K_e[2, 4] = -6*E*I/l**2
    K_e[2, 5] = 2*E*I/l
    K_e[3, 0] = -A*E/l
    K_e[3, 3] = A*E/l
    K_e[4, 1] = -12*E*I/l**3
    K_e[4, 2] = -6*E*I/l**2
    K_e[4, 4] = 12*E*I/l**3
    K_e[4, 5] = -6*E*I/l**2
    K_e[5, 1] = 6*E*I/l**2
    K_e[5, 2] = 2*E*I/l
    K_e[5, 4] = -6*E*I/l**2
    K_e[5, 5] = 4*E*I/l
    
    M_e = np.zeros((6, 6), dtype=float)
    M_e[0, 0] = (1/3)*A*l*rho
    M_e[0, 3] = (1/6)*A*l*rho
    M_e[1, 1] = (13/35)*A*l*rho
    M_e[1, 2] = (11/210)*A*l**2*rho
    M_e[1, 4] = (9/70)*A*l*rho
    M_e[1, 5] = -13/420*A*l**2*rho
    M_e[2, 1] = (11/210)*A*l**2*rho
    M_e[2, 2] = (1/105)*A*l**3*rho
    M_e[2, 4] = (13/420)*A*l**2*rho
    M_e[2, 5] = -1/140*A*l**3*rho
    M_e[3, 0] = (1/6)*A*l*rho
    M_e[3, 3] = (1/3)*A*l*rho
    M_e[4, 1] = (9/70)*A*l*rho
    M_e[4, 2] = (13/420)*A*l**2*rho
    M_e[4, 4] = (13/35)*A*l*rho
    M_e[4, 5] = -11/210*A*l**2*rho
    M_e[5, 1] = -13/420*A*l**2*rho
    M_e[5, 2] = -1/140*A*l**3*rho
    M_e[5, 4] = -11/210*A*l**2*rho
    M_e[5, 5] = (1/105)*A*l**3*rho
    
    return M_e, K_e

def beam_2d_circular(rho, E, x, d_out, d_int):
    
    l = np.diff(x)
    D_0, D_1 = d_out
    d_0, d_1 = d_int
    
    K_e = np.zeros((6, 6), dtype=float)
    K_e[0, 0] = (1/12)*np.pi*D_0**2*E/l + (1/12)*np.pi*D_0*D_1*E/l + (1/12)*np.pi*D_1**2*E/l - 1/12*np.pi*E*d_0**2/l - 1/12*np.pi*E*d_0*d_1/l - 1/12*np.pi*E*d_1**2/l
    K_e[0, 3] = -1/12*np.pi*D_0**2*E/l - 1/12*np.pi*D_0*D_1*E/l - 1/12*np.pi*D_1**2*E/l + (1/12)*np.pi*E*d_0**2/l + (1/12)*np.pi*E*d_0*d_1/l + (1/12)*np.pi*E*d_1**2/l
    K_e[1, 1] = (33/560)*np.pi*D_0**4*E/l**3 + (3/112)*np.pi*D_0**3*D_1*E/l**3 + (9/560)*np.pi*D_0**2*D_1**2*E/l**3 + (3/112)*np.pi*D_0*D_1**3*E/l**3 + (33/560)*np.pi*D_1**4*E/l**3 - 33/560*np.pi*E*d_0**4/l**3 - 3/112*np.pi*E*d_0**3*d_1/l**3 - 9/560*np.pi*E*d_0**2*d_1**2/l**3 - 3/112*np.pi*E*d_0*d_1**3/l**3 - 33/560*np.pi*E*d_1**4/l**3
    K_e[1, 2] = (47/1120)*np.pi*D_0**4*E/l**2 + (11/560)*np.pi*D_0**3*D_1*E/l**2 + (9/1120)*np.pi*D_0**2*D_1**2*E/l**2 + (1/140)*np.pi*D_0*D_1**3*E/l**2 + (19/1120)*np.pi*D_1**4*E/l**2 - 47/1120*np.pi*E*d_0**4/l**2 - 11/560*np.pi*E*d_0**3*d_1/l**2 - 9/1120*np.pi*E*d_0**2*d_1**2/l**2 - 1/140*np.pi*E*d_0*d_1**3/l**2 - 19/1120*np.pi*E*d_1**4/l**2
    K_e[1, 4] = -33/560*np.pi*D_0**4*E/l**3 - 3/112*np.pi*D_0**3*D_1*E/l**3 - 9/560*np.pi*D_0**2*D_1**2*E/l**3 - 3/112*np.pi*D_0*D_1**3*E/l**3 - 33/560*np.pi*D_1**4*E/l**3 + (33/560)*np.pi*E*d_0**4/l**3 + (3/112)*np.pi*E*d_0**3*d_1/l**3 + (9/560)*np.pi*E*d_0**2*d_1**2/l**3 + (3/112)*np.pi*E*d_0*d_1**3/l**3 + (33/560)*np.pi*E*d_1**4/l**3
    K_e[1, 5] = (19/1120)*np.pi*D_0**4*E/l**2 + (1/140)*np.pi*D_0**3*D_1*E/l**2 + (9/1120)*np.pi*D_0**2*D_1**2*E/l**2 + (11/560)*np.pi*D_0*D_1**3*E/l**2 + (47/1120)*np.pi*D_1**4*E/l**2 - 19/1120*np.pi*E*d_0**4/l**2 - 1/140*np.pi*E*d_0**3*d_1/l**2 - 9/1120*np.pi*E*d_0**2*d_1**2/l**2 - 11/560*np.pi*E*d_0*d_1**3/l**2 - 47/1120*np.pi*E*d_1**4/l**2
    K_e[2, 1] = (47/1120)*np.pi*D_0**4*E/l**2 + (11/560)*np.pi*D_0**3*D_1*E/l**2 + (9/1120)*np.pi*D_0**2*D_1**2*E/l**2 + (1/140)*np.pi*D_0*D_1**3*E/l**2 + (19/1120)*np.pi*D_1**4*E/l**2 - 47/1120*np.pi*E*d_0**4/l**2 - 11/560*np.pi*E*d_0**3*d_1/l**2 - 9/1120*np.pi*E*d_0**2*d_1**2/l**2 - 1/140*np.pi*E*d_0*d_1**3/l**2 - 19/1120*np.pi*E*d_1**4/l**2
    K_e[2, 2] = (17/560)*np.pi*D_0**4*E/l + (9/560)*np.pi*D_0**3*D_1*E/l + (1/140)*np.pi*D_0**2*D_1**2*E/l + (1/280)*np.pi*D_0*D_1**3*E/l + (3/560)*np.pi*D_1**4*E/l - 17/560*np.pi*E*d_0**4/l - 9/560*np.pi*E*d_0**3*d_1/l - 1/140*np.pi*E*d_0**2*d_1**2/l - 1/280*np.pi*E*d_0*d_1**3/l - 3/560*np.pi*E*d_1**4/l
    K_e[2, 4] = -47/1120*np.pi*D_0**4*E/l**2 - 11/560*np.pi*D_0**3*D_1*E/l**2 - 9/1120*np.pi*D_0**2*D_1**2*E/l**2 - 1/140*np.pi*D_0*D_1**3*E/l**2 - 19/1120*np.pi*D_1**4*E/l**2 + (47/1120)*np.pi*E*d_0**4/l**2 + (11/560)*np.pi*E*d_0**3*d_1/l**2 + (9/1120)*np.pi*E*d_0**2*d_1**2/l**2 + (1/140)*np.pi*E*d_0*d_1**3/l**2 + (19/1120)*np.pi*E*d_1**4/l**2
    K_e[2, 5] = (13/1120)*np.pi*D_0**4*E/l + (1/280)*np.pi*D_0**3*D_1*E/l + (1/1120)*np.pi*D_0**2*D_1**2*E/l + (1/280)*np.pi*D_0*D_1**3*E/l + (13/1120)*np.pi*D_1**4*E/l - 13/1120*np.pi*E*d_0**4/l - 1/280*np.pi*E*d_0**3*d_1/l - 1/1120*np.pi*E*d_0**2*d_1**2/l - 1/280*np.pi*E*d_0*d_1**3/l - 13/1120*np.pi*E*d_1**4/l
    K_e[3, 0] = -1/12*np.pi*D_0**2*E/l - 1/12*np.pi*D_0*D_1*E/l - 1/12*np.pi*D_1**2*E/l + (1/12)*np.pi*E*d_0**2/l + (1/12)*np.pi*E*d_0*d_1/l + (1/12)*np.pi*E*d_1**2/l
    K_e[3, 3] = (1/12)*np.pi*D_0**2*E/l + (1/12)*np.pi*D_0*D_1*E/l + (1/12)*np.pi*D_1**2*E/l - 1/12*np.pi*E*d_0**2/l - 1/12*np.pi*E*d_0*d_1/l - 1/12*np.pi*E*d_1**2/l
    K_e[4, 1] = -33/560*np.pi*D_0**4*E/l**3 - 3/112*np.pi*D_0**3*D_1*E/l**3 - 9/560*np.pi*D_0**2*D_1**2*E/l**3 - 3/112*np.pi*D_0*D_1**3*E/l**3 - 33/560*np.pi*D_1**4*E/l**3 + (33/560)*np.pi*E*d_0**4/l**3 + (3/112)*np.pi*E*d_0**3*d_1/l**3 + (9/560)*np.pi*E*d_0**2*d_1**2/l**3 + (3/112)*np.pi*E*d_0*d_1**3/l**3 + (33/560)*np.pi*E*d_1**4/l**3
    K_e[4, 2] = -47/1120*np.pi*D_0**4*E/l**2 - 11/560*np.pi*D_0**3*D_1*E/l**2 - 9/1120*np.pi*D_0**2*D_1**2*E/l**2 - 1/140*np.pi*D_0*D_1**3*E/l**2 - 19/1120*np.pi*D_1**4*E/l**2 + (47/1120)*np.pi*E*d_0**4/l**2 + (11/560)*np.pi*E*d_0**3*d_1/l**2 + (9/1120)*np.pi*E*d_0**2*d_1**2/l**2 + (1/140)*np.pi*E*d_0*d_1**3/l**2 + (19/1120)*np.pi*E*d_1**4/l**2
    K_e[4, 4] = (33/560)*np.pi*D_0**4*E/l**3 + (3/112)*np.pi*D_0**3*D_1*E/l**3 + (9/560)*np.pi*D_0**2*D_1**2*E/l**3 + (3/112)*np.pi*D_0*D_1**3*E/l**3 + (33/560)*np.pi*D_1**4*E/l**3 - 33/560*np.pi*E*d_0**4/l**3 - 3/112*np.pi*E*d_0**3*d_1/l**3 - 9/560*np.pi*E*d_0**2*d_1**2/l**3 - 3/112*np.pi*E*d_0*d_1**3/l**3 - 33/560*np.pi*E*d_1**4/l**3
    K_e[4, 5] = -19/1120*np.pi*D_0**4*E/l**2 - 1/140*np.pi*D_0**3*D_1*E/l**2 - 9/1120*np.pi*D_0**2*D_1**2*E/l**2 - 11/560*np.pi*D_0*D_1**3*E/l**2 - 47/1120*np.pi*D_1**4*E/l**2 + (19/1120)*np.pi*E*d_0**4/l**2 + (1/140)*np.pi*E*d_0**3*d_1/l**2 + (9/1120)*np.pi*E*d_0**2*d_1**2/l**2 + (11/560)*np.pi*E*d_0*d_1**3/l**2 + (47/1120)*np.pi*E*d_1**4/l**2
    K_e[5, 1] = (19/1120)*np.pi*D_0**4*E/l**2 + (1/140)*np.pi*D_0**3*D_1*E/l**2 + (9/1120)*np.pi*D_0**2*D_1**2*E/l**2 + (11/560)*np.pi*D_0*D_1**3*E/l**2 + (47/1120)*np.pi*D_1**4*E/l**2 - 19/1120*np.pi*E*d_0**4/l**2 - 1/140*np.pi*E*d_0**3*d_1/l**2 - 9/1120*np.pi*E*d_0**2*d_1**2/l**2 - 11/560*np.pi*E*d_0*d_1**3/l**2 - 47/1120*np.pi*E*d_1**4/l**2
    K_e[5, 2] = (13/1120)*np.pi*D_0**4*E/l + (1/280)*np.pi*D_0**3*D_1*E/l + (1/1120)*np.pi*D_0**2*D_1**2*E/l + (1/280)*np.pi*D_0*D_1**3*E/l + (13/1120)*np.pi*D_1**4*E/l - 13/1120*np.pi*E*d_0**4/l - 1/280*np.pi*E*d_0**3*d_1/l - 1/1120*np.pi*E*d_0**2*d_1**2/l - 1/280*np.pi*E*d_0*d_1**3/l - 13/1120*np.pi*E*d_1**4/l
    K_e[5, 4] = -19/1120*np.pi*D_0**4*E/l**2 - 1/140*np.pi*D_0**3*D_1*E/l**2 - 9/1120*np.pi*D_0**2*D_1**2*E/l**2 - 11/560*np.pi*D_0*D_1**3*E/l**2 - 47/1120*np.pi*D_1**4*E/l**2 + (19/1120)*np.pi*E*d_0**4/l**2 + (1/140)*np.pi*E*d_0**3*d_1/l**2 + (9/1120)*np.pi*E*d_0**2*d_1**2/l**2 + (11/560)*np.pi*E*d_0*d_1**3/l**2 + (47/1120)*np.pi*E*d_1**4/l**2
    K_e[5, 5] = (3/560)*np.pi*D_0**4*E/l + (1/280)*np.pi*D_0**3*D_1*E/l + (1/140)*np.pi*D_0**2*D_1**2*E/l + (9/560)*np.pi*D_0*D_1**3*E/l + (17/560)*np.pi*D_1**4*E/l - 3/560*np.pi*E*d_0**4/l - 1/280*np.pi*E*d_0**3*d_1/l - 1/140*np.pi*E*d_0**2*d_1**2/l - 9/560*np.pi*E*d_0*d_1**3/l - 17/560*np.pi*E*d_1**4/l
    
    M_e = np.zeros((6, 6), dtype=float)
    M_e[0, 0] = (1/20)*np.pi*D_0**2*l*rho + (1/40)*np.pi*D_0*D_1*l*rho + (1/120)*np.pi*D_1**2*l*rho - 1/20*np.pi*d_0**2*l*rho - 1/40*np.pi*d_0*d_1*l*rho - 1/120*np.pi*d_1**2*l*rho
    M_e[0, 3] = (1/80)*np.pi*D_0**2*l*rho + (1/60)*np.pi*D_0*D_1*l*rho + (1/80)*np.pi*D_1**2*l*rho - 1/80*np.pi*d_0**2*l*rho - 1/60*np.pi*d_0*d_1*l*rho - 1/80*np.pi*d_1**2*l*rho
    M_e[1, 1] = (29/504)*np.pi*D_0**2*l*rho + (1/36)*np.pi*D_0*D_1*l*rho + (19/2520)*np.pi*D_1**2*l*rho - 29/504*np.pi*d_0**2*l*rho - 1/36*np.pi*d_0*d_1*l*rho - 19/2520*np.pi*d_1**2*l*rho
    M_e[1, 2] = (13/2016)*np.pi*D_0**2*l**2*rho + (5/1008)*np.pi*D_0*D_1*l**2*rho + (17/10080)*np.pi*D_1**2*l**2*rho - 13/2016*np.pi*d_0**2*l**2*rho - 5/1008*np.pi*d_0*d_1*l**2*rho - 17/10080*np.pi*d_1**2*l**2*rho
    M_e[1, 4] = (23/2520)*np.pi*D_0**2*l*rho + (1/72)*np.pi*D_0*D_1*l*rho + (23/2520)*np.pi*D_1**2*l*rho - 23/2520*np.pi*d_0**2*l*rho - 1/72*np.pi*d_0*d_1*l*rho - 23/2520*np.pi*d_1**2*l*rho
    M_e[1, 5] = -5/2016*np.pi*D_0**2*l**2*rho - 17/5040*np.pi*D_0*D_1*l**2*rho - 19/10080*np.pi*D_1**2*l**2*rho + (5/2016)*np.pi*d_0**2*l**2*rho + (17/5040)*np.pi*d_0*d_1*l**2*rho + (19/10080)*np.pi*d_1**2*l**2*rho
    M_e[2, 1] = (13/2016)*np.pi*D_0**2*l**2*rho + (5/1008)*np.pi*D_0*D_1*l**2*rho + (17/10080)*np.pi*D_1**2*l**2*rho - 13/2016*np.pi*d_0**2*l**2*rho - 5/1008*np.pi*d_0*d_1*l**2*rho - 17/10080*np.pi*d_1**2*l**2*rho
    M_e[2, 2] = (1/1008)*np.pi*D_0**2*l**3*rho + (1/1008)*np.pi*D_0*D_1*l**3*rho + (1/2520)*np.pi*D_1**2*l**3*rho - 1/1008*np.pi*d_0**2*l**3*rho - 1/1008*np.pi*d_0*d_1*l**3*rho - 1/2520*np.pi*d_1**2*l**3*rho
    M_e[2, 4] = (19/10080)*np.pi*D_0**2*l**2*rho + (17/5040)*np.pi*D_0*D_1*l**2*rho + (5/2016)*np.pi*D_1**2*l**2*rho - 19/10080*np.pi*d_0**2*l**2*rho - 17/5040*np.pi*d_0*d_1*l**2*rho - 5/2016*np.pi*d_1**2*l**2*rho
    M_e[2, 5] = -1/2016*np.pi*D_0**2*l**3*rho - 1/1260*np.pi*D_0*D_1*l**3*rho - 1/2016*np.pi*D_1**2*l**3*rho + (1/2016)*np.pi*d_0**2*l**3*rho + (1/1260)*np.pi*d_0*d_1*l**3*rho + (1/2016)*np.pi*d_1**2*l**3*rho
    M_e[3, 0] = (1/80)*np.pi*D_0**2*l*rho + (1/60)*np.pi*D_0*D_1*l*rho + (1/80)*np.pi*D_1**2*l*rho - 1/80*np.pi*d_0**2*l*rho - 1/60*np.pi*d_0*d_1*l*rho - 1/80*np.pi*d_1**2*l*rho
    M_e[3, 3] = (1/120)*np.pi*D_0**2*l*rho + (1/40)*np.pi*D_0*D_1*l*rho + (1/20)*np.pi*D_1**2*l*rho - 1/120*np.pi*d_0**2*l*rho - 1/40*np.pi*d_0*d_1*l*rho - 1/20*np.pi*d_1**2*l*rho
    M_e[4, 1] = (23/2520)*np.pi*D_0**2*l*rho + (1/72)*np.pi*D_0*D_1*l*rho + (23/2520)*np.pi*D_1**2*l*rho - 23/2520*np.pi*d_0**2*l*rho - 1/72*np.pi*d_0*d_1*l*rho - 23/2520*np.pi*d_1**2*l*rho
    M_e[4, 2] = (19/10080)*np.pi*D_0**2*l**2*rho + (17/5040)*np.pi*D_0*D_1*l**2*rho + (5/2016)*np.pi*D_1**2*l**2*rho - 19/10080*np.pi*d_0**2*l**2*rho - 17/5040*np.pi*d_0*d_1*l**2*rho - 5/2016*np.pi*d_1**2*l**2*rho
    M_e[4, 4] = (19/2520)*np.pi*D_0**2*l*rho + (1/36)*np.pi*D_0*D_1*l*rho + (29/504)*np.pi*D_1**2*l*rho - 19/2520*np.pi*d_0**2*l*rho - 1/36*np.pi*d_0*d_1*l*rho - 29/504*np.pi*d_1**2*l*rho
    M_e[4, 5] = -17/10080*np.pi*D_0**2*l**2*rho - 5/1008*np.pi*D_0*D_1*l**2*rho - 13/2016*np.pi*D_1**2*l**2*rho + (17/10080)*np.pi*d_0**2*l**2*rho + (5/1008)*np.pi*d_0*d_1*l**2*rho + (13/2016)*np.pi*d_1**2*l**2*rho
    M_e[5, 1] = -5/2016*np.pi*D_0**2*l**2*rho - 17/5040*np.pi*D_0*D_1*l**2*rho - 19/10080*np.pi*D_1**2*l**2*rho + (5/2016)*np.pi*d_0**2*l**2*rho + (17/5040)*np.pi*d_0*d_1*l**2*rho + (19/10080)*np.pi*d_1**2*l**2*rho
    M_e[5, 2] = -1/2016*np.pi*D_0**2*l**3*rho - 1/1260*np.pi*D_0*D_1*l**3*rho - 1/2016*np.pi*D_1**2*l**3*rho + (1/2016)*np.pi*d_0**2*l**3*rho + (1/1260)*np.pi*d_0*d_1*l**3*rho + (1/2016)*np.pi*d_1**2*l**3*rho
    M_e[5, 4] = -17/10080*np.pi*D_0**2*l**2*rho - 5/1008*np.pi*D_0*D_1*l**2*rho - 13/2016*np.pi*D_1**2*l**2*rho + (17/10080)*np.pi*d_0**2*l**2*rho + (5/1008)*np.pi*d_0*d_1*l**2*rho + (13/2016)*np.pi*d_1**2*l**2*rho
    M_e[5, 5] = (1/2520)*np.pi*D_0**2*l**3*rho + (1/1008)*np.pi*D_0*D_1*l**3*rho + (1/1008)*np.pi*D_1**2*l**3*rho - 1/2520*np.pi*d_0**2*l**3*rho - 1/1008*np.pi*d_0*d_1*l**3*rho - 1/1008*np.pi*d_1**2*l**3*rho
    
    return M_e, K_e

def concentrated_inertia_2D(m, I, r=0.):

    # Offset, parallel axis theorem    
    I += m*(r**2)
    
    M_e = np.diag([m, m, I])
    K_e = np.zeros(M_e.shape)
    
    return M_e, K_e

def freq_h(M, K):
    
    # Eigen vector problem
    vals, vecs = sa.eigh(M, K)
    
    # Natural frequencies
    f_n = 1/(2*np.pi)*np.sqrt(1./vals)
    
    # Sorting
    idx = np.argsort(f_n)
    f_n = f_n[idx]
    vecs = vecs[:, idx]
    
    # Normalising the eigenvectors
    for i in range(vecs.shape[1]):
        vecs[:, i] = vecs[:, i] / sa.norm(vecs[:, i])
    
    return f_n, vecs

#%% Classes

class Circular2DBeam(object):
    
    def __init__(self, rho, E, x, d_out, d_int):
        self.rho = rho
        self.E = E
        self.x = x
        self.d_out = d_out
        self.d_int = d_int
        
        self.n_elms = x.shape[0]
        self.n_nodes = self.n_elms + 1
        self.n_dpn = 3
        self.n_dofs = self.n_nodes * self.n_dpn
        
        self.nodes = np.array([[i_e, i_e+1] for i_e in range(self.n_elms)])
        self.dofs = np.arange(self.n_dofs).reshape((self.n_nodes, self.n_dpn))
        self.cts = np.zeros(self.dofs.shape, dtype=bool)
        
        self.q_idx = np.arange(self.n_dofs)
        self.M = np.zeros((self.n_dofs, self.n_dofs))
        self.K = np.zeros((self.n_dofs, self.n_dofs))
        
        for i_e in range(self.n_elms):
            n0, n1 = [*self.nodes[i_e, :]]
            qe_idx = np.concatenate((self.dofs[n0, :], self.dofs[n1, :]))
            Le = boolean_matrix(qe_idx, self.q_idx)
            
            Me, Ke = beam_2d_circular(self.rho, self.E, self.x[i_e, :], self.d_out[i_e, :], self.d_int[i_e, :])
            self.M += Le.T @ Me @ Le
            self.K += Le.T @ Ke @ Le

    def fixed_constraint(self, cts=None):
        
        if (cts is not None):        
            self.cts = cts

        q_bool = ~self.cts.flatten()
        
        self.M = self.M[np.ix_(q_bool, q_bool)]
        self.K = self.K[np.ix_(q_bool, q_bool)]
        
        k = 0
        for i in range(self.n_nodes):
            for j in range(self.n_dpn):
                if (self.cts[i, j]):
                    self.dofs[i, j] = -1
                else:
                    self.dofs[i, j] = k
                    k += 1
        self.n_dofs = k
        self.q_idx = np.arange(self.n_dofs)
                
class Beam2D(object):
    
    def __init__(self, rho, E, l, A, I, n_elms=None):
        
        self.l = l/n_elms*np.ones((n_elms,)) if isinstance(l, float) else l
        self.rho = rho*np.ones((n_elms,)) if isinstance(rho, float) else rho
        self.E = E*np.ones((n_elms,)) if isinstance(E, float) else E
        self.A = A*np.ones((n_elms,)) if isinstance(A, float) else A
        self.I = I*np.ones((n_elms,)) if isinstance(I, float) else I
        
        self.n_elms = self.l.shape[0]
        self.n_nodes = self.n_elms + 1
        self.n_dpn = 3
        self.n_dofs = self.n_nodes * self.n_dpn
        
        self.nodes = np.array([[i_e, i_e+1] for i_e in range(self.n_elms)])
        self.dofs = np.arange(self.n_dofs).reshape((self.n_nodes, self.n_dpn))
        self.cts = np.zeros(self.dofs.shape, dtype=bool)
        
        self.q_idx = np.arange(self.n_dofs)
        self.M = np.zeros((self.n_dofs, self.n_dofs))
        self.K = np.zeros((self.n_dofs, self.n_dofs))
        
        for i_e in range(self.n_elms):
            n0, n1 = [*self.nodes[i_e, :]]
            qe_idx = np.concatenate((self.dofs[n0, :], self.dofs[n1, :]))
            Le = boolean_matrix(qe_idx, self.q_idx)
            
            Me, Ke = beam_2D_const(self.rho[i_e], self.E[i_e], self.l[i_e], self.A[i_e], self.I[i_e])
            self.M += Le.T @ Me @ Le
            self.K += Le.T @ Ke @ Le

    def fixed_constraint(self, cts=None):
        
        if (cts is not None):        
            self.cts = cts

        q_bool = ~self.cts.flatten()
        
        self.M = self.M[np.ix_(q_bool, q_bool)]
        self.K = self.K[np.ix_(q_bool, q_bool)]
        
        k = 0
        for i in range(self.n_nodes):
            for j in range(self.n_dpn):
                if (self.cts[i, j]):
                    self.dofs[i, j] = -1
                else:
                    self.dofs[i, j] = k
                    k += 1
        self.n_dofs = k
        self.q_idx = np.arange(self.n_dofs)

class ConcentratedInertia(object):
    
    def __init__(self, m, I, r=0.):
        I += m*(r**2)
    
        self.M = np.diag([m, m, I])
        self.K = np.zeros(self.M.shape)


#%% Tower (tw)

tmp = empty_object()

tmp.rho = 8.5 * 10.**3 # [kg/m**3]
tmp.E = 210. * 10.**9 # [Pa]
tmp.nu = 0.3 # [-]

tmp.table = np.loadtxt('tower.txt')
tmp.n_elms = int(tmp.table.shape[0]/2)
tmp.x = tmp.table[:, 0].reshape((tmp.n_elms, 2))
tmp.d_out = tmp.table[:, 1].reshape((tmp.n_elms, 2))
tmp.d_int = (tmp.table[:, 1] - 2.*tmp.table[:, 2]).reshape((tmp.n_elms, 2))

# (self, rho, E, x, d_out, d_int):
tw =  Circular2DBeam(tmp.rho, tmp.E, tmp.x, tmp.d_out, tmp.d_int)
del tmp

tw.cts[0, :] = True
tw.fixed_constraint()

f_n, _ = freq_h(tw.M, tw.K)

#%% Tower top (Tp)

tmp = empty_object()
tmp.table = np.loadtxt('DTU_10MW_RWT_Towertop_st.dat', skiprows=3, max_rows=2)

tmp.l = tmp.table[1, 0]
tmp.rho = tmp.table[0, 1] / tmp.table[0, 15]
tmp.A = tmp.table[0, 15]
tmp.E = tmp.table[0, 8]
tmp.I = tmp.table[0, 10]

tp = Beam2D(tmp.rho, tmp.E, tmp.l, tmp.A, tmp.I, n_elms=1)
del tmp

#%% Nacelle (nc)

tmp = empty_object()
tmp.m = 4.4604E+05
tmp.I = 4.1060E+06
tmp.offset = np.array([0.0, 2.6870E+00, 3.0061E-01])
tmp.r = sa.norm(tmp.offset)

nc = ConcentratedInertia(tmp.m, tmp.I, tmp.r)
del tmp

#%% Composite object

a = empty_object()

a.n_elms = tw.n_elms + tp.n_elms
a.n_nodes = a.n_elms - 1
a.nodes = np.block([[tw.nodes], [tp.nodes+tw.nodes[-1, -1]]])

a.q1_idx = tw.q_idx
a.M1 = tw.M
a.K1 = tw.K

a.M2 = tp.M
a.K2 = tp.K
a.q2_idx = tp.q_idx + a.q1_idx[-3]

a.M3 = nc.M
a.K3 = nc.K
a.q3_idx = a.q2_idx[-3:]

a.q_idx = np.array(list(set(a.q1_idx) | set(a.q2_idx) | set(a.q3_idx)))
a.L1 = boolean_matrix(a.q1_idx, a.q_idx)
a.L2 = boolean_matrix(a.q2_idx, a.q_idx)
a.L3 = boolean_matrix(a.q3_idx, a.q_idx)

a.M = a.L1.T @ a.M1 @ a.L1 + a.L2.T @ a.M2 @ a.L2 + a.L3.T @ a.M3 @ a.L3
a.K = a.L1.T @ a.K1 @ a.L1 + a.L2.T @ a.K2 @ a.L2 + a.L3.T @ a.K3 @ a.L3

f_n, phi = freq_h(a.M, a.K)

# x0 = np.zeros(tw.n_nodes+tp.n_nodes-1)
x0 = np.concatenate((tw.x[:, 0], [tw.x[-1, 1]]))

tp.x = np.zeros((tp.n_elms, 2))

# y0 = np.zeros(tw.n_nodes+tp.n_nodes-1)

# mode = 2
# u = phi[:, mode]

# x = np.zeros(x0.shape)
# y = np.zeros(y0.shape)
# x[1:] = x0[1:] + u[0::3]
# y[1:] = y0[1:] + u[1::3]

# fig = plt.figure()
# plt.plot(y, x, 'bo')

