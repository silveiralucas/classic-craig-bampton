import numpy as np
import scipy.linalg as sa
import scipy.io as sio
import copy
import fem

#%%

# Empty class
class empty_object(object):
    def copy(self):
        return copy.deepcopy(self)

# Loading Matlab data
def load_from_livelink(file_name): # return: nodes, sys
    toto = sio.loadmat(file_name)
    
    sys = empty_object()
    sys.coord = toto['nodes'][0][0][0]
    sys.node = toto['nodes'][0][0][1][0] - 1
    sys.dof = toto['nodes'][0][0][2]
    
    sys.n_nodes = sys.node.size
    sys.n_dofs = sys.dof.size
    
    sys.quad = toto['quad'][0][0][0]
    
    sys.n_elms = sys.quad.shape[0]
    
    sys.M = (toto['sys'][0][0][0])
    sys.K = (toto['sys'][0][0][1])
    sys.D = (toto['sys'][0][0][2])
    sys.C = (toto['sys'][0][0][3])
    sys.L = (toto['sys'][0][0][4][0])
    sys.N = (toto['sys'][0][0][5])
    sys.Nf = (toto['sys'][0][0][6])
    sys.Mc = (toto['sys'][0][0][7])
    sys.Kc = (toto['sys'][0][0][8])
    
    return sys

#%%

file_name = './comsol/shell_tower.mat'
sys = load_from_livelink(file_name)

#%%

# Reordered version of sys.quad
sys.elm = np.zeros(sys.quad.shape, dtype = int)

for i_e in range(sys.n_elms):
    
    r_e = sys.coord[sys.quad[i_e, :], :]
    x_i = r_e[:, 0]
    y_i = r_e[:, 1]
    z_i = r_e[:, 2]
    
    z_max = r_e[:, 2].max()
    z_min = r_e[:, 2].min()
    
    z = (2./(z_max-z_min)*(r_e[:, 2]-z_min)-1).round().astype(int)
    z1 = np.array([-1, -1, -1, 0, 0, 0, 1, 1, 1], dtype=int)
    z2 = np.array([-1, 0, 1, -1, 0, 1, -1, 0, 1], dtype=int)
    z3 = np.array([1, 1, 1, 0, 0, 0, -1, -1, -1], dtype=int)
    z4 = np.array([1, 0, -1, 1, 0, -1, 1, 0, -1], dtype=int)
    
    theta_3 = np.array([1, 0, -1, 1, 0, -1, 1, 0, -1])
    theta_4 = np.array([-1, -1, -1, 0, 0, 0, 1, 1, 1])
    
    if (z == z1).all():
        node_order = np.array([0, 2, 8, 6, 1, 5, 7, 3, 4])
    elif(z == z2).all():
        node_order = np.array([6, 0, 2, 8, 3, 1, 5, 7, 4])
    elif(z == z3).all():
        node_order = np.array([8, 6, 0, 2, 7, 3, 1, 5, 4])
    elif(z == z4).all():
        node_order = np.array([2, 8, 6, 0, 5, 7, 3, 1, 4])
    else:
        print("I'm sorry, Dave, I'm affraid I can't do that")
        
    sys.elm[i_e, :] = sys.quad[i_e, node_order]

del node_order

#%%

toto = np.zeros((sys.n_nodes, ), dtype=int)
for i_e in range(sys.n_elms):
    for i_n in range(sys.elm.shape[1]):
        node = sys.elm[i_e, i_n]
        toto[node] += 1

normal = np.zeros((sys.n_nodes, 4, 3), dtype=float)
n_count = np.zeros((sys.n_nodes, ), dtype=int)
h_xi = np.zeros((3, ))
h_eta = np.zeros((3, ))
h_zeta = np.zeros((3, ))
e_xi = np.zeros((sys.n_nodes, 3))
e_eta = np.zeros((sys.n_nodes, 3))
e_zeta = np.zeros((sys.n_nodes, 3))

for i_e in range(sys.n_elms):
    
    # xi and eta order tratidional order
    xi_order = np.array([-1, 1, 1, -1, 0, 1, 0, -1, 0])
    eta_order = np.array([-1, -1, 1, 1, -1, 0, 1, 0, 0])
    
    # Nodes coordonates
    r_e = sys.coord[sys.elm[i_e, :], :]
    x_i = r_e[:, 0]
    y_i = r_e[:, 1]
    z_i = r_e[:, 2]
    
    # Nodes normals
    for i_node in range(9):
        
        node = sys.elm[i_e, i_node]
        
        xi = xi_order[i_node]
        eta = eta_order[i_node]
        
        # Shape functions and derivatives
        N, N_xi, N_eta = fem.quad9(xi, eta)

        # Curvilinear coordinates        
        h_xi[0] = N_xi.T @ x_i
        h_xi[1] = N_xi.T @ y_i
        h_xi[2] = N_xi.T @ z_i
        e_xi[node] = h_xi / sa.norm(h_xi)

        h_eta[0] = N_eta.T @ x_i
        h_eta[1] = N_eta.T @ y_i
        h_eta[2] = N_eta.T @ z_i
        e_eta[node] = h_eta / sa.norm(h_eta)

        h_zeta = np.cross(h_xi, h_eta)
        e_zeta[node] = h_zeta / sa.norm(h_zeta)
        
        normal[node, n_count[node], :] = h_zeta / sa.norm(h_zeta)
        n_count[node] += 1

e_2 = np.zeros((sys.n_nodes, 3), dtype=float)
for i_n in range(sys.n_nodes):
    e_2[i_n, 0] = normal[i_n, :, 0].sum()/n_count[i_n]
    e_2[i_n, 1] = normal[i_n, :, 1].sum()/n_count[i_n]
    e_2[i_n, 2] = normal[i_n, :, 2].sum()/n_count[i_n]

for i_n in range(sys.n_nodes):
    if (e_2[i_n, 2]==0.):
        print(i_n)
