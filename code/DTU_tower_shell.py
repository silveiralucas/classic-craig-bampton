import numpy as np
import scipy.io as sio
import scipy.linalg as sa
import scipy.sparse as ss
import scipy.sparse.linalg as ssl
import copy
from numba import jit
from mayavi import mlab
import custom_functions as cf
import sympy as sym

from time import time
import matplotlib.pyplot as plt

import fem

#%%

gen_plots = True

#%%

# Empty class
class empty_object(object):
    def copy(self):
        return copy.deepcopy(self)

# Loading Matlab data
def load_from_livelink(file_name): # return: nodes, sys
    toto = sio.loadmat(file_name)
    
    sys = empty_object()
    sys.coord = toto['nodes'][0][0][0]
    sys.node = toto['nodes'][0][0][1][0] - 1
    sys.dof = toto['nodes'][0][0][2]
    
    sys.n_nodes = sys.node.size
    sys.n_dofs = sys.dof.size
    
    sys.quad = toto['quad'][0][0][0]
    
    sys.n_elms = sys.quad.shape[0]
    
    sys.M = (toto['sys'][0][0][0])
    sys.K = (toto['sys'][0][0][1])
    sys.D = (toto['sys'][0][0][2])
    sys.C = (toto['sys'][0][0][3])
    sys.L = (toto['sys'][0][0][4][0])
    sys.N = (toto['sys'][0][0][5])
    sys.Nf = (toto['sys'][0][0][6])
    sys.Mc = (toto['sys'][0][0][7])
    sys.Kc = (toto['sys'][0][0][8])
        
    return sys

@jit
def boolean_matrix(a, b):
    
    # Get matrix sparsity
    n = 0
    for i in range(a.size):
        for j in range(b.size):
            if (a[i]==b[j]):
                n += 1
    
    data = np.ones((n,), dtype=int)
    idx = np.zeros((n,), dtype=int)
    jdx = np.zeros((n,), dtype=int)
    n = 0
    for i in range(a.size):
        for j in range(b.size):
            if (a[i]==b[j]):
                idx[n] = i
                jdx[n] = j
                n += 1
    
    L = ss.csr_matrix((data, (idx, jdx)), shape=(a.size, b.size))
    
    return L

def freq_h(M, K, n_m=20):

    t0 = time()
    vals, vecs = ssl.eigsh(A=M, k=n_m, M=K)
    f_n = 1./(2.*np.pi) * np.sqrt(1./vals)
    idx = f_n.argsort()
    f_n = f_n[idx]
    phi = vecs[:, idx]
    t1 = time()
    print(t1-t0)
    
    for i_m in range(phi.shape[1]):
        phi[:, i_m] = phi[:, i_m] / sa.norm(phi[:, i_m])
    
    return f_n, phi

def pvet(a):
    return a.reshape((a.size, 1))

#%%

file_name = './comsol/shell_tower_test.mat'
sys = load_from_livelink(file_name)

# sys.Mc = sys.Nf.T @ sys.M @ sys.N
# sys.Kc = sys.Nf.T @ sys.K @ sys.N

# Reordered version of sys.quad
sys.elm = np.zeros(sys.quad.shape, dtype = int)

for i_e in range(sys.n_elms):
    
    r_e = sys.coord[sys.quad[i_e, :], :]
    z_max = r_e[:, 2].max()
    z_min = r_e[:, 2].min()
    
    z = (2./(z_max-z_min)*(r_e[:, 2]-z_min)-1).round().astype(int)
    z1 = np.array([-1, -1, -1, 0, 0, 0, 1, 1, 1], dtype=int)
    z2 = np.array([-1, 0, 1, -1, 0, 1, -1, 0, 1], dtype=int)
    z3 = np.array([1, 1, 1, 0, 0, 0, -1, -1, -1], dtype=int)
    z4 = np.array([1, 0, -1, 1, 0, -1, 1, 0, -1], dtype=int)
    
    if (z == z1).all():
        node_order = np.array([0, 2, 8, 6, 1, 5, 7, 3, 4])
    elif(z == z2).all():
        node_order = np.array([6, 0, 2, 8, 3, 1, 5, 7, 4])
    elif(z == z3).all():
        node_order = np.array([8, 6, 0, 2, 7, 3, 1, 5, 4])
    elif(z == z4).all():
        node_order = np.array([2, 8, 6, 0, 5, 7, 3, 1, 4])
    else:
        print("I'm sorry, Dave, I'm affraid I can't do that")
        
    sys.elm[i_e, :] = sys.quad[i_e, node_order]

del node_order

#%% Nodes normals

h_xi = np.zeros((3, ))
h_eta = np.zeros((3, ))
h_zeta = np.zeros((3, ))
e_xi = np.zeros((sys.n_nodes, 3))
e_eta = np.zeros((sys.n_nodes, 3))
e_zeta = np.zeros((sys.n_nodes, 3))

for i_e in range(sys.n_elms):
    
    # xi and eta order tratidional order
    xi_order = np.array([-1, 1, 1, -1, 0, 1, 0, -1, 0])
    eta_order = np.array([-1, -1, 1, 1, -1, 0, 1, 0, 0])
    
    # Nodes coordonates
    r_e = sys.coord[sys.elm[i_e, :], :]
    x_i = r_e[:, 0]
    y_i = r_e[:, 1]
    z_i = r_e[:, 2]
    
    # Nodes normals
    for i_node in range(9):
        
        node = sys.elm[i_e, i_node]
        
        xi = xi_order[i_node]
        eta = eta_order[i_node]
        
        # Shape functions and derivatives
        N, N_xi, N_eta = fem.shape_functions(xi, eta)

        # Curvilinear coordinates        
        h_xi[0] = N_xi.T @ x_i
        h_xi[1] = N_xi.T @ y_i
        h_xi[2] = N_xi.T @ z_i
        e_xi[node] = h_xi / sa.norm(h_xi)

        h_eta[0] = N_eta.T @ x_i
        h_eta[1] = N_eta.T @ y_i
        h_eta[2] = N_eta.T @ z_i
        e_eta[node] = h_eta / sa.norm(h_eta)

        h_zeta = np.cross(h_xi, h_eta)
        e_zeta[node] = h_zeta / sa.norm(h_zeta)

sys.t1 = e_xi
sys.t2 = e_eta
sys.n = e_zeta

sys.e0 = e_xi.copy()
sys.e2 = e_zeta.copy()
sys.e1 = np.cross(sys.e0, sys.e2)

sys.S =  np.zeros((sys.n_nodes, 3, 3))
sys.S1 =  np.zeros((sys.n_nodes, 3, 3))
for i_n in range(sys.n_nodes):
    sys.S[i_n, 0, :] = sys.e0[i_n, :]
    sys.S[i_n, 1, :] = sys.e1[i_n, :]
    sys.S[i_n, 2, :] = sys.e2[i_n, :]
    
    sys.S1[i_n, :, :] = sa.inv(sys.S[i_n, :, :])

#%%

# Check the normals
t1 = np.zeros((sys.n_elms, 3), dtype=int)
t2 = np.zeros((sys.n_elms, 3), dtype=int)

for i_e in range(sys.n_elms):
    
    nodes = sys.elm[i_e, :]
    t1[i_e, :] = nodes[[0, 1, 2]]
    t2[i_e, :] = nodes[[2, 3, 0]]
    
    pass

sys.triangles = np.concatenate([t1, t2])
sys.x = sys.coord[:, 0]
sys.y = sys.coord[:, 1]
sys.z = sys.coord[:, 2]

fig = mlab.figure()
mlab.triangular_mesh(sys.x, sys.y, sys.z, sys.triangles[:, :])
# mlab.quiver3d(sys.x, sys.y, sys.z, e_eta[:, 0], e_eta[:, 1], e_eta[:, 2], scale_factor=3)
mlab.quiver3d(sys.x, sys.y, sys.z, sys.e1[:, 0], sys.e1[:, 1], sys.e1[:, 2], scale_factor=3)


# elm = np.arange(0, sys.n_elms)
# i = np.arange(0, 9)

# mlab.quiver3d(sys.coord[sys.elm[np.ix_(elm, i)], 0], sys.coord[sys.elm[np.ix_(elm, i)], 1], sys.coord[sys.elm[np.ix_(elm, i)], 2], e_zeta[sys.elm[np.ix_(elm, i)], 0], e_zeta[sys.elm[np.ix_(elm, i)], 1], e_zeta[sys.elm[np.ix_(elm, i)], 2], scale_factor=3)

#%% Constraint theta_z

twf = sys.copy()

twf.q_idx = np.arange(sys.n_dofs)

dofn = np.arange(twf.n_nodes*5).reshape((twf.n_nodes, 5))

twf.N = np.zeros((twf.n_nodes*6, twf.n_nodes*5), dtype=float)
for i_n in range(sys.n_nodes):
    
    n1, n2, n3 = sys.n[i_n, :]
    
    Ne = np.zeros((6, 5), dtype=float)
    Ne[0, 0] = 1.
    Ne[1, 1] = 1.
    Ne[3, 2] = 1.
    Ne[4, 3] = 1.
    Ne[5, 4] = 1.
    Ne[2, 0] = -n1/n3
    Ne[2, 1] = -n2/n3
    
    i_idx = sys.dof[i_n, :]
    j_idx = dofn[i_n, :]
    twf.N[np.ix_(i_idx, j_idx)] = twf.N[np.ix_(i_idx, j_idx)] + Ne
    
twf.N = ss.csc_matrix(twf.N)

twf.Mc = twf.N.T @ twf.M @ twf.N
twf.Kc = twf.N.T @ twf.K @ twf.N




#%% Constraint the bottom, rearranging the matrix order

bot = np.array([i_n for i_n in range(twf.n_nodes) if twf.coord[i_n, 2]<=0.])
top = np.array([i_n for i_n in range(twf.n_nodes) if twf.coord[i_n, 2]>=115.63])

n_dofs = twf.Mc.shape[0]

# DOFs at the bottom
qc_bool = np.zeros((n_dofs, ), dtype=bool)
qc_bool[dofn[bot, :]] = True

# DOFs at the top
qb_bool = np.zeros((n_dofs, ), dtype=bool)
qb_bool[dofn[top, :]] = True

# Internal DOFs (i.e. not the boundary)
qi_bool = ~qb_bool

qb_bool = qb_bool * ~qc_bool
qi_bool = qi_bool * ~qc_bool

q_idx = np.arange(n_dofs)
qr_idx = np.concatenate([q_idx[qb_bool], q_idx[qi_bool]])

twr = empty_object()

twr.Nr = boolean_matrix(q_idx, qr_idx)
twr.Mc = twr.Nr.T @ twf.Mc @ twr.Nr
twr.Kc = twr.Nr.T @ twf.Kc @ twr.Nr

twr.f_n, twr.phi = freq_h(twr.Mc, twr.Kc)

#%% Display mode shapes

i_m = 8
u = sys.N @ twr.Nr @ twr.phi[:, i_m]*200
# i_m = 6
# u = sys.Nf @ Nr @ N @ alpha_1 @ phi_1p[:, i_m]*200

ux = u[sys.dof[:, 3]]
vy = u[sys.dof[:, 4]]
wz = u[sys.dof[:, 5]]

x = sys.coord[:, 0] + ux
y = sys.coord[:, 1] + vy
z = sys.coord[:, 2] + wz


if (gen_plots):
    fig = mlab.figure()
    mlab.triangular_mesh(x, y, z, sys.triangles)
    # mlab.points3d(x, y, z, scale_factor=1.)    
    # mlab.triangular_mesh(x, y, z, triangles)


#%% Constraint the top

# Forget the original dof numbering, embrace the new one
n_b = q_idx[qb_bool].size
n_i = q_idx[qi_bool].size
n_c = 6

cnt = empty_object()
cnt.r = sys.coord[top, :]
cnt.n = sys.n[top, :]
cnt.r_c = sys.coord[top, :].mean(axis=0)
cnt.dof = np.arange(n_b).reshape((int(n_b/5), 5))

n_elms = top.shape[0]
n_dofs = cnt.dof.size
A = np.zeros((n_dofs, 6))
for i_e in range(n_elms):
    Ae = fem.rigid_connector(cnt.r[i_e], cnt.n[i_e], cnt.r_c)
    i_idx = cnt.dof[i_e, :]
    j_idx = np.arange(6)

    A[np.ix_(i_idx, j_idx)] = A[np.ix_(i_idx, j_idx)] + Ae

A = ss.coo_matrix(A)
I = ss.eye(n_i, format="coo")
O_bi = ss.coo_matrix((n_b, n_i))
O_ic = ss.coo_matrix((n_i, n_c))

twn = empty_object()

twn.N = ss.csr_matrix(ss.bmat([[A, O_bi],[O_ic, I]]))
twn.Mc = twn.N.T @ twr.Mc @ twn.N
twn.Kc = twn.N.T @ twr.Kc @ twn.N

twn.f_n, twn.phi = freq_h(twn.Mc, twn.Kc)


#%% Display mode shapes

i_m = 6
u = sys.N @ twr.Nr @ twn.N @ twn.phi[:, i_m]*400

ax = u[sys.dof[:, 0]]
ay = u[sys.dof[:, 1]]
az = u[sys.dof[:, 2]]
ux = u[sys.dof[:, 3]]
vy = u[sys.dof[:, 4]]
wz = u[sys.dof[:, 5]]

x = sys.coord[:, 0] + ux
y = sys.coord[:, 1] + vy
z = sys.coord[:, 2] + wz

if (gen_plots):
    fig = mlab.figure()
    mlab.triangular_mesh(x, y, z, sys.triangles)

#%% Apply boundary conditions at the top nodes







#%% Constraint the bottom, rearranging the matrix order

# bot = np.array([i_n for i_n in range(sys.n_nodes) if sys.coord[i_n, 2]<=0.])
# top = np.array([i_n for i_n in range(sys.n_nodes) if sys.coord[i_n, 2]>=115.63])

# # Constraint DOF at the bottom
# qc = np.zeros((sys.n_dofs, ))
# qc[sys.dof[bot, :].flatten()] = 1
# qc = sys.Nf.T @ qc
# qc_bool = np.zeros(qc.shape, dtype=bool)
# qc_bool[qc!=0] = True

# # DOFs at the boundary
# qb = np.zeros((sys.n_dofs, ))
# # qb[sys.dof[top, -3:].flatten()] = 1.
# qb[sys.dof[top, :].flatten()] = 1.
# qb = sys.Nf.T @ qb
# qb_bool = np.zeros(qb.shape, dtype=bool)
# qb_bool[qb!=0] = True
# qi_bool = ~qb_bool

# qb_bool = qb_bool * ~qc_bool
# qi_bool = qi_bool * ~qc_bool

# n_dofs = sys.Nf.shape[1]
# q_idx = np.arange(n_dofs)
# qr_idx = np.concatenate((q_idx[qb_bool], q_idx[qi_bool]))

# Nr = boolean_matrix(q_idx, qr_idx)
# Mr = Nr.T @ sys.Mc @ Nr
# Kr = Nr.T @ sys.Kc @ Nr

# f_n, phi = freq_h(Mr, Kr)

# toto, _ = freq_h(Nr.T @ Mc @ Nr, Nr.T @ Kc @ Nr)

#%%

# # DOFs at the bottom
# qc_bool = np.zeros((sys.n_dofs, ), dtype=bool)
# qc_bool[sys.dof[bot, :].flatten()] = True

# # DOFs at the top
# qb_bool = np.zeros((sys.n_dofs, ), dtype=bool)
# qb_bool[sys.dof[top, :].flatten()] = True








#%% Another way of constraining the bottom

# qc_idx = np.zeros(qc_bool.sum(), dtype=int)
# nc = 0
# for i in range(qc_bool.size):
#     if qc_bool[i]:
#         qc_idx[nc] = i
#         nc += 1
#         pass
#     pass

# B = np.zeros((qc_idx.size, sys.Mc.shape[1]))
# for i in range(qc_idx.size):
#     B[i, qc_idx[i]] = 1
#     pass

# Nc = sa.null_space(B)
# B = ss.csr_matrix(B)
# Nc = ss.csr_matrix(Nc)

# Md = Nc.T @ sys.Mc @ Nc
# Kd = Nc.T @ sys.Kc @ Nc

# fd, _ = freq_h(Md, Kd)

#%%

# q = np.zeros((sys.n_dofs, ), dtype=object)
# q[:6] = np.array(sym.symbols('a_x, a_y, a_z, u, v, w'))

# N = sys.N.todense()

# qt = N.T @ q


#%% Check the normals



#%%




# B = np.zeros((sys.n_nodes, sys.n_dofs))

# for i_n in range(sys.n_nodes):
    
#     B[i_n, sys.dof[i_n, 0]] = sys.n[i_n, 0]
#     B[i_n, sys.dof[i_n, 1]] = sys.n[i_n, 1]
#     B[i_n, sys.dof[i_n, 2]] = sys.n[i_n, 2]
    
# B = ss.csc_matrix(B)





#%%

# # DOFs at the boundary
# qb = np.zeros((sys.n_dofs, ))
# # qb[sys.dof[top, -3:].flatten()] = 1.
# qb[sys.dof[top, :].flatten()] = 1.
# qb = sys.Nf.T @ qb
# qb_bool = np.zeros(qb.shape, dtype=bool)
# qb_bool[qb!=0] = True




#%%


# x = [Kr[5*top[i]+2, 5*top[i]+2] for i in range(32)]

# fig = plt.figure()
# plt.plot(x)

# theta = np.arctan2(sys.coord[top, 0], sys.coord[top, 1])

# x = [sys.K[6*top[i]+4, 6*top[i]+4] for i in range(32)]
# x=np.array(x)

# isort = np.argsort(theta)

# theta=theta[isort]
# x=x[isort]

# fig = plt.figure()
# plt.plot(theta[::2], x[::2], 'o')


# fig = plt.plot()
# plt.plot(sys.coord[top, 0])

#%%




#%% Constraint the top

# # Forget the original dof numbering, embrace the new one
# n_b = q_idx[qb_bool].size
# n_i = q_idx[qi_bool].size
# n_c = 6

# cnt = empty_object()
# cnt.r = sys.coord[top, :]
# cnt.r_c = sys.coord[top, :].mean(axis=0)
# cnt.dof = np.arange(n_b).reshape((int(n_b/3), 3))

# n_elms = top.shape[0]
# n_dofs = cnt.dof.size
# A = np.zeros((n_dofs, 6))
# for i_e in range(n_elms):
#     Ae, _ = rigid_string(cnt.r[i_e], cnt.r_c)
#     i_idx = cnt.dof[i_e, :]
#     j_idx = np.arange(6)

#     A[np.ix_(i_idx, j_idx)] = A[np.ix_(i_idx, j_idx)] + Ae

# A = ss.coo_matrix(A)
# I = ss.eye(n_i, format="coo")
# O_bi = ss.coo_matrix((n_b, n_i))
# O_ic = ss.coo_matrix((n_i, n_c))

# N = ss.csr_matrix(ss.bmat([[A, O_bi],[O_ic, I]]))
# Mp = N.T @ Mr @ N
# Kp = N.T @ Kr @ N

# fp_n, _ = freq_h(Mp, Kp, 20)

# #%% Exporting the results

num2str = np.vectorize(cf.num2str)

fn_comsol = np.loadtxt('./comsol/tower_connector.txt', skiprows=5, usecols=0)

error = np.abs((twn.f_n - fn_comsol)/fn_comsol) *100.

table = cf.LatexTable()

table.filename = '../tables/appendix_rigid_connector.tex'
table.M = num2str(np.block([[fn_comsol], [twn.f_n], [error]]).T, '%0.8e')
table.V = num2str(np.arange(twn.f_n.size) + 1, '%i')
table.H = np.array(['COMSOL [Hz]', 'Model [Hz]', 'Error [\%]'])
table.corner = 'Mode'
table.print()


# #%%











