import numpy as np
import scipy.linalg as sa
import copy

from .numpy_beam_3d_linear import beam_3d

#%% Functions

def boolean_matrix(a, b): # return L
    L = np.zeros((a.size, b.size), dtype=int)
    for i in range(a.size):
        for j in range(b.size):
            if (a[i]==b[j]):
                L[i, j] = 1
    return L

#%% Classes

class Beam3D(object):
    
    def __init__(self, x, A, I_x, I_y, J, rho, E, G):

        self.n_npe = 2
        self.n_dpn = 6

        self.x = x
        
        self.n_nodes = x.size
        self.n_elms = self.n_nodes - 1
        self.n_dofs = self.n_nodes * self.n_dpn

        self.A = A if isinstance(A, np.ndarray) else A*np.ones((self.n_elms, self.n_npe))
        self.I_x = I_x if isinstance(I_x, np.ndarray) else I_x*np.ones((self.n_elms, self.n_npe))
        self.I_y = I_y if isinstance(I_y, np.ndarray) else I_y*np.ones((self.n_elms, self.n_npe))
        self.J = J if isinstance(J, np.ndarray) else J*np.ones((self.n_elms, self.n_npe))
        self.rho = rho if isinstance(rho, np.ndarray) else rho*np.ones((self.n_elms, self.n_npe))
        self.E = E if isinstance(E, np.ndarray) else E*np.ones((self.n_elms, self.n_npe))
        self.G = G if isinstance(G, np.ndarray) else G*np.ones((self.n_elms, self.n_npe))
        
        self.nodes = np.array([[i_e, i_e+1] for i_e in range(self.n_elms)])
        self.dofs = np.arange(self.n_dofs).reshape((self.n_nodes, self.n_dpn))
        self.cts = np.zeros(self.dofs.shape, dtype=bool)
        
        self.q_idx = np.arange(self.n_dofs)
        self.M = np.zeros((self.n_dofs, self.n_dofs))
        self.K = np.zeros((self.n_dofs, self.n_dofs))
        
        for i_e in range(self.n_elms):
            n0, n1 = self.nodes[i_e, :]
            qe_idx = np.concatenate((self.dofs[n0, :], self.dofs[n1, :]))
            Le = boolean_matrix(qe_idx, self.q_idx)
            
            l = self.x[n1] - self.x[n0]
            A = self.A[i_e, :].mean()
            I_x = self.I_x[i_e, :].mean()
            I_y = self.I_y[i_e, :].mean()
            J = self.J[i_e, :].mean()
            rho = self.rho[i_e, :].mean()
            E = self.E[i_e, :].mean()
            G = self.G[i_e, :].mean()
            
            Me, Ke = beam_3d(l, A, I_x, I_y, J, rho, E, G)
            self.M += Le.T @ Me @ Le
            self.K += Le.T @ Ke @ Le
    
    def fixed_constraint(self, cts=None):
        
        if (cts is not None):        
            self.cts = cts

        q_bool = ~self.cts.flatten()
        
        self.M = self.M[np.ix_(q_bool, q_bool)]
        self.K = self.K[np.ix_(q_bool, q_bool)]
        
        k = 0
        for i in range(self.n_nodes):
            for j in range(self.n_dpn):
                if (self.cts[i, j]):
                    self.dofs[i, j] = -1
                else:
                    self.dofs[i, j] = k
                    k += 1
        self.n_dofs = k
        self.q_idx = np.arange(self.n_dofs)    
