import sympy as sym
import numpy as np

from my_printer import MyPrinter
code_gen = MyPrinter().doprint

#%% Local functions

def rotation_matrix(angle,axis): # return A
    '''
    Generates a transformation matrix.
    
    Parameters
    ----------
    angle : sympy.Symbol or sympy.Function
            [rad] angle between the two reference frames along the 'axis'
    axis : str
           axis of roation. 'x', 'y' and 'z'.
    
    Returns
    -------
    A : sympy.Matrix [:, :]
        transformation matrix from reference frame 1 to reference frame 2.
    '''
    if (axis=='x'):
        A = sym.Matrix([[1, 0, 0],
                    [0, sym.cos(angle), sym.sin(angle)],
                    [0, -sym.sin(angle), sym.cos(angle)]])
    elif (axis=='y'):
        A = sym.Matrix([[sym.cos(angle), 0, -sym.sin(angle)],
                    [0, 1, 0],
                    [sym.sin(angle), 0, sym.cos(angle)]])
    elif (axis=='z'):
        A = sym.Matrix([[sym.cos(angle), sym.sin(angle), 0],
                    [-sym.sin(angle), sym.cos(angle), 0],
                    [0, 0, 1]])
    else:
        print('error in rotate_matrix function')
    
    return A


#%%

alpha, beta, gamma = sym.symbols('alpha, beta, gamma')

r_p = sym.Matrix(sym.symbols('x_p, y_p, z_p'))
r_c = sym.Matrix(sym.symbols('x_c, y_c, z_c'))

A = rotation_matrix(beta, 'y') @ rotation_matrix(alpha, 'x')
n = A.T @ sym.Matrix([0, 0, 1])

v = r_p - r_c
d = sym.Abs((n.T @ v)[0])






