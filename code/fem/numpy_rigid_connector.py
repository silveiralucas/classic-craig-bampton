import numpy as np

#%%
    
def rigid_connector(r_p, r_c, t_0, t_1):
    x_p, y_p, z_p = r_p
    x_c, y_c, z_c = r_c
    t_0x, t_0y, t_0z = t_0
    t_1x, t_1y, t_1z = t_1
    
    B_e = np.zeros((5, 6), dtype=float)
    B_e[0, 3] = t_1x
    B_e[0, 4] = t_1y
    B_e[0, 5] = t_1z
    B_e[1, 3] = -t_0x
    B_e[1, 4] = -t_0y
    B_e[1, 5] = -t_0z
    B_e[2, 0] = 1
    B_e[2, 4] = -z_c + z_p
    B_e[2, 5] = y_c - y_p
    B_e[3, 1] = 1
    B_e[3, 3] = z_c - z_p
    B_e[3, 5] = -x_c + x_p
    B_e[4, 2] = 1
    B_e[4, 3] = -y_c + y_p
    B_e[4, 4] = x_c - x_p
    
    return B_e
