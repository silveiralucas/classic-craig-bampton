import numpy as np

#%%
    
def rigid_connector2(r_p, r_c):
    x_p, y_p, z_p = r_p
    x_c, y_c, z_c = r_c
    
    B_e = np.zeros((6, 6), dtype=float)
    B_e[0, 0] = 1
    B_e[0, 4] = -z_c + z_p
    B_e[0, 5] = y_c - y_p
    B_e[1, 1] = 1
    B_e[1, 3] = z_c - z_p
    B_e[1, 5] = -x_c + x_p
    B_e[2, 2] = 1
    B_e[2, 3] = -y_c + y_p
    B_e[2, 4] = x_c - x_p
    B_e[3, 3] = 1
    B_e[4, 4] = 1
    B_e[5, 5] = 1
    
    return B_e
