import numpy as np
import scipy.linalg as sa

#%% Local functions

def qd9_n(x_i, y_i, z_i, x, y):
    
    A = np.zeros((x_i.size, x_i.size), dtype=float)
    for i in range(x_i.size):
        A[i, :] = np.array([1., x_i[i], y_i[i], x_i[i]*y_i[i], x_i[i]**2, y_i[i]**2, x_i[i]**2*y_i[i], x_i[i]*y_i[i]**2, x_i[i]**2*y_i[i]**2], dtype=float)
    c = sa.inv(A)@z_i
    
    # z = c.T @ np.array([1., x, y, x*y, x**2., y**2., x**2.*y, x*y**2., x**2.*y**2.], dtype=float) 
    z_x = c.T @ np.array([0., 1., 0., y, 2.*x, 0., 2.*x*y, y**2., 2.*x*y**2.], dtype=float) 
    z_y = c.T @ np.array([0., 0., 1., x, 0., 2.*y, x**2., 2.*x*y, 2.*x**2.*y], dtype=float)
    
    # r = np.array([x, y, z], dtype=float)
    r_x = np.array([1., 0., z_x], dtype=float)
    r_y = np.array([0., 1., z_y], dtype=float)
    h = np.cross(r_x, r_y)
    n = h/sa.norm(h)
    
    # Parametric coordinates
    e_2 = n
    e_0 = r_x / sa.norm(r_x)
    e_1 = np.cross(e_2, e_0)
    
    return e_0, e_1, e_2
