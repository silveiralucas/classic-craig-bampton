import numpy as np
import scipy.linalg as sa
import scipy.optimize as opt
import matplotlib.pyplot as plt
import copy
from mayavi import mlab

#%% Local functions

class EmptyObject(object):
    def copy(self):
        return copy.deepcopy(self)

def rotation_matrix(angle,axis):
    if (axis=='x'):
        A = np.array([[1, 0, 0],
                    [0, np.cos(angle), np.sin(angle)],
                    [0, -np.sin(angle), np.cos(angle)]], dtype=float)
    elif (axis=='y'):
        A = np.array([[np.cos(angle), 0, -np.sin(angle)],
                    [0, 1, 0],
                    [np.sin(angle), 0, np.cos(angle)]], dtype=float)
    elif (axis=='z'):
        A = np.array([[np.cos(angle), np.sin(angle), 0],
                    [-np.sin(angle), np.cos(angle), 0],
                    [0, 0, 1]], dtype=float)
    else:
        print('error in rotate_matrix function')
    return A

def n10(s0, n=10):
    n1 = (s0.shape[0]-1)*(n-2)+s0.shape[0]
    s = np.zeros((n1, s0.shape[1]), dtype=float)
    for i_n in range(s0.shape[0]-1):
        for i_d in range(s0.shape[1]):
            s[9*i_n:9*(i_n+1)+1, i_d] = np.linspace(s0[i_n, i_d], s0[i_n+1, i_d], n)
    return s

#%% Bent bar

# Undeformed geometry
g0 = EmptyObject()

# Cetreline
g0.r_c = np.zeros((200, 3))
g0.r_c[:, 2] = np.linspace(0., 10.e3, 200)

# Sections
p0 = np.array([5.1, -234., 0.])
p1 = np.array([5.1, 234., 0.])
p2 = np.array([100., 234., 0.])
p3 = np.array([100., 250., 0.])
p4 = np.array([-100., 250., 0.])
p5 = np.array([-100., 234., 0.])
p6 = np.array([-5.1, 234., 0.])
p7 = np.array([-5.1, -234., 0.])
p8 = np.array([-100., -234., 0.])
p9 = np.array([-100., -250., 0.])
p10 = np.array([100., -250., 0.])
p11 = np.array([100., -234., 0.])

s = np.block([[p0], [p1], [p2], [p3], [p4], [p5], [p6], [p7], [p8], [p9], [p10], [p11], [p0]])
g0.s = np.zeros((g0.r_c.shape[0], s.shape[0], s.shape[1]), dtype=float)
for i_z in range(g0.s.shape[0]):
    g0.s[i_z, :, :] = s
    g0.s[i_z, :, 2] = g0.r_c[i_z, 2]

# Deformed geometry
g1 = EmptyObject()

# Centreline
g1.r_c = g0.r_c.copy()
g1.r_c[:, 0] = 1.e-5 * g1.r_c[:, 2]**2

# Unit vectors and transformation matrices
g1.e_0 = np.zeros(g1.r_c.shape, dtype=float)
g1.e_1 = np.zeros(g1.r_c.shape, dtype=float)
g1.e_2 = np.zeros(g1.r_c.shape, dtype=float)
g1.A_01 = np.zeros((g1.r_c.shape[0], 3, 3), dtype=float)
for i_z in range(g1.r_c.shape[0]):
    g1.e_1[i_z, :] = [0., 1., 0.]

    g1.e_2[i_z, :] = [2.e-2 * g1.r_c[i_z, 2], 0., 1.]
    g1.e_2[i_z, :] = g1.e_2[i_z, :]/sa.norm(g1.e_2[i_z, :])
    
    g1.e_0[i_z, :] = np.cross(g1.e_1[i_z, :], g1.e_2[i_z, :])
    g1.e_0[i_z, :] = g1.e_0[i_z, :]/sa.norm(g1.e_0[i_z, :])
    
    g1.A_01[i_z, 0, :] = g1.e_0[i_z, :]
    g1.A_01[i_z, 1, :] = g1.e_1[i_z, :]
    g1.A_01[i_z, 2, :] = g1.e_2[i_z, :]

# Sections 
g1.s = np.zeros(g0.s.shape, dtype=float)
for i_z in range(g0.s.shape[0]):
    for i_n in range(g0.s.shape[1]):
        g1.s[i_z, i_n, :] = (g1.A_01[i_z, :, :] @ (g0.s[i_z, i_n, :] - g0.r_c[i_z, :])) + g1.r_c[i_z, :]

fig = mlab.figure()
mlab.plot3d(g0.r_c[:, 0], g0.r_c[:, 1], g0.r_c[:, 2], tube_radius=2.5e1)
mlab.plot3d(g1.r_c[:, 0], g1.r_c[:, 1], g1.r_c[:, 2], tube_radius=2.5e1)

mlab.plot3d(g0.s[0, :, 0], g0.s[0, :, 1], g0.s[0, :, 2], tube_radius=1.e1)
mlab.plot3d(g1.s[-1, :, 0], g1.s[-1, :, 1], g1.s[-1, :, 2], tube_radius=1.e1)


# fig = mlab.figure()
# mlab.plot3d(g1.r_c[:, 0], g1.r_c[:, 1], g1.r_c[:, 2], tube_radius=0.025)
# mlab.points3d(g0.s1[:, 0], g0.s1[:, 1], g0.s1[:, 2]*0, scale_factor=0.1)
# mlab.points3d(g1.s[:, 0], g1.s[:, 1], g1.s[:, 2]*0, scale_factor=0.1)
