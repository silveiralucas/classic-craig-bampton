import numpy as np

#%%
    
def beam_2d_const(x, A, I, rho, E): # returns M_e, K_e
    '''
    Mass and stiffness matrices of a two-dimensional Euler-Bernoulli beam with
    three degrees of freedom (longitudinal, transverse and angular) with
    constant cross section geometry and material properties.
    
    P.S.: this function was generated automatically from the beam_2d.py
    symbolic script.
    
    Paramenters
    -----------
    x : numpy.ndarray([2], dtype=float)
        undeformed nodal longitudinal position [m]
    A : float
        cross section area [m**2]
    I : float
        cross section area moment of inertia [m**4]
    rho : float
          material density [kg/m**2]
    E : float
        Young's modulus [Pa]
    
    Returns
    -------
    M_e : numpy.ndarray([6, 6], dtype=float)
          mass matrix [kg]
    K_e : numpy.ndarray([6, 6], dtype=float)
          stiffness matrix [N/m]
    '''

    l = x[1] - x[0]
    
    K_e = np.zeros((6, 6), dtype=float)
    K_e[0, 0] = A*E/l
    K_e[0, 3] = -A*E/l
    K_e[1, 1] = 12*E*I/l**3
    K_e[1, 2] = 6*E*I/l**2
    K_e[1, 4] = -12*E*I/l**3
    K_e[1, 5] = 6*E*I/l**2
    K_e[2, 1] = 6*E*I/l**2
    K_e[2, 2] = 4*E*I/l
    K_e[2, 4] = -6*E*I/l**2
    K_e[2, 5] = 2*E*I/l
    K_e[3, 0] = -A*E/l
    K_e[3, 3] = A*E/l
    K_e[4, 1] = -12*E*I/l**3
    K_e[4, 2] = -6*E*I/l**2
    K_e[4, 4] = 12*E*I/l**3
    K_e[4, 5] = -6*E*I/l**2
    K_e[5, 1] = 6*E*I/l**2
    K_e[5, 2] = 2*E*I/l
    K_e[5, 4] = -6*E*I/l**2
    K_e[5, 5] = 4*E*I/l
    
    M_e = np.zeros((6, 6), dtype=float)
    M_e[0, 0] = (1/3)*A*l*rho
    M_e[0, 3] = (1/6)*A*l*rho
    M_e[1, 1] = (13/35)*A*l*rho
    M_e[1, 2] = (11/210)*A*l**2*rho
    M_e[1, 4] = (9/70)*A*l*rho
    M_e[1, 5] = -13/420*A*l**2*rho
    M_e[2, 1] = (11/210)*A*l**2*rho
    M_e[2, 2] = (1/105)*A*l**3*rho
    M_e[2, 4] = (13/420)*A*l**2*rho
    M_e[2, 5] = -1/140*A*l**3*rho
    M_e[3, 0] = (1/6)*A*l*rho
    M_e[3, 3] = (1/3)*A*l*rho
    M_e[4, 1] = (9/70)*A*l*rho
    M_e[4, 2] = (13/420)*A*l**2*rho
    M_e[4, 4] = (13/35)*A*l*rho
    M_e[4, 5] = -11/210*A*l**2*rho
    M_e[5, 1] = -13/420*A*l**2*rho
    M_e[5, 2] = -1/140*A*l**3*rho
    M_e[5, 4] = -11/210*A*l**2*rho
    M_e[5, 5] = (1/105)*A*l**3*rho
    
    return M_e, K_e
