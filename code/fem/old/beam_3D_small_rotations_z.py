import numpy as np

#%%
    
def beam_3D(G, rho, E, I_y, J, l, I_x, A):
    
    K_e = np.zeros((12, 12), dtype=float)
    K_e[0, 0] = 12*E*I_y/l**3
    K_e[0, 4] = 6*E*I_y/l**2
    K_e[0, 6] = -12*E*I_y/l**3
    K_e[0, 10] = 6*E*I_y/l**2
    K_e[1, 1] = 12*E*I_x/l**3
    K_e[1, 3] = -6*E*I_x/l**2
    K_e[1, 7] = -12*E*I_x/l**3
    K_e[1, 9] = -6*E*I_x/l**2
    K_e[2, 2] = A*E/l
    K_e[2, 8] = -A*E/l
    K_e[3, 1] = -6*E*I_x/l**2
    K_e[3, 3] = 4*E*I_x/l
    K_e[3, 7] = 6*E*I_x/l**2
    K_e[3, 9] = 2*E*I_x/l
    K_e[4, 0] = 6*E*I_y/l**2
    K_e[4, 4] = 4*E*I_y/l
    K_e[4, 6] = -6*E*I_y/l**2
    K_e[4, 10] = 2*E*I_y/l
    K_e[5, 5] = G*J/l
    K_e[5, 11] = -G*J/l
    K_e[6, 0] = -12*E*I_y/l**3
    K_e[6, 4] = -6*E*I_y/l**2
    K_e[6, 6] = 12*E*I_y/l**3
    K_e[6, 10] = -6*E*I_y/l**2
    K_e[7, 1] = -12*E*I_x/l**3
    K_e[7, 3] = 6*E*I_x/l**2
    K_e[7, 7] = 12*E*I_x/l**3
    K_e[7, 9] = 6*E*I_x/l**2
    K_e[8, 2] = -A*E/l
    K_e[8, 8] = A*E/l
    K_e[9, 1] = -6*E*I_x/l**2
    K_e[9, 3] = 2*E*I_x/l
    K_e[9, 7] = 6*E*I_x/l**2
    K_e[9, 9] = 4*E*I_x/l
    K_e[10, 0] = 6*E*I_y/l**2
    K_e[10, 4] = 2*E*I_y/l
    K_e[10, 6] = -6*E*I_y/l**2
    K_e[10, 10] = 4*E*I_y/l
    K_e[11, 5] = -G*J/l
    K_e[11, 11] = G*J/l
    
    M_e = np.zeros((12, 12), dtype=float)
    M_e[0, 0] = (13/35)*A*l*rho + (6/5)*I_y*rho/l
    M_e[0, 4] = (11/210)*A*l**2*rho + (1/10)*I_y*rho
    M_e[0, 6] = (9/70)*A*l*rho - 6/5*I_y*rho/l
    M_e[0, 10] = -13/420*A*l**2*rho + (1/10)*I_y*rho
    M_e[1, 1] = (13/35)*A*l*rho + (6/5)*I_x*rho/l
    M_e[1, 3] = -11/210*A*l**2*rho - 1/10*I_x*rho
    M_e[1, 7] = (9/70)*A*l*rho - 6/5*I_x*rho/l
    M_e[1, 9] = (13/420)*A*l**2*rho - 1/10*I_x*rho
    M_e[2, 2] = (1/3)*A*l*rho
    M_e[2, 8] = (1/6)*A*l*rho
    M_e[3, 1] = -11/210*A*l**2*rho - 1/10*I_x*rho
    M_e[3, 3] = (1/105)*A*l**3*rho + (2/15)*I_x*l*rho
    M_e[3, 7] = -13/420*A*l**2*rho + (1/10)*I_x*rho
    M_e[3, 9] = -1/140*A*l**3*rho - 1/30*I_x*l*rho
    M_e[4, 0] = (11/210)*A*l**2*rho + (1/10)*I_y*rho
    M_e[4, 4] = (1/105)*A*l**3*rho + (2/15)*I_y*l*rho
    M_e[4, 6] = (13/420)*A*l**2*rho - 1/10*I_y*rho
    M_e[4, 10] = -1/140*A*l**3*rho - 1/30*I_y*l*rho
    M_e[5, 5] = (1/3)*J*l*rho
    M_e[5, 11] = (1/6)*J*l*rho
    M_e[6, 0] = (9/70)*A*l*rho - 6/5*I_y*rho/l
    M_e[6, 4] = (13/420)*A*l**2*rho - 1/10*I_y*rho
    M_e[6, 6] = (13/35)*A*l*rho + (6/5)*I_y*rho/l
    M_e[6, 10] = -11/210*A*l**2*rho - 1/10*I_y*rho
    M_e[7, 1] = (9/70)*A*l*rho - 6/5*I_x*rho/l
    M_e[7, 3] = -13/420*A*l**2*rho + (1/10)*I_x*rho
    M_e[7, 7] = (13/35)*A*l*rho + (6/5)*I_x*rho/l
    M_e[7, 9] = (11/210)*A*l**2*rho + (1/10)*I_x*rho
    M_e[8, 2] = (1/6)*A*l*rho
    M_e[8, 8] = (1/3)*A*l*rho
    M_e[9, 1] = (13/420)*A*l**2*rho - 1/10*I_x*rho
    M_e[9, 3] = -1/140*A*l**3*rho - 1/30*I_x*l*rho
    M_e[9, 7] = (11/210)*A*l**2*rho + (1/10)*I_x*rho
    M_e[9, 9] = (1/105)*A*l**3*rho + (2/15)*I_x*l*rho
    M_e[10, 0] = -13/420*A*l**2*rho + (1/10)*I_y*rho
    M_e[10, 4] = -1/140*A*l**3*rho - 1/30*I_y*l*rho
    M_e[10, 6] = -11/210*A*l**2*rho - 1/10*I_y*rho
    M_e[10, 10] = (1/105)*A*l**3*rho + (2/15)*I_y*l*rho
    M_e[11, 5] = (1/6)*J*l*rho
    M_e[11, 11] = (1/3)*J*l*rho
    
    return M_e, K_e
