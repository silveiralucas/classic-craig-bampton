import numpy as np

#%%
    
def beam_3d(l, A, I_x, I_y, J, rho, E, G):
    '''
    Mass and stiffness matrices of a three-dimensional Euler-Bernoulli beam
    with six degrees of freedom (longitudinal, transverse and angular) with
    constant cross section geometry and material properties.
    
    P.S.: this function was generated automatically from the 
    symbolic_beam_3D_small_rotations_z.py  symbolic script.
    
    Paramenters
    -----------
    l : float
        beam element length [m]
    A : float
        cross section area [m**2]
    I_x : float
          cross section area moment of inertia around x axis [m**4]
    I_y : float
          cross section area moment of inertia around y axis [m**4]
    J : float
        cross section polar area moment of inertia [m**4]
    rho : float
          material density [kg/m**2]
    E : float
        Young's modulus [Pa]
    G : float
        shear modulus of elasticity [Pa]
    
    Returns
    -------
    M_e : numpy.ndarray([6, 6], dtype=float)
          mass matrix [kg]
    K_e : numpy.ndarray([6, 6], dtype=float)
          stiffness matrix [N/m]
    '''

    K_e = np.zeros((12, 12), dtype=float)
    K_e[0, 0] = 12*E*I_y/l**3
    K_e[0, 4] = 6*E*I_y/l**2
    K_e[0, 6] = -12*E*I_y/l**3
    K_e[0, 10] = 6*E*I_y/l**2
    K_e[1, 1] = 12*E*I_x/l**3
    K_e[1, 3] = -6*E*I_x/l**2
    K_e[1, 7] = -12*E*I_x/l**3
    K_e[1, 9] = -6*E*I_x/l**2
    K_e[2, 2] = A*E/l
    K_e[2, 8] = -A*E/l
    K_e[3, 1] = -6*E*I_x/l**2
    K_e[3, 3] = 4*E*I_x/l
    K_e[3, 7] = 6*E*I_x/l**2
    K_e[3, 9] = 2*E*I_x/l
    K_e[4, 0] = 6*E*I_y/l**2
    K_e[4, 4] = 4*E*I_y/l
    K_e[4, 6] = -6*E*I_y/l**2
    K_e[4, 10] = 2*E*I_y/l
    K_e[5, 5] = G*J/l
    K_e[5, 11] = -G*J/l
    K_e[6, 0] = -12*E*I_y/l**3
    K_e[6, 4] = -6*E*I_y/l**2
    K_e[6, 6] = 12*E*I_y/l**3
    K_e[6, 10] = -6*E*I_y/l**2
    K_e[7, 1] = -12*E*I_x/l**3
    K_e[7, 3] = 6*E*I_x/l**2
    K_e[7, 7] = 12*E*I_x/l**3
    K_e[7, 9] = 6*E*I_x/l**2
    K_e[8, 2] = -A*E/l
    K_e[8, 8] = A*E/l
    K_e[9, 1] = -6*E*I_x/l**2
    K_e[9, 3] = 2*E*I_x/l
    K_e[9, 7] = 6*E*I_x/l**2
    K_e[9, 9] = 4*E*I_x/l
    K_e[10, 0] = 6*E*I_y/l**2
    K_e[10, 4] = 2*E*I_y/l
    K_e[10, 6] = -6*E*I_y/l**2
    K_e[10, 10] = 4*E*I_y/l
    K_e[11, 5] = -G*J/l
    K_e[11, 11] = G*J/l
    
    M_e = np.zeros((12, 12), dtype=float)
    M_e[0, 0] = (13/35)*A*l*rho
    M_e[0, 4] = (11/210)*A*l**2*rho
    M_e[0, 6] = (9/70)*A*l*rho
    M_e[0, 10] = -13/420*A*l**2*rho
    M_e[1, 1] = (13/35)*A*l*rho
    M_e[1, 3] = -11/210*A*l**2*rho
    M_e[1, 7] = (9/70)*A*l*rho
    M_e[1, 9] = (13/420)*A*l**2*rho
    M_e[2, 2] = (1/3)*A*l*rho
    M_e[2, 8] = (1/6)*A*l*rho
    M_e[3, 1] = -11/210*A*l**2*rho
    M_e[3, 3] = (1/105)*A*l**3*rho
    M_e[3, 7] = -13/420*A*l**2*rho
    M_e[3, 9] = -1/140*A*l**3*rho
    M_e[4, 0] = (11/210)*A*l**2*rho
    M_e[4, 4] = (1/105)*A*l**3*rho
    M_e[4, 6] = (13/420)*A*l**2*rho
    M_e[4, 10] = -1/140*A*l**3*rho
    M_e[5, 5] = (1/3)*J*l*rho
    M_e[5, 11] = (1/6)*J*l*rho
    M_e[6, 0] = (9/70)*A*l*rho
    M_e[6, 4] = (13/420)*A*l**2*rho
    M_e[6, 6] = (13/35)*A*l*rho
    M_e[6, 10] = -11/210*A*l**2*rho
    M_e[7, 1] = (9/70)*A*l*rho
    M_e[7, 3] = -13/420*A*l**2*rho
    M_e[7, 7] = (13/35)*A*l*rho
    M_e[7, 9] = (11/210)*A*l**2*rho
    M_e[8, 2] = (1/6)*A*l*rho
    M_e[8, 8] = (1/3)*A*l*rho
    M_e[9, 1] = (13/420)*A*l**2*rho
    M_e[9, 3] = -1/140*A*l**3*rho
    M_e[9, 7] = (11/210)*A*l**2*rho
    M_e[9, 9] = (1/105)*A*l**3*rho
    M_e[10, 0] = -13/420*A*l**2*rho
    M_e[10, 4] = -1/140*A*l**3*rho
    M_e[10, 6] = -11/210*A*l**2*rho
    M_e[10, 10] = (1/105)*A*l**3*rho
    M_e[11, 5] = (1/6)*J*l*rho
    M_e[11, 11] = (1/3)*J*l*rho
    
    return M_e, K_e
