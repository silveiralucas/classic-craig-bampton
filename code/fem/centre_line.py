import numpy as np
import scipy.linalg as sa
import scipy.optimize as opt

#%% Local functions

def rotation_matrix(angle,axis):
    if (axis=='x'):
        A = np.array([[1, 0, 0],
                    [0, np.cos(angle), np.sin(angle)],
                    [0, -np.sin(angle), np.cos(angle)]], dtype=float)
    elif (axis=='y'):
        A = np.array([[np.cos(angle), 0, -np.sin(angle)],
                    [0, 1, 0],
                    [np.sin(angle), 0, np.cos(angle)]], dtype=float)
    elif (axis=='z'):
        A = np.array([[np.cos(angle), np.sin(angle), 0],
                    [-np.sin(angle), np.cos(angle), 0],
                    [0, 0, 1]], dtype=float)
    else:
        print('error in rotate_matrix function')
    return A

def d_plane(r_e, theta):
    # Centroid
    x_c = r_e[:, 0].mean()
    y_c = r_e[:, 1].mean()
    z_c = r_e[:, 2].mean()
    # Plane orientation
    alpha, beta = theta
    # Distance from the plane    
    d_n = np.zeros((r_e.shape[0], ), dtype=float)
    for i_n in range(r_e.shape[0]):
        x_p, y_p, z_p = r_e[i_n, :]
        d_n[i_n] = np.abs((-x_c + x_p)*np.sin(beta) - (-y_c + y_p)*np.sin(alpha)*np.cos(beta) + (-z_c + z_p)*np.cos(alpha)*np.cos(beta))
    # Root mean square average
    d = sa.norm(d_n)
    return d

def d_angle(r_c, r_0, r_1):
    # Torsion angle
    gamma = np.zeros((r_0.shape[0], ), dtype=float)
    for i_n in range(r_0.shape[0]):
        c = sa.norm(r_1[i_n, :] - r_0[i_n, :])
        b = sa.norm(r_0[i_n, :] - r_c)
        a = sa.norm(r_1[i_n, :] - r_c)
        gamma[i_n] = np.arccos((a**2+b**2-c**2)/(2*a*b))
    # Average torsion angle
    return gamma.mean()

#%% 



