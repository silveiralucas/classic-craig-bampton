import numpy as np
import scipy.linalg as sa
import sympy as sym
import matplotlib.pyplot as plt
from numpy_quadratic_quadrilateral import quad9

#%% Local functions

def func_n(x_i, y_i, z_i, x, y):
    
    A = np.zeros((x_i.size, x_i.size), dtype=float)
    for i in range(x_i.size):
        A[i, :] = np.array([1., x_i[i], y_i[i], x_i[i]*y_i[i], x_i[i]**2, y_i[i]**2, x_i[i]**2*y_i[i], x_i[i]*y_i[i]**2, x_i[i]**2*y_i[i]**2], dtype=float)
    c = sa.inv(A)@z_i
    
    # z = c.T @ np.array([1., x, y, x*y, x**2., y**2., x**2.*y, x*y**2., x**2.*y**2.], dtype=float) 
    z_x = c.T @ np.array([0., 1., 0., y, 2.*x, 0., 2.*x*y, y**2., 2.*x*y**2.], dtype=float) 
    z_y = c.T @ np.array([0., 0., 1., x, 0., 2.*y, x**2., 2.*x*y, 2.*x**2.*y], dtype=float)
    
    # r = np.array([x, y, z], dtype=float)
    r_x = np.array([1., 0., z_x], dtype=float)
    r_y = np.array([0., 1., z_y], dtype=float)
    h = np.cross(r_x, r_y)
    n = h/sa.norm(h)
    
    return n

#%% Test surface - concave paraboloid

x, y = sym.symbols('x, y')
z = -x**2-y**2+10

r = sym.Matrix([x, y, z])
h_0 = r.diff(x)
h_1 = r.diff(y)
h_2 = h_0.cross(h_1)
n = h_2.normalized()
n = sym.lambdify((x, y), n)

fig = plt.figure()


#%% Discrete surface section

# Test type
prescribed = False

# Nodes
if (prescribed): 
    # Prescribed test
    x = np.linspace(-1., 1., 3)
    y = np.linspace(0., 1., 3)
else:
    # Random test
    x = np.linspace(*np.sort(np.random.rand(2)), 3)
    y = np.linspace(*np.sort(np.random.rand(2)), 3)
    # x = np.sort(np.random.rand(3))
    # y = np.sort(np.random.rand(3))

X, Y = np.meshgrid(x, y)
Z = -X**2 -Y**2 +10

# Node canonical order
i_n = np.array([0, 2, 8, 6, 1, 5, 7, 3, 4], dtype=int)
x_i = X.flatten()[i_n]
y_i = Y.flatten()[i_n]
z_i = Z.flatten()[i_n]

# Canonical coordinates
xi  = np.array([-1., 1., 1., -1., 0., 1., 0., -1., 0.], dtype=float)
eta = np.array([-1., -1., 1., 1., -1., 0., 1., 0., 0.], dtype=float)
# xi = 2./(x_i.max()-x_i.min())*(x_i-x_i.min())-1.
# eta = 2./(y_i.max()-y_i.min())*(y_i-y_i.min())-1.

e_xi = np.zeros((xi.shape[0], 3), dtype=float)
e_eta = np.zeros((xi.shape[0], 3), dtype=float)
e_zeta = np.zeros((xi.shape[0], 3), dtype=float)
for i_node in range(xi.size):
    
    # Shape functions derivatives
    _, N_xi, N_eta = quad9(xi[i_node], eta[i_node])
    
    # Tangent 0
    h_xi = np.zeros((3, ))
    h_xi[0] = N_xi.T @ x_i
    h_xi[1] = N_xi.T @ y_i
    h_xi[2] = N_xi.T @ z_i
    e_xi[i_node, :] = h_xi / sa.norm(h_xi)
    
    # Tangent 1
    h_eta = np.zeros((3, ))
    h_eta[0] = N_eta.T @ x_i
    h_eta[1] = N_eta.T @ y_i
    h_eta[2] = N_eta.T @ z_i
    e_eta[i_node, :] = h_eta / sa.norm(h_xi)
    
    # Normal
    h_zeta = np.cross(h_xi, h_eta)
    e_zeta[i_node, :] = h_zeta / sa.norm(h_zeta)

# Comparison with analytical results
e_2 = np.zeros((x_i.size, 3), dtype=float)
for i_node in range(x_i.size):
    e_2[i_node, :] = n(x_i[i_node], y_i[i_node]).flatten()
res = sa.norm(e_zeta - e_2)
print(res)

e_n = np.zeros((x_i.size, 3), dtype=float)
for i_node in range(x_i.size):
    e_n[i_node, :] = func_n(x_i, y_i, z_i, x_i[i_node], y_i[i_node])
res = sa.norm(e_n - e_2)
print(res)