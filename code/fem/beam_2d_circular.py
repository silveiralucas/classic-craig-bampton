import numpy as np

#%%
    
def beam_2d_circular(x, d_out, d_int, rho, E):
    '''
    Mass and stiffness matrices of a two-dimensional Euler-Bernoulli beam with
    three degrees of freedom (longitudinal, transverse and angular) with
    variable circular cross section and constant material properties.
    
    P.S.: this function was generated automatically from the beam_2d.py
    symbolic script.
    
    Paramenters
    -----------
    x : numpy.ndarray([2], dtype=float)
        undeformed nodal longitudinal position [m]
    d_out : numpy.ndarray([2], dtype=float)
            outer diameters [m]
    d_int : numpy.ndarray([2], dtype=float)
            inner diameters [m]
    rho : float
          material density [kg/m**2]
    E : float
        Young's modulus [Pa]
    
    Returns
    -------
    M_e : numpy.ndarray([6, 6], dtype=float)
          mass matrix [kg]
    K_e : numpy.ndarray([6, 6], dtype=float)
          stiffness matrix [N/m]
    '''

    l = x[1] - x[0]
    D_0, D_1 = d_out
    d_0, d_1 = d_int
    
    K_e = np.zeros((6, 6), dtype=float)
    K_e[0, 0] = (1/12)*np.pi*D_0**2*E/l + (1/12)*np.pi*D_0*D_1*E/l + (1/12)*np.pi*D_1**2*E/l - 1/12*np.pi*E*d_0**2/l - 1/12*np.pi*E*d_0*d_1/l - 1/12*np.pi*E*d_1**2/l
    K_e[0, 3] = -1/12*np.pi*D_0**2*E/l - 1/12*np.pi*D_0*D_1*E/l - 1/12*np.pi*D_1**2*E/l + (1/12)*np.pi*E*d_0**2/l + (1/12)*np.pi*E*d_0*d_1/l + (1/12)*np.pi*E*d_1**2/l
    K_e[1, 1] = (33/560)*np.pi*D_0**4*E/l**3 + (3/112)*np.pi*D_0**3*D_1*E/l**3 + (9/560)*np.pi*D_0**2*D_1**2*E/l**3 + (3/112)*np.pi*D_0*D_1**3*E/l**3 + (33/560)*np.pi*D_1**4*E/l**3 - 33/560*np.pi*E*d_0**4/l**3 - 3/112*np.pi*E*d_0**3*d_1/l**3 - 9/560*np.pi*E*d_0**2*d_1**2/l**3 - 3/112*np.pi*E*d_0*d_1**3/l**3 - 33/560*np.pi*E*d_1**4/l**3
    K_e[1, 2] = (47/1120)*np.pi*D_0**4*E/l**2 + (11/560)*np.pi*D_0**3*D_1*E/l**2 + (9/1120)*np.pi*D_0**2*D_1**2*E/l**2 + (1/140)*np.pi*D_0*D_1**3*E/l**2 + (19/1120)*np.pi*D_1**4*E/l**2 - 47/1120*np.pi*E*d_0**4/l**2 - 11/560*np.pi*E*d_0**3*d_1/l**2 - 9/1120*np.pi*E*d_0**2*d_1**2/l**2 - 1/140*np.pi*E*d_0*d_1**3/l**2 - 19/1120*np.pi*E*d_1**4/l**2
    K_e[1, 4] = -33/560*np.pi*D_0**4*E/l**3 - 3/112*np.pi*D_0**3*D_1*E/l**3 - 9/560*np.pi*D_0**2*D_1**2*E/l**3 - 3/112*np.pi*D_0*D_1**3*E/l**3 - 33/560*np.pi*D_1**4*E/l**3 + (33/560)*np.pi*E*d_0**4/l**3 + (3/112)*np.pi*E*d_0**3*d_1/l**3 + (9/560)*np.pi*E*d_0**2*d_1**2/l**3 + (3/112)*np.pi*E*d_0*d_1**3/l**3 + (33/560)*np.pi*E*d_1**4/l**3
    K_e[1, 5] = (19/1120)*np.pi*D_0**4*E/l**2 + (1/140)*np.pi*D_0**3*D_1*E/l**2 + (9/1120)*np.pi*D_0**2*D_1**2*E/l**2 + (11/560)*np.pi*D_0*D_1**3*E/l**2 + (47/1120)*np.pi*D_1**4*E/l**2 - 19/1120*np.pi*E*d_0**4/l**2 - 1/140*np.pi*E*d_0**3*d_1/l**2 - 9/1120*np.pi*E*d_0**2*d_1**2/l**2 - 11/560*np.pi*E*d_0*d_1**3/l**2 - 47/1120*np.pi*E*d_1**4/l**2
    K_e[2, 1] = (47/1120)*np.pi*D_0**4*E/l**2 + (11/560)*np.pi*D_0**3*D_1*E/l**2 + (9/1120)*np.pi*D_0**2*D_1**2*E/l**2 + (1/140)*np.pi*D_0*D_1**3*E/l**2 + (19/1120)*np.pi*D_1**4*E/l**2 - 47/1120*np.pi*E*d_0**4/l**2 - 11/560*np.pi*E*d_0**3*d_1/l**2 - 9/1120*np.pi*E*d_0**2*d_1**2/l**2 - 1/140*np.pi*E*d_0*d_1**3/l**2 - 19/1120*np.pi*E*d_1**4/l**2
    K_e[2, 2] = (17/560)*np.pi*D_0**4*E/l + (9/560)*np.pi*D_0**3*D_1*E/l + (1/140)*np.pi*D_0**2*D_1**2*E/l + (1/280)*np.pi*D_0*D_1**3*E/l + (3/560)*np.pi*D_1**4*E/l - 17/560*np.pi*E*d_0**4/l - 9/560*np.pi*E*d_0**3*d_1/l - 1/140*np.pi*E*d_0**2*d_1**2/l - 1/280*np.pi*E*d_0*d_1**3/l - 3/560*np.pi*E*d_1**4/l
    K_e[2, 4] = -47/1120*np.pi*D_0**4*E/l**2 - 11/560*np.pi*D_0**3*D_1*E/l**2 - 9/1120*np.pi*D_0**2*D_1**2*E/l**2 - 1/140*np.pi*D_0*D_1**3*E/l**2 - 19/1120*np.pi*D_1**4*E/l**2 + (47/1120)*np.pi*E*d_0**4/l**2 + (11/560)*np.pi*E*d_0**3*d_1/l**2 + (9/1120)*np.pi*E*d_0**2*d_1**2/l**2 + (1/140)*np.pi*E*d_0*d_1**3/l**2 + (19/1120)*np.pi*E*d_1**4/l**2
    K_e[2, 5] = (13/1120)*np.pi*D_0**4*E/l + (1/280)*np.pi*D_0**3*D_1*E/l + (1/1120)*np.pi*D_0**2*D_1**2*E/l + (1/280)*np.pi*D_0*D_1**3*E/l + (13/1120)*np.pi*D_1**4*E/l - 13/1120*np.pi*E*d_0**4/l - 1/280*np.pi*E*d_0**3*d_1/l - 1/1120*np.pi*E*d_0**2*d_1**2/l - 1/280*np.pi*E*d_0*d_1**3/l - 13/1120*np.pi*E*d_1**4/l
    K_e[3, 0] = -1/12*np.pi*D_0**2*E/l - 1/12*np.pi*D_0*D_1*E/l - 1/12*np.pi*D_1**2*E/l + (1/12)*np.pi*E*d_0**2/l + (1/12)*np.pi*E*d_0*d_1/l + (1/12)*np.pi*E*d_1**2/l
    K_e[3, 3] = (1/12)*np.pi*D_0**2*E/l + (1/12)*np.pi*D_0*D_1*E/l + (1/12)*np.pi*D_1**2*E/l - 1/12*np.pi*E*d_0**2/l - 1/12*np.pi*E*d_0*d_1/l - 1/12*np.pi*E*d_1**2/l
    K_e[4, 1] = -33/560*np.pi*D_0**4*E/l**3 - 3/112*np.pi*D_0**3*D_1*E/l**3 - 9/560*np.pi*D_0**2*D_1**2*E/l**3 - 3/112*np.pi*D_0*D_1**3*E/l**3 - 33/560*np.pi*D_1**4*E/l**3 + (33/560)*np.pi*E*d_0**4/l**3 + (3/112)*np.pi*E*d_0**3*d_1/l**3 + (9/560)*np.pi*E*d_0**2*d_1**2/l**3 + (3/112)*np.pi*E*d_0*d_1**3/l**3 + (33/560)*np.pi*E*d_1**4/l**3
    K_e[4, 2] = -47/1120*np.pi*D_0**4*E/l**2 - 11/560*np.pi*D_0**3*D_1*E/l**2 - 9/1120*np.pi*D_0**2*D_1**2*E/l**2 - 1/140*np.pi*D_0*D_1**3*E/l**2 - 19/1120*np.pi*D_1**4*E/l**2 + (47/1120)*np.pi*E*d_0**4/l**2 + (11/560)*np.pi*E*d_0**3*d_1/l**2 + (9/1120)*np.pi*E*d_0**2*d_1**2/l**2 + (1/140)*np.pi*E*d_0*d_1**3/l**2 + (19/1120)*np.pi*E*d_1**4/l**2
    K_e[4, 4] = (33/560)*np.pi*D_0**4*E/l**3 + (3/112)*np.pi*D_0**3*D_1*E/l**3 + (9/560)*np.pi*D_0**2*D_1**2*E/l**3 + (3/112)*np.pi*D_0*D_1**3*E/l**3 + (33/560)*np.pi*D_1**4*E/l**3 - 33/560*np.pi*E*d_0**4/l**3 - 3/112*np.pi*E*d_0**3*d_1/l**3 - 9/560*np.pi*E*d_0**2*d_1**2/l**3 - 3/112*np.pi*E*d_0*d_1**3/l**3 - 33/560*np.pi*E*d_1**4/l**3
    K_e[4, 5] = -19/1120*np.pi*D_0**4*E/l**2 - 1/140*np.pi*D_0**3*D_1*E/l**2 - 9/1120*np.pi*D_0**2*D_1**2*E/l**2 - 11/560*np.pi*D_0*D_1**3*E/l**2 - 47/1120*np.pi*D_1**4*E/l**2 + (19/1120)*np.pi*E*d_0**4/l**2 + (1/140)*np.pi*E*d_0**3*d_1/l**2 + (9/1120)*np.pi*E*d_0**2*d_1**2/l**2 + (11/560)*np.pi*E*d_0*d_1**3/l**2 + (47/1120)*np.pi*E*d_1**4/l**2
    K_e[5, 1] = (19/1120)*np.pi*D_0**4*E/l**2 + (1/140)*np.pi*D_0**3*D_1*E/l**2 + (9/1120)*np.pi*D_0**2*D_1**2*E/l**2 + (11/560)*np.pi*D_0*D_1**3*E/l**2 + (47/1120)*np.pi*D_1**4*E/l**2 - 19/1120*np.pi*E*d_0**4/l**2 - 1/140*np.pi*E*d_0**3*d_1/l**2 - 9/1120*np.pi*E*d_0**2*d_1**2/l**2 - 11/560*np.pi*E*d_0*d_1**3/l**2 - 47/1120*np.pi*E*d_1**4/l**2
    K_e[5, 2] = (13/1120)*np.pi*D_0**4*E/l + (1/280)*np.pi*D_0**3*D_1*E/l + (1/1120)*np.pi*D_0**2*D_1**2*E/l + (1/280)*np.pi*D_0*D_1**3*E/l + (13/1120)*np.pi*D_1**4*E/l - 13/1120*np.pi*E*d_0**4/l - 1/280*np.pi*E*d_0**3*d_1/l - 1/1120*np.pi*E*d_0**2*d_1**2/l - 1/280*np.pi*E*d_0*d_1**3/l - 13/1120*np.pi*E*d_1**4/l
    K_e[5, 4] = -19/1120*np.pi*D_0**4*E/l**2 - 1/140*np.pi*D_0**3*D_1*E/l**2 - 9/1120*np.pi*D_0**2*D_1**2*E/l**2 - 11/560*np.pi*D_0*D_1**3*E/l**2 - 47/1120*np.pi*D_1**4*E/l**2 + (19/1120)*np.pi*E*d_0**4/l**2 + (1/140)*np.pi*E*d_0**3*d_1/l**2 + (9/1120)*np.pi*E*d_0**2*d_1**2/l**2 + (11/560)*np.pi*E*d_0*d_1**3/l**2 + (47/1120)*np.pi*E*d_1**4/l**2
    K_e[5, 5] = (3/560)*np.pi*D_0**4*E/l + (1/280)*np.pi*D_0**3*D_1*E/l + (1/140)*np.pi*D_0**2*D_1**2*E/l + (9/560)*np.pi*D_0*D_1**3*E/l + (17/560)*np.pi*D_1**4*E/l - 3/560*np.pi*E*d_0**4/l - 1/280*np.pi*E*d_0**3*d_1/l - 1/140*np.pi*E*d_0**2*d_1**2/l - 9/560*np.pi*E*d_0*d_1**3/l - 17/560*np.pi*E*d_1**4/l
    
    M_e = np.zeros((6, 6), dtype=float)
    M_e[0, 0] = (1/20)*np.pi*D_0**2*l*rho + (1/40)*np.pi*D_0*D_1*l*rho + (1/120)*np.pi*D_1**2*l*rho - 1/20*np.pi*d_0**2*l*rho - 1/40*np.pi*d_0*d_1*l*rho - 1/120*np.pi*d_1**2*l*rho
    M_e[0, 3] = (1/80)*np.pi*D_0**2*l*rho + (1/60)*np.pi*D_0*D_1*l*rho + (1/80)*np.pi*D_1**2*l*rho - 1/80*np.pi*d_0**2*l*rho - 1/60*np.pi*d_0*d_1*l*rho - 1/80*np.pi*d_1**2*l*rho
    M_e[1, 1] = (29/504)*np.pi*D_0**2*l*rho + (1/36)*np.pi*D_0*D_1*l*rho + (19/2520)*np.pi*D_1**2*l*rho - 29/504*np.pi*d_0**2*l*rho - 1/36*np.pi*d_0*d_1*l*rho - 19/2520*np.pi*d_1**2*l*rho
    M_e[1, 2] = (13/2016)*np.pi*D_0**2*l**2*rho + (5/1008)*np.pi*D_0*D_1*l**2*rho + (17/10080)*np.pi*D_1**2*l**2*rho - 13/2016*np.pi*d_0**2*l**2*rho - 5/1008*np.pi*d_0*d_1*l**2*rho - 17/10080*np.pi*d_1**2*l**2*rho
    M_e[1, 4] = (23/2520)*np.pi*D_0**2*l*rho + (1/72)*np.pi*D_0*D_1*l*rho + (23/2520)*np.pi*D_1**2*l*rho - 23/2520*np.pi*d_0**2*l*rho - 1/72*np.pi*d_0*d_1*l*rho - 23/2520*np.pi*d_1**2*l*rho
    M_e[1, 5] = -5/2016*np.pi*D_0**2*l**2*rho - 17/5040*np.pi*D_0*D_1*l**2*rho - 19/10080*np.pi*D_1**2*l**2*rho + (5/2016)*np.pi*d_0**2*l**2*rho + (17/5040)*np.pi*d_0*d_1*l**2*rho + (19/10080)*np.pi*d_1**2*l**2*rho
    M_e[2, 1] = (13/2016)*np.pi*D_0**2*l**2*rho + (5/1008)*np.pi*D_0*D_1*l**2*rho + (17/10080)*np.pi*D_1**2*l**2*rho - 13/2016*np.pi*d_0**2*l**2*rho - 5/1008*np.pi*d_0*d_1*l**2*rho - 17/10080*np.pi*d_1**2*l**2*rho
    M_e[2, 2] = (1/1008)*np.pi*D_0**2*l**3*rho + (1/1008)*np.pi*D_0*D_1*l**3*rho + (1/2520)*np.pi*D_1**2*l**3*rho - 1/1008*np.pi*d_0**2*l**3*rho - 1/1008*np.pi*d_0*d_1*l**3*rho - 1/2520*np.pi*d_1**2*l**3*rho
    M_e[2, 4] = (19/10080)*np.pi*D_0**2*l**2*rho + (17/5040)*np.pi*D_0*D_1*l**2*rho + (5/2016)*np.pi*D_1**2*l**2*rho - 19/10080*np.pi*d_0**2*l**2*rho - 17/5040*np.pi*d_0*d_1*l**2*rho - 5/2016*np.pi*d_1**2*l**2*rho
    M_e[2, 5] = -1/2016*np.pi*D_0**2*l**3*rho - 1/1260*np.pi*D_0*D_1*l**3*rho - 1/2016*np.pi*D_1**2*l**3*rho + (1/2016)*np.pi*d_0**2*l**3*rho + (1/1260)*np.pi*d_0*d_1*l**3*rho + (1/2016)*np.pi*d_1**2*l**3*rho
    M_e[3, 0] = (1/80)*np.pi*D_0**2*l*rho + (1/60)*np.pi*D_0*D_1*l*rho + (1/80)*np.pi*D_1**2*l*rho - 1/80*np.pi*d_0**2*l*rho - 1/60*np.pi*d_0*d_1*l*rho - 1/80*np.pi*d_1**2*l*rho
    M_e[3, 3] = (1/120)*np.pi*D_0**2*l*rho + (1/40)*np.pi*D_0*D_1*l*rho + (1/20)*np.pi*D_1**2*l*rho - 1/120*np.pi*d_0**2*l*rho - 1/40*np.pi*d_0*d_1*l*rho - 1/20*np.pi*d_1**2*l*rho
    M_e[4, 1] = (23/2520)*np.pi*D_0**2*l*rho + (1/72)*np.pi*D_0*D_1*l*rho + (23/2520)*np.pi*D_1**2*l*rho - 23/2520*np.pi*d_0**2*l*rho - 1/72*np.pi*d_0*d_1*l*rho - 23/2520*np.pi*d_1**2*l*rho
    M_e[4, 2] = (19/10080)*np.pi*D_0**2*l**2*rho + (17/5040)*np.pi*D_0*D_1*l**2*rho + (5/2016)*np.pi*D_1**2*l**2*rho - 19/10080*np.pi*d_0**2*l**2*rho - 17/5040*np.pi*d_0*d_1*l**2*rho - 5/2016*np.pi*d_1**2*l**2*rho
    M_e[4, 4] = (19/2520)*np.pi*D_0**2*l*rho + (1/36)*np.pi*D_0*D_1*l*rho + (29/504)*np.pi*D_1**2*l*rho - 19/2520*np.pi*d_0**2*l*rho - 1/36*np.pi*d_0*d_1*l*rho - 29/504*np.pi*d_1**2*l*rho
    M_e[4, 5] = -17/10080*np.pi*D_0**2*l**2*rho - 5/1008*np.pi*D_0*D_1*l**2*rho - 13/2016*np.pi*D_1**2*l**2*rho + (17/10080)*np.pi*d_0**2*l**2*rho + (5/1008)*np.pi*d_0*d_1*l**2*rho + (13/2016)*np.pi*d_1**2*l**2*rho
    M_e[5, 1] = -5/2016*np.pi*D_0**2*l**2*rho - 17/5040*np.pi*D_0*D_1*l**2*rho - 19/10080*np.pi*D_1**2*l**2*rho + (5/2016)*np.pi*d_0**2*l**2*rho + (17/5040)*np.pi*d_0*d_1*l**2*rho + (19/10080)*np.pi*d_1**2*l**2*rho
    M_e[5, 2] = -1/2016*np.pi*D_0**2*l**3*rho - 1/1260*np.pi*D_0*D_1*l**3*rho - 1/2016*np.pi*D_1**2*l**3*rho + (1/2016)*np.pi*d_0**2*l**3*rho + (1/1260)*np.pi*d_0*d_1*l**3*rho + (1/2016)*np.pi*d_1**2*l**3*rho
    M_e[5, 4] = -17/10080*np.pi*D_0**2*l**2*rho - 5/1008*np.pi*D_0*D_1*l**2*rho - 13/2016*np.pi*D_1**2*l**2*rho + (17/10080)*np.pi*d_0**2*l**2*rho + (5/1008)*np.pi*d_0*d_1*l**2*rho + (13/2016)*np.pi*d_1**2*l**2*rho
    M_e[5, 5] = (1/2520)*np.pi*D_0**2*l**3*rho + (1/1008)*np.pi*D_0*D_1*l**3*rho + (1/1008)*np.pi*D_1**2*l**3*rho - 1/2520*np.pi*d_0**2*l**3*rho - 1/1008*np.pi*d_0*d_1*l**3*rho - 1/1008*np.pi*d_1**2*l**3*rho
    
    return M_e, K_e
