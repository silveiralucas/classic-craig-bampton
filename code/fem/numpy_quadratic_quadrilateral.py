import numpy as np

#%%
    
def quad9(xi, eta):
    
    N = np.zeros((9, ), dtype=float)
    N[0] = (1/4)*eta*xi*(eta - 1)*(xi - 1)
    N[1] = (1/4)*eta*xi*(eta - 1)*(xi + 1)
    N[2] = (1/4)*eta*xi*(eta + 1)*(xi + 1)
    N[3] = (1/4)*eta*xi*(eta + 1)*(xi - 1)
    N[4] = -1/2*eta*(eta - 1)*(xi - 1)*(xi + 1)
    N[5] = -1/2*xi*(eta - 1)*(eta + 1)*(xi + 1)
    N[6] = -1/2*eta*(eta + 1)*(xi - 1)*(xi + 1)
    N[7] = -1/2*xi*(eta - 1)*(eta + 1)*(xi - 1)
    N[8] = (eta - 1)*(eta + 1)*(xi - 1)*(xi + 1)
    
    N_xi = np.zeros((9, ), dtype=float)
    N_xi[0] = (1/4)*eta*(eta - 1)*(2*xi - 1)
    N_xi[1] = (1/4)*eta*(eta - 1)*(2*xi + 1)
    N_xi[2] = (1/4)*eta*(eta + 1)*(2*xi + 1)
    N_xi[3] = (1/4)*eta*(eta + 1)*(2*xi - 1)
    N_xi[4] = eta*xi*(1 - eta)
    N_xi[5] = -1/2*(eta - 1)*(eta + 1)*(2*xi + 1)
    N_xi[6] = -eta*xi*(eta + 1)
    N_xi[7] = (1/2)*(1 - 2*xi)*(eta - 1)*(eta + 1)
    N_xi[8] = 2*xi*(eta**2 - 1)
    
    N_eta = np.zeros((9, ), dtype=float)
    N_eta[0] = (1/4)*xi*(2*eta - 1)*(xi - 1)
    N_eta[1] = (1/4)*xi*(2*eta - 1)*(xi + 1)
    N_eta[2] = (1/4)*xi*(2*eta + 1)*(xi + 1)
    N_eta[3] = (1/4)*xi*(2*eta + 1)*(xi - 1)
    N_eta[4] = (1/2)*(1 - 2*eta)*(xi - 1)*(xi + 1)
    N_eta[5] = -eta*xi*(xi + 1)
    N_eta[6] = -1/2*(2*eta + 1)*(xi - 1)*(xi + 1)
    N_eta[7] = eta*xi*(1 - xi)
    N_eta[8] = 2*eta*(xi**2 - 1)
    
    return N, N_xi, N_eta
