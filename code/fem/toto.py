import sympy as sym
from my_printer import MyPrinter
code_gen = MyPrinter().doprint

#%%

c = sym.Matrix(sym.symbols('c_0:9'))
x, y = sym.symbols('x, y')

z = c.T@sym.Matrix([1, x, y, x*y, x**2, y**2, x**2*y, x*y**2, x**2*y**2])