import sympy as sym
from my_printer import MyPrinter

code_gen = MyPrinter().doprint

#%%

export_code = False

#%%

def rotate_matrix(angle,axis): # return A
    '''
    Generates a transformation matrix.
    
    Parameters
    ----------
    angle : sympy.Symbol or sympy.Function
            [rad] angle between the two reference frames along the 'axis'
    axis : str
           axis of roation. 'x', 'y' and 'z'.
    
    Returns
    -------
    A : sympy.Matrix [:, :]
        transformation matrix from reference frame 1 to reference frame 2.
    '''
    if (axis=='x'):
        A = sym.Matrix([[1, 0, 0],
                    [0, sym.cos(angle), sym.sin(angle)],
                    [0, -sym.sin(angle), sym.cos(angle)]])
    elif (axis=='y'):
        A = sym.Matrix([[sym.cos(angle), 0, -sym.sin(angle)],
                    [0, 1, 0],
                    [sym.sin(angle), 0, sym.cos(angle)]])
    elif (axis=='z'):
        A = sym.Matrix([[sym.cos(angle), sym.sin(angle), 0],
                    [-sym.sin(angle), sym.cos(angle), 0],
                    [0, 0, 1]])
    else:
        print('error in rotate_matrix function')
    
    return A


#%%

# Symbols
n = sym.Matrix(sym.symbols('n_:3'))
a = sym.Matrix(sym.symbols('a_:3'))
t_0 = sym.Matrix(sym.symbols('t_0x, t_0y, t_0z'))
t_1 = sym.Matrix(sym.symbols('t_1x, t_1y, t_1z'))

theta = sym.Matrix((sym.symbols('phi, beta, gamma')))
r_p = sym.Matrix(sym.symbols('x_p, y_p, z_p'))
r_c = sym.Matrix(sym.symbols('x_c, y_c, z_c'))
u_c = sym.Matrix(sym.symbols('u_c, v_c, w_c'))

# Small rotations assumption
## sin(small) = small, cos(small) = 1.0
list_small = [*theta]
dict_small = {} # empty dictionary
for small in list_small:
    dict_small.update({sym.sin(small): small, sym.cos(small): 1, sym.tan(small): small})        
## small*small = 0.0
list_small_squared = [] # empty list
for small1 in list_small:
    for small2 in list_small:
        list_small_squared = list_small_squared + [small1 * small2]
set_small_squared = set(list_small_squared) # eliminate repetead items by using the set data structure
dict_small_squared = {small: 0 for small in set_small_squared}

#%%

# Rotation matrix
A = rotate_matrix(theta[2], 'z') @ rotate_matrix(theta[1], 'y') @ rotate_matrix(theta[0], 'x')
A = A.expand()
# A = A.subs(dict_small).subs(dict_small_squared)

# Displacement vector
u_p = u_c + (A.T-sym.eye(3)).subs(dict_small).subs(dict_small_squared)@(r_p-r_c)
u_p = u_p.expand()

q = sym.Matrix([u_c, theta])
B = sym.zeros(3, 6)
for i in range(3):
    for j in range(6):
        B[i, j] = u_p[i].coeff(q[j])

# a[0] = theta.T @ t_1
# a[1] = - theta.T @ t_0
u_p = sym.Matrix([u_p, theta])

q = sym.Matrix([u_c, theta])
B = sym.zeros(u_p.shape[0], q.shape[0])
for i in range(u_p.shape[0]):
    for j in range(6):
        B[i, j] = u_p[i].expand().coeff(q[j])

#%% Generate a numerical code


if export_code:
    
    print('Exporting code.')
    
    with open('numpy_rigid_connector2.py', mode='w+') as f:
        print('import numpy as np', file=f)
        print('', file=f)
        print('#%%', file=f)
        print('    ', file=f)
        
        print('def rigid_connector2(r_p, r_c):', file=f)
        print('    x_p, y_p, z_p = r_p', file=f)
        print('    x_c, y_c, z_c = r_c', file=f)

        print('    ', file=f)
        
        print('    B_e = np.zeros((%i, %i), dtype=float)' %(B.shape[0], B.shape[1]), file=f)
        for i in range(B.shape[0]):
            for j in range(B.shape[1]):
                if (code_gen(B[i, j])!='0'):
                    text = code_gen(B[i, j]).replace('numpy.', 'np.')
                    print('    B_e[%i, %i] = %s'%(i, j, text), file=f)

        print('    ', file=f)
        print('    return B_e', file=f)
