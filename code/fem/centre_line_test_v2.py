import numpy as np
import scipy.linalg as sa
import scipy.optimize as opt
import copy
from mayavi import mlab
import matplotlib.pyplot as plt
import centre_line as cl

#%% Local functions

class EmptyObject(object):
    def copy(self):
        return copy.deepcopy(self)

def rotation_matrix(angle,axis):
    if (axis=='x'):
        A = np.array([[1, 0, 0],
                    [0, np.cos(angle), np.sin(angle)],
                    [0, -np.sin(angle), np.cos(angle)]], dtype=float)
    elif (axis=='y'):
        A = np.array([[np.cos(angle), 0, -np.sin(angle)],
                    [0, 1, 0],
                    [np.sin(angle), 0, np.cos(angle)]], dtype=float)
    elif (axis=='z'):
        A = np.array([[np.cos(angle), np.sin(angle), 0],
                    [-np.sin(angle), np.cos(angle), 0],
                    [0, 0, 1]], dtype=float)
    else:
        print('error in rotate_matrix function')
    return A

#%% Bent I beam

# Undeformed geometry
g0 = EmptyObject()

# Cetreline
g0.r_c = np.zeros((200, 3))
g0.r_c[:, 2] = np.linspace(0., 10, 200)

# Sections
p0 = np.array([5.1, -234., 0.])
p1 = np.array([5.1, 234., 0.])
p2 = np.array([100., 234., 0.])
p3 = np.array([100., 250., 0.])
p4 = np.array([-100., 250., 0.])
p5 = np.array([-100., 234., 0.])
p6 = np.array([-5.1, 234., 0.])
p7 = np.array([-5.1, -234., 0.])
p8 = np.array([-100., -234., 0.])
p9 = np.array([-100., -250., 0.])
p10 = np.array([100., -250., 0.])
p11 = np.array([100., -234., 0.])

s = np.block([[p0], [p1], [p2], [p3], [p4], [p5], [p6], [p7], [p8], [p9], [p10], [p11], [p0]])*1.e-3
g0.s = np.zeros((g0.r_c.shape[0], s.shape[0], s.shape[1]), dtype=float)
for i_z in range(g0.s.shape[0]):
    g0.s[i_z, :, :] = s
    g0.s[i_z, :, 2] = g0.r_c[i_z, 2]

# fig = mlab.figure()
# mlab.plot3d(g0.r_c[:, 0], g0.r_c[:, 1], g0.r_c[:, 2], tube_radius=0.01)
# mlab.plot3d(g0.s[0, :, 0], g0.s[0, :, 1], g0.s[0, :, 2], tube_radius=0.01)

# Deformed geometry
g1 = EmptyObject()

# Centreline
g1.r_c = g0.r_c.copy()
g1.r_c[:, 0] = 1.e-2 * g1.r_c[:, 2]**2

# Unit vectors and transformation matrices
g1.e_0 = np.zeros(g1.r_c.shape, dtype=float)
g1.e_1 = np.zeros(g1.r_c.shape, dtype=float)
g1.e_2 = np.zeros(g1.r_c.shape, dtype=float)
g1.A_01 = np.zeros((g1.r_c.shape[0], 3, 3), dtype=float)
for i_z in range(g1.r_c.shape[0]):
    g1.e_1[i_z, :] = [0., 1., 0.]

    g1.e_2[i_z, :] = [2.e-2 * g1.r_c[i_z, 2], 0., 1.]
    g1.e_2[i_z, :] = g1.e_2[i_z, :]/sa.norm(g1.e_2[i_z, :])
    
    g1.e_0[i_z, :] = np.cross(g1.e_1[i_z, :], g1.e_2[i_z, :])
    g1.e_0[i_z, :] = g1.e_0[i_z, :]/sa.norm(g1.e_0[i_z, :])
    
    g1.A_01[i_z, 0, :] = g1.e_0[i_z, :]
    g1.A_01[i_z, 1, :] = g1.e_1[i_z, :]
    g1.A_01[i_z, 2, :] = g1.e_2[i_z, :]

# Sections 
g1.s = np.zeros(g0.s.shape, dtype=float)
for i_z in range(g0.s.shape[0]):
    for i_n in range(g0.s.shape[1]):
        A_z = rotation_matrix(g1.r_c[i_z, 2]*(np.pi/180.), 'z')
        
        g1.s[i_z, i_n, :] = g1.A_01[i_z, :, :].T @ (A_z @ g0.s[i_z, i_n, :] - g0.r_c[i_z, :]) + g1.r_c[i_z, :]
        

fig = mlab.figure()
mlab.plot3d(g1.r_c[:, 0], g1.r_c[:, 1], g1.r_c[:, 2], tube_radius=0.01)
mlab.quiver3d(g1.r_c[-1, 0], g1.r_c[-1, 1], g1.r_c[-1, 2], g1.e_0[-1, 0], g1.e_0[-1, 1], g1.e_0[-1, 2], scale_factor=1)
mlab.quiver3d(g1.r_c[-1, 0], g1.r_c[-1, 1], g1.r_c[-1, 2], g1.e_1[-1, 0], g1.e_1[-1, 1], g1.e_1[-1, 2], scale_factor=1)
mlab.quiver3d(g1.r_c[-1, 0], g1.r_c[-1, 1], g1.r_c[-1, 2], g1.e_2[-1, 0], g1.e_2[-1, 1], g1.e_2[-1, 2], scale_factor=1)

mlab.plot3d(g0.s[0, :, 0], g0.s[0, :, 1], g0.s[0, :, 2], tube_radius=0.01)
mlab.plot3d(g1.s[-1, :, 0], g1.s[-1, :, 1], g1.s[-1, :, 2], tube_radius=0.01)

#%% Test

n_sec = g0.r_c.shape[0]

A = np.zeros(g1.A_01.shape)
alpha = np.zeros((g0.r_c.shape[0], ), dtype=float)
beta = np.zeros((g0.r_c.shape[0], ), dtype=float)
gamma = np.zeros((g0.r_c.shape[0], ), dtype=float)
for i_z in range(n_sec):
    r_e = g1.s[i_z, :, :]
    r_c = g1.r_c[i_z, :]
    
    alpha[i_z], beta[i_z] = opt.minimize(lambda theta: cl.d_plane(r_e, theta), np.array([0., 0.])).x
    A[i_z, :, :] = rotation_matrix(beta[i_z], 'y') @ rotation_matrix(alpha[i_z], 'x')
    
    r_0 = np.zeros(r_e.shape)
    r_1 = np.zeros(r_e.shape)
    for i_n in range(r_e.shape[0]):
        r_1[i_n, :] = A[i_z, :, :] @ (r_e[i_n, :]-r_c)
        r_0[i_n, :] = g0.s[i_z, i_n, :] - g0.r_c[i_z, :]
    gamma[i_z] = cl.d_angle(np.zeros((3, )), r_0, r_1) * (180./np.pi)
    
#%%

fig = plt.figure()
plt.plot(g0.r_c[:, 2], gamma)
plt.xlabel(r'z [m]')
plt.ylabel(r'$\gamma$ [deg]')

