import sympy as sym

from my_printer import MyPrinter
code_gen = MyPrinter().doprint

#%%

export_code = True

#%%
xi, eta, zeta = sym.symbols('xi, eta, zeta')
c = sym.Matrix(sym.symbols('c_0:9'))

# Nodal order
xi_i = sym.Matrix([-1, 1, 1, -1, 0, 1, 0, -1, 0])
eta_i = sym.Matrix([-1, -1, 1, 1, -1, 0, 1, 0, 0])

# Matrix A, where
# A[i, :] = [1, xi_i, eta_i, xi_i^2, eta_i^2, xi_i^2·eta_i, xi_i·eta_i^2, xi_i^2·eta_i^2]
A = sym.zeros(9, 9)
for i in range(xi_i.shape[0]):
    A[i, 0] = 1
    A[i, 1] = xi_i[i]
    A[i, 2] = eta_i[i]
    A[i, 3] = xi_i[i]*eta_i[i]
    A[i, 4] = xi_i[i]**2
    A[i, 5] = eta_i[i]**2
    A[i, 6] = xi_i[i]**2 * eta_i[i]
    A[i, 7] = xi_i[i] * eta_i[i]**2
    A[i, 8] = xi_i[i]**2 * eta_i[i]**2

# Solve the equation
# N_i[xi_j, eta_j] = delta_ij, or
# A·c_i = B[:, i]
# c_i = A^(-1)·B[:, i]
# N_i[xi, eta] = c_i · q
B = sym.eye(9)

q = sym.Matrix([1, xi, eta, xi*eta, xi**2, eta**2, xi**2*eta, xi*eta**2, xi**2*eta**2])

N = sym.zeros(B.shape[1], 1)
for i in range(B.shape[1]):
    ci = A.inv() @ sym.eye(9)[:, i]
    N[i] = (ci.T @ q)[0].simplify().factor()

# dN/dxi, dN/deta
N_xi = sym.simplify(N.diff(xi))
N_eta = sym.simplify(N.diff(eta))

#%% Exporting numerical code

if (export_code):
    print('Exporting code.')
    
    with open('numpy_quadratic_quadrilateral.py', mode='w+') as f:
        
        print('import numpy as np', file=f)
        print('', file=f)
        print('#%%', file=f)
        print('    ', file=f)
        
        print('def quad9(xi, eta):', file=f)
        
        print('    ', file=f)
        print('    N = np.zeros((%i, ), dtype=float)' %(N.shape[0]), file=f)
        for i in range(N.shape[0]):
            if (code_gen(N[i])!='0'):
                text = code_gen(N[i]).replace('numpy.', 'np.')
                print('    N[%i] = %s' %(i, text), file=f)
        
        print('    ', file=f)
        print('    N_xi = np.zeros((%i, ), dtype=float)' %(N_xi.shape[0]), file=f)
        for i in range(N_xi.shape[0]):
            if (code_gen(N_xi[i])!='0'):
                text = code_gen(N_xi[i]).replace('numpy.', 'np.')
                print('    N_xi[%i] = %s' %(i, text), file=f)
        
        print('    ', file=f)
        print('    N_eta = np.zeros((%i, ), dtype=float)' %(N_eta.shape[0]), file=f)
        for i in range(N_eta.shape[0]):
            if (code_gen(N_eta[i])!='0'):
                text = code_gen(N_eta[i]).replace('numpy.', 'np.')
                print('    N_eta[%i] = %s' %(i, text), file=f)
        
        print('    ', file=f)
        print('    return N, N_xi, N_eta', file=f)
    