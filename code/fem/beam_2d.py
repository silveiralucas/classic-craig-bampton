import numpy as np
import scipy.linalg as sa
import copy

from .beam_2d_const import beam_2d_const
from .beam_2d_circular import beam_2d_circular

#%% Functions

def boolean_matrix(a, b): # return L
    '''
    Boolean transformation matrix between coordinates a and b.
    
    Parameters
    ----------
    a : numpy.ndarray([:], dtype=int)
        index of degrees of freedom
    b : numpy.ndarray([:], dtype=int)
        index of degrees of freedom
    
    Returs
    ------
    L : numpy.ndarray([:, :], dtype=int)
        Boolean transformation matrix between coordinates a and b
    '''

    L = np.zeros((a.size, b.size), dtype=int)
    for i in range(a.size):
        for j in range(b.size):
            if (a[i]==b[j]):
                L[i, j] = 1
    return L

def concentrated_inertia_2D(m, I, r=0.):

    # Offset, parallel axis theorem    
    I += m*(r**2)
    
    M_e = np.diag([m, m, I])
    K_e = np.zeros(M_e.shape)
    
    return M_e, K_e

def freq_h(M, K):
    vals, vecs = sa.eigh(M, K)
    
    f_n = 1/(2*np.pi)*np.sqrt(1./vals)
    idx = np.argsort(f_n)
    f_n = f_n[idx]
    vecs = vecs[:, idx]
    
    return f_n, vecs

#%% Classes

class EmptyObject(object):
    '''
    General use object with no atributes or methods other than copy.
    '''
    def copy(self):
        return copy.deepcopy(self)

class Beam2D(object):
    
    def __init__(self, x, A, I, rho, E):

        self.n_npe = 2
        self.n_dpn = 3

        self.x = x
        
        self.n_nodes = x.size
        self.n_elms = self.n_nodes - 1
        self.n_dofs = self.n_nodes * self.n_dpn

        self.A = A if isinstance(A, np.ndarray) else A*np.ones((self.n_elms, self.n_npe))
        self.I = I if isinstance(I, np.ndarray) else I*np.ones((self.n_elms, self.n_npe))
        self.rho = rho if isinstance(rho, np.ndarray) else rho*np.ones((self.n_elms, self.n_npe))
        self.E = E if isinstance(E, np.ndarray) else E*np.ones((self.n_elms, self.n_npe))
        
        self.nodes = np.array([[i_e, i_e+1] for i_e in range(self.n_elms)])
        self.dofs = np.arange(self.n_dofs).reshape((self.n_nodes, self.n_dpn))
        self.cts = np.zeros(self.dofs.shape, dtype=bool)
        
        self.q_idx = np.arange(self.n_dofs)
        self.M = np.zeros((self.n_dofs, self.n_dofs))
        self.K = np.zeros((self.n_dofs, self.n_dofs))

        for i_e in range(self.n_elms):
            n0, n1 = self.nodes[i_e, :]
            qe_idx = np.concatenate((self.dofs[n0, :], self.dofs[n1, :]))
            Le = boolean_matrix(qe_idx, self.q_idx)
            
            rho = self.rho[i_e, :].mean()
            E = self.E[i_e, :].mean()
            A = self.A[i_e, :].mean()
            I = self.I[i_e, :].mean()
            
            Me, Ke = beam_2d_const(self.x[n0:n1+1], A, I, rho, E)
            self.M += Le.T @ Me @ Le
            self.K += Le.T @ Ke @ Le

    def fixed_constraint(self, cts=None):
        
        if (cts is not None):        
            self.cts = cts

        q_bool = ~self.cts.flatten()
        
        self.M = self.M[np.ix_(q_bool, q_bool)]
        self.K = self.K[np.ix_(q_bool, q_bool)]
        
        k = 0
        for i in range(self.n_nodes):
            for j in range(self.n_dpn):
                if (self.cts[i, j]):
                    self.dofs[i, j] = -1
                else:
                    self.dofs[i, j] = k
                    k += 1
        self.n_dofs = k
        self.q_idx = np.arange(self.n_dofs)

class ConcentratedInertia(object):
    
    def __init__(self, m, I, r=0.):
        I += m*(r**2)
    
        self.M = np.diag([m, m, I])
        self.K = np.zeros(self.M.shape)

class CircularBeam2D(object):
    
    def __init__(self, x, d_out, d_int, rho, E):

        self.n_npe = 2
        self.n_dpn = 3

        self.x = x
        
        self.n_nodes = x.size
        self.n_elms = self.n_nodes - 1
        self.n_dofs = self.n_nodes * self.n_dpn

        self.d_out = d_out if isinstance(d_out, np.ndarray) else d_out*np.ones((self.n_elms, self.n_npe))
        self.d_int = d_int if isinstance(d_int, np.ndarray) else d_int*np.ones((self.n_elms, self.n_npe))
        self.rho = rho if isinstance(rho, np.ndarray) else rho*np.ones((self.n_elms, self.n_npe))
        self.E = rho if isinstance(E, np.ndarray) else E*np.ones((self.n_elms, self.n_npe))
        
        self.nodes = np.array([[i_e, i_e+1] for i_e in range(self.n_elms)])
        self.dofs = np.arange(self.n_dofs).reshape((self.n_nodes, self.n_dpn))
        self.cts = np.zeros(self.dofs.shape, dtype=bool)
        
        self.q_idx = np.arange(self.n_dofs)
        self.M = np.zeros((self.n_dofs, self.n_dofs))
        self.K = np.zeros((self.n_dofs, self.n_dofs))

        for i_e in range(self.n_elms):
            n0, n1 = self.nodes[i_e, :]
            qe_idx = np.concatenate((self.dofs[n0, :], self.dofs[n1, :]))
            Le = boolean_matrix(qe_idx, self.q_idx)
            
            rho = self.rho[i_e, :].mean()
            E = self.E[i_e, :].mean()
            
            Me, Ke = beam_2d_circular(self.x[n0:n1+1], self.d_out[i_e, :], self.d_int[i_e, :], rho, E)
            self.M += Le.T @ Me @ Le
            self.K += Le.T @ Ke @ Le

    def fixed_constraint(self, cts=None):
        
        if (cts is not None):        
            self.cts = cts

        q_bool = ~self.cts.flatten()
        
        self.M = self.M[np.ix_(q_bool, q_bool)]
        self.K = self.K[np.ix_(q_bool, q_bool)]
        
        k = 0
        for i in range(self.n_nodes):
            for j in range(self.n_dpn):
                if (self.cts[i, j]):
                    self.dofs[i, j] = -1
                else:
                    self.dofs[i, j] = k
                    k += 1
        self.n_dofs = k
        self.q_idx = np.arange(self.n_dofs)
